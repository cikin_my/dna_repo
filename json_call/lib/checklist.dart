import 'package:flutter/material.dart';
import 'dart:async';
import 'package:http/http.dart' as http;
import 'dart:convert';

class Checklist extends StatefulWidget {
  @override
  _ChecklistState createState() => _ChecklistState();
}

class _ChecklistState extends State<Checklist> {
  //initState
  bool selected = false;
  var userStatus = List<bool>();

  Future<List<User>> _getUsers() async {
    var data = await http
        .get("https://bitbucket.org/cikin_my/myjson/raw/f0f27161cdfcaa6ab14a2ff318144a13fb094f2d/sqa_checklist.json");

    var jsonData = json.decode(data.body);

    List<User> users = [];

    for (var u in jsonData) {
      User user =
      User(u["id"], u["title"], u["description"]);

      users.add(user);
      userStatus.add(false);
    }

    print(users.length);

    return users;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('sqa checklist'),
      ),
      body: Container(
        child: FutureBuilder(
          future: _getUsers(),
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            print(snapshot.data);
            if (snapshot.data == null) {
              return Container(child: Center(child: Text("Loading...")));
            } else {
              return ListView.builder(
                itemCount: snapshot.data.length,
                itemBuilder: (BuildContext context, int index) {
                  return Container(
                    decoration: BoxDecoration( //                    <-- BoxDecoration
                      border: Border(
                          bottom: BorderSide(color: Colors.grey.shade200)
                      ),
                    ),
                    child: ListTile(
                      title: Text(snapshot.data[index].title),
                      subtitle: snapshot.data[index].description == "" ? Text('') : Text(snapshot.data[index].description),
                      isThreeLine: true,
                      trailing: Checkbox(
                          value: userStatus[index],
                          onChanged: (bool val) {
                            setState(() {
                              userStatus[index] = !userStatus[index];
                            });
                          }),
                      onTap: () {
                        Navigator.push(
                            context,
                            new MaterialPageRoute(
                                builder: (context) =>
                                    DetailPage(snapshot.data[index])));
                      },
                    ),
                  );
                },
              );
            }
          },
        ),
      ),
    );
  }
}

class DetailPage extends StatelessWidget {
  final User user;

  DetailPage(this.user);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(user.title),
        ));
  }
}

class User {
  final String id;
  final String title;
  final String description;


  User(this.id,this.title,this.description);
}