
import 'package:dna_app/menu.dart';
import 'package:dna_app/myDrawer.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class HomePage_ApLead extends StatefulWidget {
  @override
  _HomePage_ApLeadState createState() => _HomePage_ApLeadState();
}

class _HomePage_ApLeadState extends State<HomePage_ApLead> {
  double screenHeight;
  @override
  Widget build(BuildContext context) {
    screenHeight = MediaQuery.of(context).size.height;

    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        title: const Text(
          'Welcome Back!',
          style: TextStyle(
              fontWeight: FontWeight.w900,
              color: Colors.black87,
              fontFamily: "OpenSans",
              fontSize: 14
          ),
        ),
        backgroundColor: Colors.white70,
        actions: <Widget>[
          IconButton(
            icon: FaIcon(
              FontAwesomeIcons.thLarge,
              size: 14,
              color: Colors.blue.shade700,
            ),
            tooltip: 'Show Snackbar',
            onPressed: () {},
          ),
          IconButton(
            icon: FaIcon(
              FontAwesomeIcons.expand,
              size: 14,
              color: Colors.blue.shade700,
            ),
            tooltip: 'Next page',
            onPressed: () {},
          ),
        ],
        leading: Builder(
          builder: (context) => GestureDetector(
            child: const Padding(
              padding: EdgeInsets.all(8.0),
              child: ClipOval(
                child: Image(
                  image: AssetImage('assets/images/me.png'),
                ),

              ),
            ),
            onTap:(){
              Scaffold.of(context).openDrawer();
            },
          ),
        ),


      ),
      drawer: MyDrawer(),
      body:Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          SizedBox(
            height:screenHeight / 18,
          ),
          Text('DNA', style:TextStyle(fontSize: 20,fontFamily: 'BellotaText', letterSpacing: 2, fontWeight:FontWeight.bold),),
          Text('DN Authorisation App', style:TextStyle(fontSize: 18,fontFamily: 'BellotaText', letterSpacing: 2),),
          SizedBox(height: 18,),
          Text('Main Menu', style:TextStyle(fontSize: 18,fontFamily: 'BellotaText', letterSpacing: 1),),
          SizedBox(height: 8,),
          Menu()
        ],
      ),
    );
  }
}
