import 'package:dna_app/ap_assist/home/homepage.dart';
import 'package:dna_app/homepage.dart';
import 'package:dna_app/main.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  double screenHeight;

  // Set intial mode to login
  AuthMode _authMode = AuthMode.NON;

  @override
  Widget build(BuildContext context) {
    screenHeight = MediaQuery.of(context).size.height;
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Stack(
          children: <Widget>[
            lowerHalf(context),
            upperHalf(context),
            pageTitle(),
            (_authMode == AuthMode.LEAD) ? LoginAsLead(context) : (_authMode == AuthMode.ASSIST) ? LoginAsAssist(context) : loginType(context),
            Row()
          ],
        ),
      ),
    );
  }

  Widget pageTitle() {
    return Container(
      margin: EdgeInsets.only(top: screenHeight / 2.4),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          new Expanded(
            child: new GestureDetector(
              child: new Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  new Padding(
                    padding: new EdgeInsets.only(left: 8.0, right: 8.0, bottom: 8.0, top: 0.0),
                    child: new Text(
                      "DNA",
                      style: new TextStyle(
                        fontFamily: 'BellotaText',
                        fontSize: 30.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  new Padding(
                    padding: new EdgeInsets.only(left: 8.0, right: 4.0, bottom: 4.0),
                    child: new Text(
                      "DN Authorisation App",
                      style: new TextStyle(fontFamily: 'BellotaText', fontSize: 21.0, fontWeight: FontWeight.bold, letterSpacing: 2),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget loginType(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
          margin: EdgeInsets.only(top: screenHeight / 1.8),
          padding: EdgeInsets.only(left: 10, right: 10),
          child: Card(
            color: Colors.transparent,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10),
            ),
            elevation: 0,
            child: Padding(
              padding: const EdgeInsets.all(30.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Align(
                    alignment: Alignment.center,
                    child: Text(
                      "Login As : ",
                      style: TextStyle(
                        fontFamily: 'BellotaText',
                        color: Colors.black54,
                        fontSize: 18,
                        fontWeight: FontWeight.w600,
                        letterSpacing: 2,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  SizedBox(
                    width: double.infinity,
                    child: RaisedButton.icon(
                      onPressed: () {
                        setState(() {
                          _authMode = AuthMode.LEAD;
                        });
                      },
                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(80.0)),
                      label: Text(
                        'AP LEAD',
                        style: TextStyle(fontFamily: 'BellotaText', color: Colors.white, letterSpacing: 3),
                      ),
                      icon: Padding(
                        padding: EdgeInsets.all(10.0),
                        child: FaIcon(FontAwesomeIcons.userTie),
                      ),
                      textColor: Colors.white,
                      splashColor: Colors.deepOrange.shade700,
                      color: Colors.orange,
                    ),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  SizedBox(
                    width: double.infinity,
                    child: RaisedButton.icon(
                      onPressed: () {
                        // Set intial mode to login
                        setState(() {
                          _authMode = AuthMode.ASSIST;
                        });
                      },
                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(20.0))),
                      label: Text(
                        'AP ASSIST',
                        style: TextStyle(fontFamily: 'BellotaText', color: Colors.white, letterSpacing: 3),
                      ),
                      icon: Padding(
                        padding: EdgeInsets.all(10.0),
                        child: FaIcon(
                          FontAwesomeIcons.userAstronaut,
                          color: Colors.white,
                        ),
                      ),
                      textColor: Colors.white,
                      splashColor: Colors.red.shade700,
                      color: Colors.red,
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            SizedBox(
              height: 10,
            ),
            Image.asset(
              "assets/images/tnb.png",
              width: 90.0,
              height: 90.0,
            ),
          ],
        )
      ],
    );
  }

  Widget upperHalf(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(18.0),
      height: screenHeight / 2,
      child: Image.asset(
        'assets/images/login2.png',
        fit: BoxFit.fitWidth,
      ),
    );
  }

  Widget lowerHalf(BuildContext context) {
    return Align(
      alignment: Alignment.bottomCenter,
      child: Container(
        height: screenHeight / 2,
        color: Colors.white,
        child: Center(child: Text("WaveClipperTwo(reverse: true)")),
      ),
    );
  }

  Widget LoginAsLead(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
          margin: EdgeInsets.only(top: screenHeight / 3),
          padding: EdgeInsets.only(left: 10, right: 10),
          child: Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10),
            ),
            elevation: 8,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(30.0, 10.0, 30.0, 30.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Align(
                    alignment: Alignment.center,
                    child: Text(
                      "Ap Lead",
                      style: TextStyle(color: Colors.black, fontSize: 24, fontWeight: FontWeight.w600, letterSpacing: 3),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  Align(
                    alignment: Alignment.center,
                    child: Text(
                      "Login",
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 18,
                        fontWeight: FontWeight.w400,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  TextFormField(
                    decoration: InputDecoration(labelText: "Email"),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  TextFormField(
                    decoration: InputDecoration(labelText: "Password"),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      MaterialButton(
                          onPressed: () {
                            Navigator.of(context).pop();
                            Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context) => new LoginPage()));
                          },
                          child: Text("Back")),
                      Expanded(
                        child: Container(),
                      ),
                      FlatButton(
                        child: Text("Login"),
                        splashColor: Colors.deepOrange,
                        color: Colors.orange,
                        textColor: Colors.white,
                        padding: EdgeInsets.only(left: 38, right: 38, top: 15, bottom: 15),
                        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
                        onPressed: () {
                          Navigator.of(context).pop();
                          Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context) => HomePage_ApLead()));
                        },
                      )
                    ],
                  )
                ],
              ),
            ),
          ),
        ),
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            SizedBox(
              height: 40,
            ),
            Text(
              "Don't have an account ?",
              style: TextStyle(color: Colors.grey),
            ),
            FlatButton(
              onPressed: () {},
              textColor: Colors.black87,
              child: Text("Create Account"),
            )
          ],
        )
      ],
    );
  }

  Widget LoginAsAssist(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
          margin: EdgeInsets.only(top: screenHeight / 5),
          padding: EdgeInsets.only(left: 10, right: 10),
          child: Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10),
            ),
            elevation: 8,
            child: Padding(
              padding: const EdgeInsets.all(30.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Align(
                    alignment: Alignment.center,
                    child: Text(
                      "AP Assist",
                      style: TextStyle(color: Colors.black, fontSize: 24, fontWeight: FontWeight.w600, letterSpacing: 3),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  Align(
                    alignment: Alignment.center,
                    child: Text(
                      "Login",
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 18,
                        fontWeight: FontWeight.w400,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  TextFormField(
                    decoration: InputDecoration(labelText: "Email"),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  TextFormField(
                    decoration: InputDecoration(labelText: "Password"),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      MaterialButton(
                          onPressed: () {
                            Navigator.of(context).pop();
                            Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context) => LoginPage()));
                          },
                          child: Text("Back")),
                      Expanded(
                        child: Container(),
                      ),
                      FlatButton(
                        child: Text("Login"),
                        splashColor: Colors.red.shade700,
                        color: Colors.red,
                        textColor: Colors.white,
                        padding: EdgeInsets.only(left: 38, right: 38, top: 15, bottom: 15),
                        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
                        onPressed: () {
                          Navigator.of(context).pop();
                          Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context) => HomePage_ApAssist()));
                        },
                      )
                    ],
                  )
                ],
              ),
            ),
          ),
        ),
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            SizedBox(
              height: 40,
            ),
            Text(
              "Already have an account?",
              style: TextStyle(color: Colors.grey),
            ),
            FlatButton(
              onPressed: () {},
              textColor: Colors.black87,
              child: Text("Login"),
            )
          ],
        ),
        Align(
          alignment: Alignment.bottomCenter,
          child: FlatButton(
            child: Text(
              "Terms & Conditions",
              style: TextStyle(
                color: Colors.grey,
              ),
            ),
            onPressed: () {},
          ),
        ),
      ],
    );
  }
}
