

import 'package:dna_app/PTW/ptw_summary.dart';
import 'package:dna_app/PTW/upload_manual_form.dart';
import 'package:dna_app/ap_assist/PTW/OkstQp_issue_ptw.dart';
import 'package:dna_app/ap_assist/PTW/bahagian_C_2.dart';
import 'package:dna_app/ap_assist/testSelected.dart';
import 'package:dna_app/login_page.dart';
import 'package:flutter/material.dart';
import 'dart:async';
import 'package:camera/camera.dart';



Future<void> main() async {
  // Ensure that plugin services are initialized so that `availableCameras()`
  // can be called before `runApp()`
  WidgetsFlutterBinding.ensureInitialized();

  // Obtain a list of the available cameras on the device.
  final cameras = await availableCameras();

  // Get a specific camera from the list of available cameras.
  final firstCamera = cameras.first;

  runApp(MyApp());
}
enum AuthMode { LEAD, ASSIST,OKST,NON}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.red,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home:PtwSummary(),
      debugShowCheckedModeBanner: false,
    );
  }
}




