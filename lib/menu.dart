import 'package:dna_app/Locking/listview_locking_point.dart';
import 'package:dna_app/Operation/listview_worklist.dart';
import 'package:dna_app/PTW/ptw_list.dart';
import 'package:dna_app/ViewLog/listview_operation_list.dart';
import 'file:///D:/All%20PROJECTs/Flutter%20project/DNA/dna_app/lib/testpage/job_sheet.dart';
//import 'file:///D:/Task_TNB/Flutter/DNA/DNA_Repo/lib/Operation/operation.dart';
import 'package:flutter/material.dart';

class Menu extends StatefulWidget {
  @override
  _MenuState createState() => _MenuState();
}

class _MenuState extends State<Menu> {
  List<String> events = [
    "Operation",
    "Locking Points",
    "Permit to Work",
    "View Log",
  ];

  @override
  Widget build(BuildContext context) {
    var color = 0xFFF5F5F5;
    return Flexible(
      child: GridView(
        padding: EdgeInsets.only(left: 16, right: 16),
        physics: BouncingScrollPhysics(), //only for ios
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
        children: events.map((url) {
          return GestureDetector(
            child: Container(
              decoration: BoxDecoration(color: Color(color), borderRadius: BorderRadius.circular(10), boxShadow: [
                BoxShadow(
                  color: Colors.grey[200],
                  blurRadius: 2.0, // has the effect of softening the shadow
                  spreadRadius: 1.0, // has the effect of extending the shadow
                  offset: Offset(
                    3.0, // horizontal, move right 10
                    3.0, // vertical, move down 10
                  ),
                )
              ]),
              margin: const EdgeInsets.all(10.0),
              child: getCardByTitle(url),
            ),
            onTap: () {
              Navigator.of(context).pop();
              switch (url) {
                case "Operation":
                  {
                    Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context) => WorkList()));
                  }
                  break;

                case "Locking Points":
                  {
                    Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context) => new LockingPoint()));
                  }
                  break;
                case "Permit to Work":
                  {
                    Navigator.of(context).push(new MaterialPageRoute(
                        builder: (BuildContext context) =>
                        new PTW_JobSheet()));
                  }
                  break;
                case "View Log":
                  {
                    Navigator.of(context).push(new MaterialPageRoute(
                        builder: (BuildContext context) =>
                        new ViewLog_Operation()));
                  }
                  break;
                case "checkIn":
                  {
//                    Navigator.of(context).push(new MaterialPageRoute(
//                        builder: (BuildContext context) => new CheckInOut()));
                  }
                  break;
                case "myTeam":
                  {
//                    Navigator.of(context).push(new MaterialPageRoute(
//                        builder: (BuildContext context) => new MyTeam()));
                  }
                  break;

                default:
                  {
                    //statements;
                    Navigator.of(context).pop();
                  }
                  break;
              }
              if (url == "View Log") {}

//                print(url);
            },
          );
        }).toList(),
      ),
    );
  }
}

Column getCardByTitle(String url) {
  String img = "";
  String title = "";
  if (url == "Operation") {
    img = "assets/images/gears.png";
    title = "Operation";
  } else if (url == "Locking Points") {
    img = "assets/images/scheme.png";
    title = "Locking Points";
  } else if (url == "Permit to Work") {
    img = "assets/images/clipboard.png";
    title = "Permit to Work";
  } else {
    img = "assets/images/tablet.png";
    title = "View Log";
  }

  return Column(
    mainAxisAlignment: MainAxisAlignment.center,
    children: <Widget>[
      Image.asset(
        img,
        width: 75,
      ),
      SizedBox(
        height: 10,
      ),
      Text(
        title,
        style: TextStyle(color: Colors.blueGrey[700], fontSize: 15, fontWeight: FontWeight.w500,letterSpacing: 1),
      ),
//      SizedBox(
//        height: 8,
//      ),
//      Text(
//        subtitle,
//        style: TextStyle(
//            color: Colors.black45, fontSize: 12, fontWeight: FontWeight.w600),
//      ),
    ],
  );
}
