import 'dart:convert';
import 'package:dna_app/ViewLog/view_log.dart';
import 'package:dna_app/homepage.dart';
import 'package:dna_app/myAppBar.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class ViewLog_Operation extends StatefulWidget {
  @override
  _ViewLog_OperationState createState() => _ViewLog_OperationState();
}

class _ViewLog_OperationState extends State<ViewLog_Operation> {
  Widget _buildContent(_data) {
    return InkWell(
      onTap: () {
        Navigator.of(context).push(MaterialPageRoute(
            builder: (BuildContext context) =>
                ViewLog()));
      },
      highlightColor: Colors.white30,
      splashColor: Colors.blue.shade50,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.fromLTRB(8, 8, 8, 2),
                    child: Row(
                      children: <Widget>[
                        Text(
                          _data['type'] + ":",
                          style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.w500),
                        ),
                        SizedBox(width: 5,),
                        Text(
                          _data['location'],
                          style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.w500),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                      padding: const EdgeInsets.fromLTRB(8, 0, 8, 8),
                      child: Container(
                          child: _data["Ap_assist"] != "unassigned" ? Row(
                            children: <Widget>[
                              Icon(Icons.arrow_forward, color: Colors.orange[200], size: 20,),
                              Text(_data["Ap_assist"], style: TextStyle(color: Colors.green, letterSpacing: 1,),),
                            ],
                          ) : Text('Unassigned', style:
                          TextStyle(color:
                          Colors
                              .grey[600]),)
                      )

                  ),
                ],
              ),
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                    padding: EdgeInsets.only(left: 4.0),
                    child: GestureDetector(
                      child: Icon(Icons.keyboard_arrow_right, color: Colors.blue, size: 30.0),
                      onTap: () {
                        //go to PO  Work Plan page
                      },
                    )),
              ],
            )
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Operation",
          style: TextStyle(
            letterSpacing: 1,
            fontFamily: 'Lato',
          ),
        ),
        centerTitle: true,
        leading: Builder(
          builder: (BuildContext context) {
            return IconButton(
              icon: const Icon(Icons.arrow_back),
              onPressed: () {
                Navigator.of(context).push(
                  MaterialPageRoute(
                      builder: (BuildContext context) => HomePage_ApLead()),
                );
              },
            );
          },
        ),
      ),
      body: FutureBuilder(
          future: DefaultAssetBundle.of(context).loadString('data/SwitchingData.json'),
          builder: (context, snapshot) {
            var _data = jsonDecode(snapshot.data.toString());
            if (snapshot.data == null) {
              return Container(child: Center(child: Text("Loading...")));
            } else {
              return ListView.builder(
                padding: EdgeInsets.all(8.0),
                itemCount: _data == null ? 0 : _data.length,
                itemBuilder: (BuildContext context, int index) {
                  return Container(
                    decoration: BoxDecoration(
                      border: Border(bottom: BorderSide(color: Colors.grey.shade200)),
                    ),
                    child: _buildContent(_data[index]),
                  );
                },
              );
            }
          }),
    );
  }
}