import 'package:dna_app/ViewLog/listview_operation_list.dart';
import 'package:flutter/material.dart';
class ViewLog extends StatefulWidget {
  @override
  _ViewLogState createState() => _ViewLogState();
}

class _ViewLogState extends State<ViewLog> {

  Widget _buildInfoContainer() {
    return Container(
      height: 100,
      margin: const EdgeInsets.all(8.0),
      padding: const EdgeInsets.all(10.0),
      decoration: BoxDecoration(
        boxShadow: [
          BoxShadow(blurRadius: 5.0, color: Colors.red[200], offset: Offset(0, 5)),
        ],
        borderRadius: BorderRadius.circular(15.0),
        gradient: LinearGradient(
          colors: [
            Color.fromRGBO(255, 137, 100, 1),
            Color.fromRGBO(255, 93, 110, 1),
          ],
        ),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Row(
            children: <Widget>[
              Text( ('Details'),
                style: TextStyle(fontWeight: FontWeight.bold, letterSpacing: 2, color: Colors.white, fontSize: 16, fontFamily: 'BellotaText'),
                textAlign: TextAlign.center,
              ),
            ],
            mainAxisAlignment: MainAxisAlignment.center,
          ),
          Divider(),
          SizedBox(
            height: 15,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                'No. JS:  ',
                style: TextStyle(fontSize: 15, color: Colors.white, letterSpacing: .75),
              ),
              Text(
                'ABC-12-12345',
                style: TextStyle(fontSize: 15, color: Colors.white, letterSpacing: .75),
              ),
            ],
          ),
          SizedBox(
            height: 5,
          ),
        ],
      ),
    );
  }
  Widget _buildTimelineElement_1(){
    return Padding(
      padding: const EdgeInsets.only(top:18.0, left:18),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Icon(Icons.check_circle, color: Colors.green,size: 30,),
              SizedBox(width: 8,),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text('Instruction to Execute',style: TextStyle(fontSize: 15),),
                  Text('Complete on'+ '12-May-2020 15:45 PM',style: TextStyle(fontSize: 13, color: Colors.grey[500],fontStyle: FontStyle.italic),),
                ],
              ),
            ],
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: SizedBox(height: 50, child: VerticalDivider(thickness: 2,color: Colors.blueGrey[700],)),
          ),
        ],
      ),
    );
  }
  Widget _buildTimelineElement_2(){
    return Padding(
      padding: const EdgeInsets.only(left:18.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Icon(Icons.check_circle, color: Colors.green,size: 30,),
              SizedBox(width: 8,),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text('Verify Locking Point',style: TextStyle(fontSize: 15),),
                  Text('Complete on'+ '12-May-2020 15:55 PM', style: TextStyle(fontSize: 13, color: Colors.grey[500],fontStyle: FontStyle.italic),),
                ],
              ),
            ],
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: SizedBox(height: 50, child: VerticalDivider(thickness: 2,color: Colors.blueGrey[700],)),
          ),
        ],
      ),
    );
  }
  Widget _buildTimelineElement_3(){
    return Padding(
      padding: const EdgeInsets.only( left:18.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Icon(Icons.remove_circle, color: Colors.grey,size: 30,),
              SizedBox(width: 8,),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text('Issue PWT',style: TextStyle(fontSize: 15),),
                  Text('',style: TextStyle(fontSize: 13, color: Colors.grey[500],fontStyle: FontStyle.italic),),
                ],
              ),
            ],
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: SizedBox(height: 50, child: VerticalDivider(thickness: 2,color: Colors.blueGrey[700],)),
          ),
        ],
      ),
    );
  }
  Widget _buildTimelineElement_4(){
    return Padding(
      padding: const EdgeInsets.only( left: 18.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Icon(Icons.remove_circle, color: Colors.grey,size: 30,),
              SizedBox(width: 8,),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text('Cancel PWT', style: TextStyle(fontSize: 15),),
                  Text('',style: TextStyle(fontSize: 13, color: Colors.grey[500],fontStyle: FontStyle.italic),),
                ],
              ),
            ],
          ),

        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    //    text styling ::
    TextStyle _style1 = TextStyle(
      fontFamily: 'opansans',
      color: Colors.blueGrey.shade700,
      fontSize: 15.0,
      fontWeight: FontWeight.w700,
    );
    TextStyle _style2 = TextStyle(
      fontFamily: 'opansans',
      color: Colors.blueGrey.shade700,
      fontSize: 13.0,
      fontWeight: FontWeight.w700,
    );
    TextStyle _style3 = TextStyle(
      fontFamily: 'opansans',
      fontSize: 13.0,
    );

    return Scaffold(
      appBar: AppBar(
        title: Text(
          "View Log",
          style: TextStyle(
            letterSpacing: 1,
            fontFamily: 'Lato',
          ),
        ),
        centerTitle: true,
        leading: Builder(
          builder: (BuildContext context) {
            return IconButton(
              icon: const Icon(Icons.arrow_back),
              onPressed: () {
                Navigator.of(context).push(
                  MaterialPageRoute(builder: (BuildContext context) => ViewLog_Operation()),
                );
              },
            );
          },
        ),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          _buildInfoContainer(),
          SizedBox( height: 18,),
          _buildTimelineElement_1(),
          _buildTimelineElement_2(),
          _buildTimelineElement_3(),
          _buildTimelineElement_4(),

        ],
      ),
    );
  }
}
