import 'package:dna_app/login_page.dart';
import 'package:flutter/material.dart';


class MyDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: <Widget>[
          UserAccountsDrawerHeader(
            accountName:  Text(
              "Shukeri Salleh",
              style: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.w800,
                color: Colors.white,
              ),
            ),
            accountEmail:  Text(
              'Ap Lead',
              style: TextStyle(
                letterSpacing: 3.0,
                fontSize: 13,
                fontWeight: FontWeight.w600,
                color: Colors.white60,

              ),
            ),
            currentAccountPicture: ClipRRect(
              borderRadius: BorderRadius.circular(110),
              child: Image.asset("assets/images/me.png", fit: BoxFit.cover,),
            ),
            decoration:  BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topRight,
                  end: Alignment.bottomLeft,
                  colors: [Color.fromRGBO(240, 152, 25,1), Color.fromRGBO(255, 88, 88,1)]),
            ),
          ),
          ListTile(
            trailing:  Icon(Icons.person_add),
            title:  Text("sample menu"),
            onTap: () {
//              Navigator.of(context).pop();
//              Navigator.of(context).push( MaterialPageRoute(
//                  builder: (BuildContext context) =>  WorkerAdd()));
            },
          ),
          Divider(height: 1),
          ListTile(
            trailing:  Icon(Icons.people),
            title:  Text("sample Menu"),
            onTap: () {
//              Navigator.of(context).pop();
//              Navigator.of(context).push( MaterialPageRoute(
//                  builder: (BuildContext context) =>  WorkerList()));
            },
          ),
          Divider(
            height: 1,
          ),
          ListTile(
            trailing:  Icon(Icons.settings_power),
            title:  Text("Log Out"),
            onTap: () {
              Navigator.of(context).pop();
              Navigator.of(context).push( MaterialPageRoute(
                  builder: (BuildContext context) =>  LoginPage()));
            },
          ),
        ],
      ),
    );
  }
}