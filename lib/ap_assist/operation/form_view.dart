import 'dart:io';

import 'package:dna_app/ap_assist/operation/listview.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

import 'package:intl/intl.dart';

import 'package:simple_animations/simple_animations.dart';

class FormView_AcceptAndSafetyDeclaration extends StatefulWidget {
  @override
  _FormView_AcceptAndSafetyDeclarationState createState() => _FormView_AcceptAndSafetyDeclarationState();
}

class _FormView_AcceptAndSafetyDeclarationState extends State<FormView_AcceptAndSafetyDeclaration> {
  double screenHeight;
  String value;
  DateTime date3;

  bool _showTimePicker = false;
  bool _isConfirmed = false;
  bool _showBtnConfirm = false;
  bool _showExecuteBtn = true;

  /*
  * =====================================================
  * Image Picker
  * =====================================================
  * */
  File _image;
  final picker = ImagePicker();

  Future getImage() async {
    final pickedFile = await picker.getImage(source: ImageSource.camera);

    setState(() {
      _image = File(pickedFile.path);
    });
  }

  /*
  * =====================================================
  * Time Picker
  * =====================================================
  * */
  TimeOfDay _time = new TimeOfDay.now();

  Future<Null> _selectTime(BuildContext context) async {
    final TimeOfDay picked = await showTimePicker(context: context, initialTime: _time);
    if (picked != null && picked != _time) {
      print('Date Time : ${_time.toString()}');
      setState(() {
        _time = picked;
      });
    }
  }

  @override
  void initState() {
    super.initState();
  }

  /*s All Widget
============================================= */
  showAlertDialog(BuildContext context) {
    // set up the buttons
    Widget cancelButton = FlatButton(
      child: Text(
        "Cancel",
        style: TextStyle(letterSpacing: 1),
      ),
      onPressed: () {
        Navigator.pop(context);
      },
      color: Colors.grey,
      textColor: Colors.white,
      splashColor: Colors.grey[700],
    );
    Widget continueButton = FlatButton(
      child: Text(
        "Confirm",
        style: TextStyle(letterSpacing: 1),
      ),
      onPressed: () {
        setState(() {
          _isConfirmed = true;
          _showTimePicker = false;
          _showBtnConfirm = false;
        });
        Navigator.pop(context);
      },
      color: Colors.red,
      textColor: Colors.white,
      splashColor: Colors.red[700],
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      titlePadding: EdgeInsets.all(0),
      contentPadding: EdgeInsets.all(0),
      contentTextStyle: TextStyle(color: Colors.black, fontSize: 12, fontFamily: 'opensans'),
      title: Container(
        padding: EdgeInsets.all(10),
        decoration: BoxDecoration(color: Colors.red),
        child: Text(
          "Self Declaration AP",
          style: TextStyle(color: Colors.white, letterSpacing: 1, fontSize: 16),
          textAlign: TextAlign.center,
        ),
      ),

      /*CHANGE THIS TEXT WITH THE LATEST ONE - cikin oii*/
      content: Container(
        height: screenHeight / 1.35,
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              SizedBox(
                height: 18,
              ),
              Row(children: <Widget>[
                Expanded(
                  child: new Container(
                      margin: const EdgeInsets.only(left: 10.0, right: 20.0),
                      child: Divider(
                        color: Colors.black,
                        height: 36,
                      )),
                ),
                Text("Deklarasi", style: TextStyle(fontWeight: FontWeight.w700, letterSpacing: 2), textAlign: TextAlign.center),
                Expanded(
                  child: new Container(
                      margin: const EdgeInsets.only(left: 20.0, right: 10.0),
                      child: Divider(
                        color: Colors.black,
                        height: 36,
                      )),
                ),
              ]),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text('1) Semakan saya, switch Gear BUKAN jenis VEI Flusarc F.'),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text('2) Semakan saya, presure gauge indicator bagi RMU menunjukkan tekanan gas yang cukup (Jarum petunjuk berada di zon hijau). '),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text('3) Semakan saya, switch Gear BUKAN jenis RMU SF6 - F&G yang tiada pressure gauge indicator. '),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text('4) Semakan saya, switch gear BUKAN jenis ABB Sealink (2003 - 2004).'),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text('5) Semakan saya, switch gear BUKAN jenis RMU Motorised Indkom IN24.'),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text('6) Saya telah periksa Arc Flash Suit (AFS), Sarung Tangan, Helmet & Visor saya dalam keadaan baik.'),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text('7) Saya telah memakai Arc Flash Suit (AFS) 40kcal lengkap dengan Glove, Helmat & Visor.'),
              ),
              Divider(
                color: Colors.black,
                height: 36,
              ),
              Text(
                'Saya mengesahkan bahawa semua jawapan senarai semak di atas adalah "YA". '
                'semua aktiviti yang berikut di bawah telah di buat dengan TEPAT dan '
                'RADAS adalah selamat  untuk di jalankan kerja ke atasnya',
                style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black),
                textAlign: TextAlign.center,
              ),
              SizedBox(height: 20),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text('AP NAME: '),
                  Text('Ariyya Stark'),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text('Staff ID: '),
                  Text('100100001'),
                ],
              )
            ],
          ),
        ),
      ),
      actions: [
        cancelButton,
        continueButton,
      ],
    );
    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  Widget _infoOperation() {
    return Padding(
      padding: const EdgeInsets.all(4.0),
      child: Card(
        elevation: 1,
        shadowColor: Colors.blueGrey[100],
        margin: const EdgeInsets.all(8.0),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                children: <Widget>[
                  Text(
                    'No Js :',
                    style: TextStyle(color: Colors.blueGrey, fontWeight: FontWeight.w500, fontSize: 14),
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  Text(
                    'ABC-12-12345',
                    style: TextStyle(color: Colors.black87, fontWeight: FontWeight.w300, fontSize: 16),
                  ),
                ],
              ),
              Row(
                children: <Widget>[
                  Text(
                    'Date & Time :',
                    style: TextStyle(color: Colors.blueGrey, fontWeight: FontWeight.w500, fontSize: 14),
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  Text(
                    '1-Mar-2020 18:00PM',
                    style: TextStyle(color: Colors.black87, fontWeight: FontWeight.w300, fontSize: 16),
                  ),
                ],
              ),
              Text(
                'Keterangan Kerja :',
                style: TextStyle(color: Colors.blueGrey, fontWeight: FontWeight.w500, fontSize: 14),
              ),
              SizedBox(
                width: 5,
              ),
              Text(
                'reventive Maintenance PE ABC',
                style: TextStyle(color: Colors.black87, fontWeight: FontWeight.w300, fontSize: 16),
              ),
              Divider(),
              Row(
                children: <Widget>[
                  Text(
                    'Punca Bekalan:   ',
                    style: TextStyle(color: Colors.blueGrey, fontWeight: FontWeight.w500, fontSize: 14),
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  Text(
                    'PMU AA ',
                    style: TextStyle(color: Colors.black87, fontWeight: FontWeight.w300, fontSize: 16),
                  ),
                ],
              ),
              Row(
                children: <Widget>[
                  Text(
                    'Lokasi :',
                    style: TextStyle(color: Colors.blueGrey, fontWeight: FontWeight.w500, fontSize: 14),
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  Text(
                    'TS - SMK Taman Megah Ria',
                    style: TextStyle(color: Colors.black87, fontWeight: FontWeight.w300, fontSize: 16),
                  ),
                ],
              ),
              Row(
                children: <Widget>[
                  Text(
                    'Switch :',
                    style: TextStyle(color: Colors.blueGrey, fontWeight: FontWeight.w500, fontSize: 14),
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  Text(
                    'JPG06065',
                    style: TextStyle(color: Colors.black87, fontWeight: FontWeight.w300, fontSize: 16),
                  ),
                ],
              ),
              Row(
                children: <Widget>[
                  Text(
                    'Command:  ON :',
                    style: TextStyle(color: Colors.blueGrey, fontWeight: FontWeight.w500, fontSize: 14),
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  Text(
                    'ON',
                    style: TextStyle(color: Colors.black87, fontWeight: FontWeight.w300, fontSize: 16),
                  ),
                ],
              ),
              Row(
                children: <Widget>[
                  Text(
                    'Open Issued at  :',
                    style: TextStyle(color: Colors.blueGrey, fontWeight: FontWeight.w500, fontSize: 14),
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  Text(
                    '1-Jan-2020 1430PM',
                    style: TextStyle(color: Colors.black87, fontWeight: FontWeight.w300, fontSize: 16),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _ButtonNext() {
    return RaisedButton.icon(
      onPressed: () => {
        setState(() {
          showAlertDialog(context);
        }),
      },
      icon: Icon(
        Icons.arrow_forward,
        size: 16,
      ),
      label: Row(
        children: <Widget>[
          Text('Next ', style: TextStyle(letterSpacing: 1)),
        ],
      ),
      color: Colors.red[600],
      splashColor: Colors.red[800],
      textColor: Colors.white,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(18)),
    );
  }

  Widget _ButtonExecute() {
    return RaisedButton.icon(
      onPressed: () => {
        setState(() {
          _showTimePicker = true;
          _showExecuteBtn = false;
        }),
      },
      icon: Icon(
        Icons.access_time,
        size: 16,
      ),
      label: Row(
        children: <Widget>[
          Text('Confirm Time ', style: TextStyle(letterSpacing: 1)),
        ],
      ),
      color: Colors.blue[600],
      splashColor: Colors.blue[800],
      textColor: Colors.white,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(18)),
    );
  }

  Widget _form() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Container(
          child: Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                  padding: EdgeInsets.all(15),
                  decoration: BoxDecoration(
                    border: Border.all(color: Colors.blueGrey),
                  ),
                  child: Text(
                    'Selected Time: ${_time.toString()}',
                    style: TextStyle(letterSpacing: 1, fontWeight: FontWeight.w600),
                  )),
              FlatButton.icon(
                onPressed: () {
                  _selectTime(context);
                },
                icon: Icon(Icons.access_time),
                label: Text(
                  'Select Time',
                  style: TextStyle(letterSpacing: 1),
                ),
                color: Colors.blue,
                splashColor: Colors.blue[700],
                textColor: Colors.white,
              ),
              SizedBox(
                height: 20,
              ),
              Text(
                'Attach Image',
                style: TextStyle(
                  fontFamily: 'opansans',
                  color: Colors.blueGrey.shade700,
                  fontSize: 16.0,
                  fontWeight: FontWeight.w700,
                  letterSpacing: 1,
                ),
              ),
              Row(
                children: <Widget>[
                  Expanded(
                    child: InkWell(
                      hoverColor: Colors.red,
                      focusColor: Colors.red,
                      splashColor: Colors.red,
                      child: Container(
                        padding: EdgeInsets.all(18),
                        child: _image == null
                            ? Image.asset('assets/images/addphoto.png')
                            : Image.file(
                                _image,
                              ),
                      ),
                      onTap: getImage,
                    ),
                  ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.only(left: 18.0, right: 18),
                child: Divider(),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _formSummary() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Card(
        child: Padding(
          padding: const EdgeInsets.only(top: 28.0, bottom: 28.0, left: 8),
          child: Row(
            children: <Widget>[
              Text(
                'Time Operated  :',
                style: TextStyle(color: Colors.blueGrey, fontWeight: FontWeight.w500, fontSize: 14),
              ),
              SizedBox(
                width: 5,
              ),
              Text(
                '1-Jan-2020 1430PM',
                style: TextStyle(color: Colors.black87, fontWeight: FontWeight.w300, fontSize: 16),
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    screenHeight = MediaQuery.of(context).size.height;

    return Scaffold(
        appBar: AppBar(
          title: Text(
            (_showBtnConfirm == true ? 'Accept & Safety Declaration' : _showTimePicker == true ? 'Execute Open' : ' Receive Open'),
            style: TextStyle(
              letterSpacing: 1,
              fontFamily: 'Lato',
            ),
          ),
          centerTitle: true,
          leading: Builder(
            builder: (BuildContext context) {
              return IconButton(
                icon: const Icon(Icons.arrow_back),
                onPressed: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(builder: (BuildContext context) => ListviewJobsheet()),
                  );
                },
              );
            },
          ),
        ),
        body: Container(
          height: double.maxFinite,
          child: Stack(
            children: <Widget>[
              ListView(
                children: <Widget>[
                  _infoOperation(),
                  Visibility(visible: _showTimePicker, child: _form()),
                  Visibility(visible: _isConfirmed, child: _formSummary()),
                  SizedBox(
                    height: 20,
                  ),
                  Visibility(
                    visible: _showExecuteBtn,
                    child: Padding(
                      padding: const EdgeInsets.only(left: 38.0, right: 38),
                      child: _ButtonExecute(),
                    ),
                  ),
                  Visibility(
                    visible: _showTimePicker,
                    child: Padding(
                      padding: const EdgeInsets.only(left: 18.0, right: 18),
                      child: _ButtonNext(),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ));
  }
}
