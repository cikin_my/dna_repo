import 'dart:convert';

import 'package:dna_app/ap_assist/operation/form_view.dart';
import 'package:dna_app/ap_assist/operation/listview.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class listviewWithInfo_ReceiveOpen extends StatefulWidget {
  @override
  _listviewWithInfo_ReceiveOpenState createState() => _listviewWithInfo_ReceiveOpenState();
}

class _listviewWithInfo_ReceiveOpenState extends State<listviewWithInfo_ReceiveOpen> {
  /*use for Demo Execute function
============================================= */
  bool _showTimePicker = false;
  bool _isVisible = true;
  bool _isConfirm = false;

  //  our widget ::
  //1. list of switching sequence
  Widget _buildListItem(_data) {
    return InkWell(
      onTap: () {
          Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext context) => FormView_AcceptAndSafetyDeclaration()));
      },
      highlightColor: Colors.white30,
      splashColor: Colors.red.shade50,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.fromLTRB(8, 8, 8, 2),
                    child: Row(
                      children: <Widget>[
                        Text(
                          _data['type'] + ":",
                          style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.w500),
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        Text(
                          _data['location'],
                          style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.w500),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                      padding: const EdgeInsets.fromLTRB(8, 0, 8, 8),
                      child: Container(
                          child: _data["Ap_assist"] != "unassigned"
                              ? Row(
                            children: <Widget>[
                              Text(
                                _data["Ap_assist"],
                                style: TextStyle(
                                  color: Colors.green,
                                  letterSpacing: 1,
                                ),
                              ),
                            ],
                          )
                              : Text(
                            'Unassigned',
                            style: TextStyle(color: Colors.grey[600]),
                          ))),
                ],
              ),
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                    padding: EdgeInsets.only(left: 4.0),
                    child: GestureDetector(
                      child:  Icon(Icons.keyboard_arrow_right, color: Colors.green[400], size: 25.0),
                      onTap: () {
                        //open alert to select assist
                      },
                    )),
              ],
            )
          ],
        ),
      ),
    );
  }

  //2. Details about that switching
  Widget _buildInfoContainer() {
    return Container(
      height: 200,
      margin: const EdgeInsets.all(8.0),
      padding: const EdgeInsets.all(10.0),
      decoration: BoxDecoration(
        boxShadow: [
          BoxShadow(blurRadius: 5.0, color: Colors.red[200], offset: Offset(0, 5)),
        ],
        borderRadius: BorderRadius.circular(15.0),
        gradient: LinearGradient(
          colors: [
            Color.fromRGBO(255, 137, 100, 1),
            Color.fromRGBO(255, 93, 110, 1),
          ],
        ),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            children: <Widget>[
              Text( ( 'Receive Open'),
                style: TextStyle(fontWeight: FontWeight.bold, letterSpacing: 2, color: Colors.white, fontSize: 16, fontFamily: 'BellotaText'),
                textAlign: TextAlign.center,
              ),
            ],
            mainAxisAlignment: MainAxisAlignment.center,
          ),
          Divider(),
          SizedBox(
            height: 15,
          ),
          Row(
            children: <Widget>[
              Text(
                'No. JS:  ',
                style: TextStyle(fontSize: 15, color: Colors.white, letterSpacing: .75),
              ),
              Text(
                'ABC-12-12345',
                style: TextStyle(fontSize: 15, color: Colors.white, letterSpacing: .75),
              ),
            ],
          ),
          SizedBox(
            height: 5,
          ),
          Row(
            children: <Widget>[
              Text(
                'Date & Time:',
                style: TextStyle(fontSize: 15, color: Colors.white, letterSpacing: .75),
              ),
              Text(
                '2-Mar-2020 18:00PM',
                style: TextStyle(fontSize: 15, color: Colors.white, letterSpacing: .75),
              ),
            ],
          ),
          SizedBox(
            height: 5,
          ),
          Text(
            'Keterangan Kerja:',
            style: TextStyle(fontSize: 16, color: Colors.white),
          ),
          Text(
            'Preventive Maintenance PE ABC, Preventive Maintenance PE ABCPreventive Maintenance PE ABC',
            style: TextStyle(fontSize: 15, color: Colors.white, letterSpacing: .75),
          ),
          SizedBox(
            height: 5,
          ),
        ],
      ),
    );
  }

  //3. button Execution to be available when all Ap is assigned to all sequence
  Widget _buildButton() {
    return Container(
      height: 84,
      padding: EdgeInsets.all(20),
      child: RaisedButton.icon(
        onPressed: () => {
          setState(() {
            _showTimePicker = true;
            _isVisible = false;
          }),
        },
        icon: Icon(
          FontAwesomeIcons.cogs,
          size: 18,
        ),
        label: Row(
          children: <Widget>[
            SizedBox(
              width: 15,
            ),
            Text('Execute', style: TextStyle(letterSpacing: 2, fontFamily: 'nexa', fontSize: 16)),
          ],
        ),
        color: Colors.blue,
        splashColor: Colors.blue[700],
        textColor: Colors.white,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
      ),
    );
  }

  //4. message to be display after click on execution button
  Widget _buildExecutedMsg() {
    return Container(
      color: Colors.green[500],
      padding: EdgeInsets.all(20),
      child: Center(
        child: Text(
          'Execute Work Instructed on 1-Mar-2020 18:30PM',
          style: TextStyle(letterSpacing: 1, color: Colors.white, fontSize: 17, fontStyle: FontStyle.italic),
          textAlign: TextAlign.center,
        ),
      ),
    );
  }


  @override
  Widget build(BuildContext context) {
    //    text styling ::
    TextStyle _style1 = TextStyle(
      fontFamily: 'opansans',
      color: Colors.blueGrey.shade700,
      fontSize: 15.0,
      fontWeight: FontWeight.w700,
    );
    TextStyle _style2 = TextStyle(
      fontFamily: 'opansans',
      color: Colors.blueGrey.shade700,
      fontSize: 13.0,
      fontWeight: FontWeight.w700,
    );
    TextStyle _style3 = TextStyle(
      fontFamily: 'opansans',
      fontSize: 13.0,
    );

    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Receive Open",
          style: TextStyle(
            letterSpacing: 1,
            fontFamily: 'Lato',
          ),
        ),
        centerTitle: true,
        leading: Builder(
          builder: (BuildContext context) {
            return IconButton(
              icon: const Icon(Icons.arrow_back),
              onPressed: () {
                Navigator.of(context).push(
                  MaterialPageRoute(builder: (BuildContext context) => ListviewJobsheet()),
                );
              },
            );
          },
        ),
      ),
      body: Container(
        height: double.maxFinite,
        child: Stack(
          children: <Widget>[
            FutureBuilder(
                future: DefaultAssetBundle.of(context).loadString('data/SwitchingData.json'),
                builder: (context, snapshot) {
                  var _data = jsonDecode(snapshot.data.toString());
                  if (snapshot.data == null) {
                    return Container(child: Center(child: Text("Loading...")));
                  } else {
                    return ListView(
                      children: <Widget>[
                        _buildInfoContainer(),
                        Padding(
                          padding: const EdgeInsets.fromLTRB(18, 25, 18, 0),
                          child: Column(
                            children: <Widget>[
                              Text('Switching Sequance', style: TextStyle(letterSpacing: 2, fontSize: 17, color: Colors.blueGrey),),
                              Divider(),
                            ],
                          ),
                        ),
                        ListView.builder(
                          physics: ScrollPhysics(),
                          shrinkWrap: true,
                          padding: EdgeInsets.all(8.0),
                          itemCount: _data == null ? 0 : _data.length,
                          itemBuilder: (BuildContext context, int index) {
                            return Container(
                              decoration: BoxDecoration(
                                border: Border(bottom: BorderSide(color: Colors.grey.shade200)),
                              ),
                              child: _buildListItem(_data[index]),
                            );
                          },
                        ),



                      ],
                    );
                  }
                }),
          ],
        ),
      ),
    );
  }
}