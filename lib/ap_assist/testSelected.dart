import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class ChangeListViewBGColor extends StatefulWidget {
  _ChangeListViewBGColorState createState() => _ChangeListViewBGColorState();
}

class _ChangeListViewBGColorState extends State<ChangeListViewBGColor> {
  final List<String> _listViewData = [
    "A List View with many Text - Here's one!",
    "A List View with many Text - Here's another!",
    "A List View with many Text - Here's more!",
    "A List View with many Text - Here's more!",
    "A List View with many Text - Here's more!",
    "A List View with many Text - Here's more!",
  ];

  bool confirmedWorker = false;
  double screenHeight;
  List<bool> isHighlighted = [true, false, false, true, false, false];

  TextStyle _label = TextStyle(
    fontFamily: 'opansans',
    color: Colors.blueGrey.shade700,
    fontSize: 16.0,
    fontWeight: FontWeight.w700,
    letterSpacing: 1,
  );
  TextStyle _info = TextStyle(
    fontFamily: 'opansans',
    fontSize: 15.0,
    fontWeight: FontWeight.normal,
    letterSpacing: 0.75,
  );

  showAlertDialog_submit(BuildContext context, index) {
    // set up the buttons
    Widget cancelButton = FlatButton(
      child: Text(
        "Cancel",
        style: TextStyle(letterSpacing: 1),
      ),
      onPressed: () {
        Navigator.pop(context);
      },
      color: Colors.grey,
      textColor: Colors.white,
      splashColor: Colors.grey[700],
    );
    Widget continueButton = FlatButton(
      child: Text(
        "Confirm",
        style: TextStyle(letterSpacing: 1),
      ),
      onPressed: () {
        for (int i = 0; i < isHighlighted.length; i++) {
          setState(() {
            if (index == i) {
              isHighlighted[index] = true;
            }
          });
        }
        setState(() {
          isHighlighted[index] = true;
        });
        Navigator.pop(context);
      },
      color: Colors.red,
      textColor: Colors.white,
      splashColor: Colors.red[700],
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      titlePadding: EdgeInsets.all(0),
      contentPadding: EdgeInsets.all(0),
      contentTextStyle: TextStyle(color: Colors.black, fontSize: 12, fontFamily: 'opensans'),
      title: Container(
        padding: EdgeInsets.all(10),
        decoration: BoxDecoration(color: Colors.red),
        child: Text(
          "PENGESAHAN",
          style: TextStyle(color: Colors.white, letterSpacing: 1, fontSize: 16),
          textAlign: TextAlign.center,
        ),
      ),
      content: Container(
        height: screenHeight / 2,
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              SizedBox(
                height: 18,
              ),
              Icon(
                FontAwesomeIcons.exclamationCircle,
                color: Colors.red[700],
                size: 25,
              ),
              SizedBox(
                height: 18,
              ),
              Text(
                'BAHAGIAN F',
                style: TextStyle(fontSize: 16, color: Colors.black, letterSpacing: 1, fontWeight: FontWeight.w600),
              ),
              SizedBox(
                height: 18,
              ),
              Text(
                'Dengan ini saya mengaku bahawa kerja-kerja telah selesai '
                'dijalankan mengikut skop dan memohon Permit Menjalankan '
                'Kerja (PTW) ini DIBATALKAN',
                style: TextStyle(
                  fontSize: 14,
                ),
                textAlign: TextAlign.center,
              ),
              SizedBox(height: 20),
              Container(
                margin: EdgeInsets.all(8),
                padding: EdgeInsets.all(8),
                child: Column(
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Expanded(
                            child: Text(
                          'Nama CP:',
                          style: _label,
                          textAlign: TextAlign.right,
                        )),
                        Expanded(
                            child: Text(
                          'James Dean',
                          style: _info,
                          textAlign: TextAlign.left,
                        )),
                      ],
                    ),
                    SizedBox(height: 4),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Expanded(
                            child: Text(
                          'No Pekerja :',
                          style: _label,
                          textAlign: TextAlign.right,
                        )),
                        Expanded(
                            child: Text(
                          '1000666788',
                          style: _info,
                          textAlign: TextAlign.left,
                        )),
                      ],
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
      actions: [
        cancelButton,
        continueButton,
      ],
    );
    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    screenHeight = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: AppBar(
        title: Text('Change ListView Bg Color Example'),
      ),
      body: Container(
        height: double.maxFinite,
        child: FutureBuilder(
            future: DefaultAssetBundle.of(context).loadString('data/worker.json'),
            builder: (context, snapshot) {
              var _data = jsonDecode(snapshot.data.toString());
              if (snapshot.data == null) {
                return Container(child: Center(child: Text("Loading...")));
              } else {
                return ListView(
                  children:
                      [
                        ListView.builder(
                          shrinkWrap: true,
                          itemCount:_data == null ? 0 : _data.length,//_listViewData.length,
                          itemBuilder: (context, index) => Container(
                            child: ListTile(
                              title: Row(
                                children: <Widget>[
                                  Icon(
                                    Icons.check_circle,
                                    color: isHighlighted[index] ? Colors.green : Colors.grey[300],
                                  ),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Text(
                                    _data[index]['name'],
                                    style: TextStyle(fontWeight: FontWeight.w600),
                                  ),
                                ],
                              ),
                              onTap: () => showAlertDialog_submit(context, index), // _onSelected(index),
                            ),
                          ),
                        ),
                      ],
                );
              }
            }),
      ),
    );
  }
}
