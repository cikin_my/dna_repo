import 'dart:convert';

import 'package:dna_app/Locking/form_view_verify_locking_point.dart';
import 'package:dna_app/ap_assist/locking_point/ApAssist_register_locking_point_form.dart';
import 'package:dna_app/ap_assist/locking_point/ApAssist_locking_point_list.dart';
import 'package:dna_app/ap_assist/locking_point/ApAssist_register_locking_point_submitted.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';


class ApAssist_RegisterLockingPoint extends StatefulWidget {
  @override
  _ApAssist_RegisterLockingPointState createState() => _ApAssist_RegisterLockingPointState();
}

class _ApAssist_RegisterLockingPointState extends State<ApAssist_RegisterLockingPoint> {
  /*use for Demo Execute function
============================================= */
  bool _showTextCExecuted = false;
  bool _isVisible = true;
  bool _apAssigned = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    setState(() {
//      images.add("Add Image");
    });
  }


  //  our widget ::
  //1. list of switching sequence
  Widget _buildListItem(_data) {
    return InkWell(
      onTap: () {
        Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext context) => formView_VerifyLockingPoint()));
      },
      highlightColor: Colors.white30,
      splashColor: Colors.red.shade50,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.fromLTRB(8, 8, 8, 2),
                  child: Row(
                    children: <Widget>[
                      Text(
                        "Feeder" + ":",
                        style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.w400),
                      ),
                      SizedBox(
                        width: 5,
                      ),
                      Text(
                        _data['feeder'],
                        style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.w400),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(8, 8, 8, 2),
                  child: Row(
                    children: <Widget>[
                      Text(
                        "link No" + ":",
                        style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.w400),
                      ),
                      SizedBox(
                        width: 5,
                      ),
                      Text(
                        _data['link_no'],
                        style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.w400),
                      ),
                    ],
                  ),
                ),

              ],
            ),
            Icon(Icons.keyboard_arrow_right, color: Colors.red,),
          ],
        ),
      ),
    );
  }

  //2. Details about that switching
  Widget _buildInfoContainer() {
    return Container(
      height: 250,
      margin: const EdgeInsets.all(8.0),
      padding: const EdgeInsets.all(10.0),
      decoration: BoxDecoration(
        boxShadow: [
          BoxShadow(blurRadius: 5.0, color: Colors.red[200], offset: Offset(0, 5)),
        ],
        borderRadius: BorderRadius.circular(15.0),
        gradient: LinearGradient(
          colors: [
            Color.fromRGBO(255, 137, 100, 1),
            Color.fromRGBO(255, 93, 110, 1),
          ],
        ),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            children: <Widget>[
              Text( (
                'Register Locking Point'
              ),
                style: TextStyle(fontWeight: FontWeight.bold, letterSpacing: 2, color: Colors.white, fontSize: 16, fontFamily: 'BellotaText'),
                textAlign: TextAlign.center,
              ),

            ],
            mainAxisAlignment: MainAxisAlignment.center,
          ),
          Divider(),
          SizedBox(
            height: 15,
          ),
          Row(
            children: <Widget>[
              Text(
                'AP Assist:  ',
                style: TextStyle(fontSize: 15, color: Colors.white, letterSpacing: .75),
              ),
              Text(
                'Arya Star',
                style: TextStyle(fontSize: 15, color: Colors.white, letterSpacing: .75),
              ),
            ],
          ),
          SizedBox(
            height: 5,
          ),
          Row(
            children: <Widget>[
              Text(
                'No. JS:  ',
                style: TextStyle(fontSize: 15, color: Colors.white, letterSpacing: .75),
              ),
              Text(
                'ABC-12-12345',
                style: TextStyle(fontSize: 15, color: Colors.white, letterSpacing: .75),
              ),
            ],
          ),
          SizedBox(
            height: 5,
          ),
          Row(
            children: <Widget>[
              Text(
                'Date & Time:',
                style: TextStyle(fontSize: 15, color: Colors.white, letterSpacing: .75),
              ),
              Text(
                '2-Mar-2020 18:00PM',
                style: TextStyle(fontSize: 15, color: Colors.white, letterSpacing: .75),
              ),
            ],
          ),
          SizedBox(
            height: 5,
          ),
          Text(
            'Keterangan Kerja:',
            style: TextStyle(fontSize: 16, color: Colors.white),
          ),
          Expanded(
            child: Text(
              'Preventive Maintenance PE ABC, Preventive Maintenance PE ABCPreventive Maintenance PE ABC',
              style: TextStyle(fontSize: 15, color: Colors.white, letterSpacing: .75 ),
            ),
          ),
          SizedBox(
            height: 5,
          ),
          Row(
            children: <Widget>[
              Text(
                'Punca Bekalan:',
                style: TextStyle(fontSize: 15, color: Colors.white, letterSpacing: .75),
              ),
              Text(
                'PMU AA',
                style: TextStyle(fontSize: 15, color: Colors.white, letterSpacing: .75),
              ),
            ],
          ),
          SizedBox(
            height: 5,
          ),
        ],
      ),
    );
  }

  //3. btn Confirm & submit
  Widget _buildButton() {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        RaisedButton.icon(
          onPressed: () => {
            Navigator.of(context).push(
              MaterialPageRoute(builder: (BuildContext context) => ApAssist_RegisterLockingPoint_submitted()),
            )
          },
          icon: Icon(
            Icons.send,
            size: 16,
          ),
          label: Row(
            children: <Widget>[
              Text('Submit', style: TextStyle(letterSpacing: 1)),
            ],
          ),
          color: Colors.blue,
          splashColor: Colors.blue[700],
          textColor: Colors.white,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
        ),
      ],
    );
  }


  @override
  Widget build(BuildContext context) {
    //    text styling ::
    TextStyle _style1 = TextStyle(
      fontFamily: 'opansans',
      color: Colors.blueGrey.shade700,
      fontSize: 15.0,
      fontWeight: FontWeight.w700,
    );
    TextStyle _style2 = TextStyle(
      fontFamily: 'opansans',
      color: Colors.blueGrey.shade700,
      fontSize: 13.0,
      fontWeight: FontWeight.w700,
    );
    TextStyle _style3 = TextStyle(
      fontFamily: 'opansans',
      fontSize: 13.0,
    );

    return Scaffold(
      appBar: AppBar(
        title: Text(
          "DNSC",
          style: TextStyle(
            letterSpacing: 1,
            fontFamily: 'Lato',
          ),
        ),
        centerTitle: true,
        leading: Builder(
          builder: (BuildContext context) {
            return IconButton(
              icon: const Icon(Icons.arrow_back),
              onPressed: () {
                Navigator.of(context).push(
                  MaterialPageRoute(builder: (BuildContext context) => ApAssist_LockingPoint()),
                );
              },
            );
          },
        ),
      ),
      body: Container(
        height: double.maxFinite,
        child: Stack(
          children: <Widget>[
            FutureBuilder(
                future: DefaultAssetBundle.of(context).loadString('data/LockingPointData.json'),
                builder: (context, snapshot) {
                  var _data = jsonDecode(snapshot.data.toString());
                  if (snapshot.data == null) {
                    return Container(child: Center(child: Text("Loading...")));
                  } else {
                    return ListView(
                      children: <Widget>[
                        _buildInfoContainer(),
                        Padding(
                          padding: const EdgeInsets.fromLTRB(18,30,18,0),
                          child: Column(
                            children: <Widget>[
                              Row(
                                children: <Widget>[
                                  Text('Locking Point(s)', style: TextStyle(letterSpacing: 1, color: Colors.blueGrey[800], fontSize: 16),),
                                  RaisedButton.icon(
                                      color: Colors.blue,
                                      splashColor: Colors.blue[700],
                                      textColor: Colors.white,
                                      onPressed: () {
                                        Navigator.of(context).push(
                                          MaterialPageRoute(builder: (BuildContext context) => ApAssist_RegisterLockingPoint_form()),
                                        );
                                      },
                                      icon:Icon(Icons.add),
                                      label: Text('Add', style: TextStyle(letterSpacing: 2),))
                                ],
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              ),

                            ],
                            crossAxisAlignment: CrossAxisAlignment.start,
                          ),
                        ),
                        ListView.builder(
                          physics: ScrollPhysics(),
                          shrinkWrap: true,
                          padding: EdgeInsets.all(8.0),
                          itemCount: _data == null ? 0 : _data.length,
                          itemBuilder: (BuildContext context, int index) {
                            return Container(
                              decoration: BoxDecoration(
                                border: Border(bottom: BorderSide(color: Colors.grey.shade200)),
                              ),
//                              child: _buildListItem(_data[index]),
                            );
                          },
                        ),

                      ],
                    );
                  }
                }),
          ],
        ),
      ),
    );
  }
}