import 'dart:io';

import 'package:dna_app/ap_assist/locking_point/ApAssist_locking_point_list.dart';
import 'package:dna_app/ap_assist/locking_point/ApAssist_register_locking_point.dart';
import 'package:dna_app/ap_assist/locking_point/ApAssist_register_locking_point_form.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:image_picker/image_picker.dart';

class ApAssist_RegisterLockingPoint_submitted extends StatefulWidget {
  @override
  _ApAssist_RegisterLockingPoint_submittedState createState() => _ApAssist_RegisterLockingPoint_submittedState();
}

class _ApAssist_RegisterLockingPoint_submittedState extends State<ApAssist_RegisterLockingPoint_submitted> {


  TextStyle _label = TextStyle(
    fontFamily: 'opansans',
    color: Colors.blueGrey.shade700,
    fontSize: 16.0,
    fontWeight: FontWeight.w700,
    letterSpacing: 1,
  );
  TextStyle _input = TextStyle(
    fontFamily: 'opansans',
    color: Colors.black87,
    fontSize: 16.0,
    fontWeight: FontWeight.w400,
    letterSpacing: 1,
  );



  Widget _infoContainer() {
    return Container(
      height: 250,
      margin: const EdgeInsets.all(8.0),
      padding: const EdgeInsets.all(10.0),
      decoration: BoxDecoration(
        boxShadow: [
          BoxShadow(blurRadius: 5.0, color: Colors.red[200], offset: Offset(0, 5)),
        ],
        borderRadius: BorderRadius.circular(15.0),
        gradient: LinearGradient(
          colors: [
            Color.fromRGBO(255, 137, 100, 1),
            Color.fromRGBO(255, 93, 110, 1),
          ],
        ),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            children: <Widget>[
              Text( (
                  'Register Locking Point'
              ),
                style: TextStyle(fontWeight: FontWeight.bold, letterSpacing: 2, color: Colors.white, fontSize: 16, fontFamily: 'BellotaText'),
                textAlign: TextAlign.center,
              ),

            ],
            mainAxisAlignment: MainAxisAlignment.center,
          ),
          Divider(),
          SizedBox(
            height: 15,
          ),
          Row(
            children: <Widget>[
              Text(
                'AP Assist:  ',
                style: TextStyle(fontSize: 15, color: Colors.white, letterSpacing: .75),
              ),
              Text(
                'Arya Star',
                style: TextStyle(fontSize: 15, color: Colors.white, letterSpacing: .75),
              ),
            ],
          ),
          SizedBox(
            height: 5,
          ),
          Row(
            children: <Widget>[
              Text(
                'No. JS:  ',
                style: TextStyle(fontSize: 15, color: Colors.white, letterSpacing: .75),
              ),
              Text(
                'ABC-12-12345',
                style: TextStyle(fontSize: 15, color: Colors.white, letterSpacing: .75),
              ),
            ],
          ),
          SizedBox(
            height: 5,
          ),
          Row(
            children: <Widget>[
              Text(
                'Date & Time:',
                style: TextStyle(fontSize: 15, color: Colors.white, letterSpacing: .75),
              ),
              Text(
                '2-Mar-2020 18:00PM',
                style: TextStyle(fontSize: 15, color: Colors.white, letterSpacing: .75),
              ),
            ],
          ),
          SizedBox(
            height: 5,
          ),
          Text(
            'Keterangan Kerja:',
            style: TextStyle(fontSize: 16, color: Colors.white),
          ),
          Expanded(
            child: Text(
              'Preventive Maintenance PE ABC, Preventive Maintenance PE ABCPreventive Maintenance PE ABC',
              style: TextStyle(fontSize: 15, color: Colors.white, letterSpacing: .75 ),
            ),
          ),
          SizedBox(
            height: 5,
          ),
          Row(
            children: <Widget>[
              Text(
                'Punca Bekalan:',
                style: TextStyle(fontSize: 15, color: Colors.white, letterSpacing: .75),
              ),
              Text(
                'PMU AA',
                style: TextStyle(fontSize: 15, color: Colors.white, letterSpacing: .75),
              ),
            ],
          ),
          SizedBox(
            height: 5,
          ),
        ],
      ),
    );
  }
  Widget _formView(){
    return Container(
      padding: EdgeInsets.all(20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text('Operasi', style: _label,),
          Text('a) VCB/OCB: Open/Rack', style: _input,),
          SizedBox(height: 18,),

          Text('Feeder/CB#', style: _label,),
          Text('XYZABC1230000', style: _input,),
          SizedBox(height: 18,),

          Text('No. Link', style: _label,),
          Text('CDMX10600890', style: _input,),
          SizedBox(height: 18,),
          Row(
            children: <Widget>[
              Text(
                'Voltage',
                style:_label,
              ),
              SizedBox(
                width: 8,
              ),
              Text(
                '11KV',
                style: new TextStyle(fontSize: 14.0),
              ),

            ],
          ),

          Text('Location', style: _label,),
          Row(
            children: <Widget>[
              Expanded(
                child:  Text('3.6337865, 101.5611392', style: _input,),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child:IconButton(
                  icon: Icon(Icons.pin_drop, color: Colors.red[600],size: 38,),
                  tooltip: 'Increase volume by 10',
                  onPressed: () {
                    setState(() {

                    });
                  },
                ),
              ),
            ],
          ),
          SizedBox(height: 18,),
          Text('Attach Image', style: _label,),
          Row(
            children: <Widget>[
              Expanded(
                  child:InkWell(
                    hoverColor: Colors.red,
                    focusColor: Colors.red,
                    splashColor: Colors.red,
                    child: Container(
                      padding: EdgeInsets.all(18),
                      child: _image == null
                          ? Image.asset('assets/images/addphoto.png')
                          : Image.file(_image, ),
                    ),
                    onTap: getImage,
                  ),
              ),

            ],
          ),



        ],
      ),
    );
  }

  /*
  * =====================================================
  * Image Picker
  * =====================================================
  * */
  File _image;
  final picker = ImagePicker();

  Future getImage() async {
    final pickedFile = await picker.getImage(source: ImageSource.camera);

    setState(() {
      _image = File(pickedFile.path);
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "DNSC",
          style: TextStyle(
            letterSpacing: 1,
            fontFamily: 'Lato',
          ),
        ),
        centerTitle: true,
        leading: Builder(
          builder: (BuildContext context) {
            return IconButton(
              icon: const Icon(Icons.arrow_back),
              onPressed: () {
                Navigator.of(context).push(
                  MaterialPageRoute(builder: (BuildContext context) => ApAssist_RegisterLockingPoint()),
                );
              },
            );
          },
        ),
      ),
      body: ListView(
          children: <Widget>[
            _infoContainer(),
            _formView(),
            Container(
              width: double.infinity,
              color: Colors.blue[50],
              child: Center(child: Column(
                children: <Widget>[
                  SizedBox(height: 10,),
                  Text('Locking Point Submitted on ', style: TextStyle(color: Colors.blueGrey[700],fontStyle: FontStyle.italic,fontSize: 15,fontWeight: FontWeight.w600,),),
                  Text('1-Mar-2020 18:30PM', style: TextStyle(color: Colors.blueGrey[700],fontStyle: FontStyle.italic,fontSize: 15,fontWeight: FontWeight.w600),),
                  SizedBox(height: 20,),
                ],
              )),
            ),
            Container(
              width: double.infinity,
              color: Colors.green[100],
              child: Center(child: Column(
                children: <Widget>[
                  SizedBox(height: 10,),
                  Text('Locking Point Verified on', style: TextStyle(color: Colors.blueGrey[700],fontStyle: FontStyle.italic,fontSize: 15,fontWeight: FontWeight.w600,),),
                  Text('1-Mar-2020 18:30PM', style: TextStyle(color: Colors.blueGrey[700],fontStyle: FontStyle.italic,fontSize: 15,fontWeight: FontWeight.w600),),
                  SizedBox(height: 20,),
                ],
              )),
            ),
            Container(
              width: double.infinity,
              color: Colors.red[100],
              child: Center(child: Column(
                children: <Widget>[
                  SizedBox(height: 10,),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text('Locking Point Rejected', style: TextStyle(color: Colors.red[700],fontStyle: FontStyle.italic,fontSize: 15,fontWeight: FontWeight.w600,),),
                      SizedBox(width: 4),
                      IconButton(
                        icon: Icon(Icons.info, color: Colors.red[700],) ,
                        onPressed: null,
                      ),

                    ],
                  ),
                  RaisedButton.icon(
                    onPressed: () => {
                      Navigator.of(context).push(
                        MaterialPageRoute(builder: (BuildContext context) => ApAssist_RegisterLockingPoint_form()),
                      )
                    },
                    icon: Icon(
                      Icons.edit,
                      size: 16,
                    ),
                    label: Row(
                      children: <Widget>[
                        Text('Edit', style: TextStyle(letterSpacing: 1)),
                      ],
                    ),
                    color: Colors.red,
                    splashColor: Colors.red[700],
                    textColor: Colors.white,
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
                  ),
                  SizedBox(height: 20,),
                ],
              )),
            ),

          ],
      ),


    );
  }
}
