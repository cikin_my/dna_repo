import 'dart:io';

import 'package:dna_app/ap_assist/locking_point/ApAssist_locking_point_list.dart';
import 'package:dna_app/ap_assist/locking_point/ApAssist_register_locking_point.dart';
import 'package:dna_app/ap_assist/locking_point/ApAssist_register_locking_point_submitted.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:image_picker/image_picker.dart';

class ApAssist_RegisterLockingPoint_form extends StatefulWidget {
  @override
  _ApAssist_RegisterLockingPoint_formState createState() => _ApAssist_RegisterLockingPoint_formState();
}

class _ApAssist_RegisterLockingPoint_formState extends State<ApAssist_RegisterLockingPoint_form> {
  String value;
  int _radioValue1 = -1;

  TextStyle _label = TextStyle(
    fontFamily: 'opansans',
    color: Colors.blueGrey.shade700,
    fontSize: 16.0,
    fontWeight: FontWeight.w700,
    letterSpacing: 1,
  );

  void _handleRadioValueChange1(int value) {
    setState(() {
      _radioValue1 = value;

      switch (_radioValue1) {
        case 0:
          Fluttertoast.showToast(
              msg: "11KV",
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.CENTER,
              timeInSecForIosWeb: 1,
              backgroundColor: Colors.orange,
              textColor: Colors.white,
              fontSize: 14.0);
          break;
        case 1:
          Fluttertoast.showToast(
              msg: "33KV",
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.CENTER,
              timeInSecForIosWeb: 1,
              backgroundColor: Colors.orange,
              textColor: Colors.white,
              fontSize: 14.0);
          break;
      }
    });
  }

  Widget _buildButton() {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        RaisedButton.icon(
          onPressed: () => {
            Navigator.of(context).push(
              MaterialPageRoute(builder: (BuildContext context) => ApAssist_RegisterLockingPoint_submitted()),
            )
          },
          icon: Icon(
            Icons.send,
            size: 16,
          ),
          label: Row(
            children: <Widget>[
              Text('Submit', style: TextStyle(letterSpacing: 1)),
            ],
          ),
          color: Colors.blue,
          splashColor: Colors.blue[700],
          textColor: Colors.white,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
        ),
      ],
    );
  }

  Widget _infoContainer() {
    return Container(
      height: 250,
      margin: const EdgeInsets.all(8.0),
      padding: const EdgeInsets.all(10.0),
      decoration: BoxDecoration(
        boxShadow: [
          BoxShadow(blurRadius: 5.0, color: Colors.red[200], offset: Offset(0, 5)),
        ],
        borderRadius: BorderRadius.circular(15.0),
        gradient: LinearGradient(
          colors: [
            Color.fromRGBO(255, 137, 100, 1),
            Color.fromRGBO(255, 93, 110, 1),
          ],
        ),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            children: <Widget>[
              Text(
                ('Register Locking Point'),
                style: TextStyle(fontWeight: FontWeight.bold, letterSpacing: 2, color: Colors.white, fontSize: 16, fontFamily: 'BellotaText'),
                textAlign: TextAlign.center,
              ),
            ],
            mainAxisAlignment: MainAxisAlignment.center,
          ),
          Divider(),
          SizedBox(
            height: 15,
          ),
          Row(
            children: <Widget>[
              Text(
                'AP Assist:  ',
                style: TextStyle(fontSize: 15, color: Colors.white, letterSpacing: .75),
              ),
              Text(
                'Arya Star',
                style: TextStyle(fontSize: 15, color: Colors.white, letterSpacing: .75),
              ),
            ],
          ),
          SizedBox(
            height: 5,
          ),
          Row(
            children: <Widget>[
              Text(
                'No. JS:  ',
                style: TextStyle(fontSize: 15, color: Colors.white, letterSpacing: .75),
              ),
              Text(
                'ABC-12-12345',
                style: TextStyle(fontSize: 15, color: Colors.white, letterSpacing: .75),
              ),
            ],
          ),
          SizedBox(
            height: 5,
          ),
          Row(
            children: <Widget>[
              Text(
                'Date & Time:',
                style: TextStyle(fontSize: 15, color: Colors.white, letterSpacing: .75),
              ),
              Text(
                '2-Mar-2020 18:00PM',
                style: TextStyle(fontSize: 15, color: Colors.white, letterSpacing: .75),
              ),
            ],
          ),
          SizedBox(
            height: 5,
          ),
          Text(
            'Keterangan Kerja:',
            style: TextStyle(fontSize: 16, color: Colors.white),
          ),
          Expanded(
            child: Text(
              'Preventive Maintenance PE ABC, Preventive Maintenance PE ABCPreventive Maintenance PE ABC',
              style: TextStyle(fontSize: 15, color: Colors.white, letterSpacing: .75),
            ),
          ),
          SizedBox(
            height: 5,
          ),
          Row(
            children: <Widget>[
              Text(
                'Punca Bekalan:',
                style: TextStyle(fontSize: 15, color: Colors.white, letterSpacing: .75),
              ),
              Text(
                'PMU AA',
                style: TextStyle(fontSize: 15, color: Colors.white, letterSpacing: .75),
              ),
            ],
          ),
          SizedBox(
            height: 5,
          ),
        ],
      ),
    );
  }

  Widget _formView() {
    return Container(
      padding: EdgeInsets.all(20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            'Operasi',
            style: _label,
          ),
          DropdownButton(
              isExpanded: true,
              value: value,
              hint: Text("Select Operasi"),
              items: [
                DropdownMenuItem(
                  child: Text("a) VCB/OCB: Open/Rac"),
                  value: " 1",
                ),
                DropdownMenuItem(
                  child: Text("b) VCB/OCB: Open/Rac"),
                  value: " 2",
                )
              ],
              onChanged: (newValue) {
                print(newValue);
                setState(() {
                  value = newValue;
                });
              }),
          SizedBox(
            height: 18,
          ),
          Text(
            'Feeder/CB#',
            style: _label,
          ),
          TextField(
            decoration: InputDecoration(
              border: OutlineInputBorder(),
            ),
          ),
          SizedBox(
            height: 18,
          ),
          Text(
            'No. Link',
            style: _label,
          ),
          TextField(
            decoration: InputDecoration(
              border: OutlineInputBorder(),
            ),
          ),
          SizedBox(
            height: 18,
          ),
          Row(
            children: <Widget>[
              Text(
                'Voltage',
                style: _label,
              ),
              SizedBox(
                width: 8,
              ),
              Radio(
                value: 0,
                groupValue: _radioValue1,
                onChanged: _handleRadioValueChange1,
              ),
              Text(
                '11KV',
                style: new TextStyle(fontSize: 14.0),
              ),
              Radio(
                value: 1,
                groupValue: _radioValue1,
                onChanged: _handleRadioValueChange1,
              ),
              Text(
                '33KV',
                style: new TextStyle(
                  fontSize: 14.0,
                ),
              ),
            ],
          ),
          Text(
            'Location',
            style: _label,
          ),
          Row(
            children: <Widget>[
              Expanded(
                child: TextField(
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: IconButton(
                  icon: Icon(
                    Icons.pin_drop,
                    color: Colors.red[600],
                    size: 38,
                  ),
                  tooltip: 'Increase volume by 10',
                  onPressed: () {
                    setState(() {});
                  },
                ),
              ),
            ],
          ),
          SizedBox(
            height: 18,
          ),
          Text('Attach Image', style: _label,),
          Row(
            children: <Widget>[
              Expanded(
                child: InkWell(
                  hoverColor: Colors.red,
                  focusColor: Colors.red,
                  splashColor: Colors.red,
                  child: Container(
                    padding: EdgeInsets.all(18),
                    child: _image == null
                        ? Image.asset('assets/images/addphoto.png')
                        : Image.file( _image, ),
                  ),
                  onTap: getImage,
                ),
              ),
            ],
          ),
          SizedBox(
            height: 18,
          ),
          Padding(
            padding: const EdgeInsets.only(left: 18.0, right: 18),
            child: Divider(),
          ),
          _buildButton(),
        ],
      ),
    );
  }

  /*
  * =====================================================
  * Image Picker
  * =====================================================
  * */
  File _image;
  final picker = ImagePicker();

  Future getImage() async {
    final pickedFile = await picker.getImage(source: ImageSource.camera);

    setState(() {
      _image = File(pickedFile.path);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "DNSC",
          style: TextStyle(
            letterSpacing: 1,
            fontFamily: 'Lato',
          ),
        ),
        centerTitle: true,
        leading: Builder(
          builder: (BuildContext context) {
            return IconButton(
              icon: const Icon(Icons.arrow_back),
              onPressed: () {
                Navigator.of(context).push(
                  MaterialPageRoute(builder: (BuildContext context) => ApAssist_RegisterLockingPoint_form()),
                );
              },
            );
          },
        ),
      ),
      body: ListView(
        children: <Widget>[
          _infoContainer(),
          _formView(),
        ],
      ),
    );
  }
}

//dropdown Class
class Item {
  const Item(this.name,this.icon);
  final String name;
  final Icon icon;
}



