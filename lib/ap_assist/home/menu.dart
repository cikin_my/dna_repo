
import 'package:dna_app/ap_assist/PTW/OkstQp_Ptw_list.dart';
import 'package:dna_app/ap_assist/locking_point/ApAssist_locking_point_list.dart';
import 'package:dna_app/ap_assist/operation/listview.dart';
import 'package:flutter/material.dart';

class Menu extends StatefulWidget {
  @override
  _MenuState createState() => _MenuState();
}

class _MenuState extends State<Menu> {
  List<String> events = [
    "Operation",
    "Locking Points",
    "Permit to Work",
    "View Log",
  ];

  @override
  Widget build(BuildContext context) {
    var color = 0xFFF5F5F5;
    return Flexible(
      child: GridView(
        padding: EdgeInsets.only(left: 16, right: 16),
        physics: BouncingScrollPhysics(), //only for ios
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
        children: events.map((url) {
          return GestureDetector(
            child: Container(
              decoration: BoxDecoration(color: Color(color), borderRadius: BorderRadius.circular(10), boxShadow: [
                BoxShadow(
                  color: Colors.grey[200],
                  blurRadius: 2.0, // has the effect of softening the shadow
                  spreadRadius: 1.0, // has the effect of extending the shadow
                  offset: Offset(
                    3.0, // horizontal, move right 10
                    3.0, // vertical, move down 10
                  ),
                )
              ]),
              margin: const EdgeInsets.all(10.0),
              child: getCardByTitle(url),
            ),
            onTap: () {
              Navigator.of(context).pop();
              switch (url) {
                case "Operation":
                  {
                   Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context) => ListviewJobsheet()));
                  }
                  break;

                case "Locking Points":
                  {
                    Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context) => ApAssist_LockingPoint()));
                  }
                  break;
                case "Permit to Work":
                  {
                    Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context) => OkstOp_PTW_List()));
                  }
                  break;
                case "View Log":
                  {
//                    Navigator.of(context).push(new MaterialPageRoute(
//                        builder: (BuildContext context) =>
//                        new ViewLog_Operation()));
                  }
                  break;

              }
              if (url == "View Log") {}

//                print(url);
            },
          );
        }).toList(),
      ),
    );
  }
}

Column getCardByTitle(String url) {
  String img = "";
  String title = "";
  if (url == "Operation") {
    img = "assets/images/gears.png";
    title = "Operation";
  } else if (url == "Locking Points") {
    img = "assets/images/scheme.png";
    title = "Locking Points";
  } else if (url == "Permit to Work") {
    img = "assets/images/clipboard.png";
    title = "Permit to Work";
  } else {
    img = "assets/images/tablet.png";
    title = "View Log";
  }

  return Column(
    mainAxisAlignment: MainAxisAlignment.center,
    children: <Widget>[
      Image.asset(
        img,
        width: 45,
      ),
      SizedBox(
        height: 10,
      ),
      Text(
        title,
        style: TextStyle(color: Colors.black87, fontSize: 16, fontWeight: FontWeight.w600),
      ),
//      SizedBox(
//        height: 8,
//      ),
//      Text(
//        subtitle,
//        style: TextStyle(
//            color: Colors.black45, fontSize: 12, fontWeight: FontWeight.w600),
//      ),
    ],
  );
}
