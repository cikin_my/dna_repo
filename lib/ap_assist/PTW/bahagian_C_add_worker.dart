import 'package:dna_app/ap_assist/PTW/bahagian_C_1.dart';
import 'package:dna_app/ap_assist/PTW/bahagian_C_2.dart';
import 'package:dna_app/ap_assist/PTW/form.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

class BahagianC_AddWorker extends StatefulWidget {
  @override
  _BahagianC_AddWorkerState createState() => _BahagianC_AddWorkerState();
}

class _BahagianC_AddWorkerState extends State<BahagianC_AddWorker> {
  TextStyle _label = TextStyle(
    fontFamily: 'opansans',
    color: Colors.blueGrey.shade700,
    fontSize: 16.0,
    fontWeight: FontWeight.w700,
    letterSpacing: 1,
  );
  TextStyle _info = TextStyle(
    fontFamily: 'opansans',
    fontSize: 15.0,
    fontWeight: FontWeight.normal,
    letterSpacing: 0.75,
  );

  String dropdownValue = 'Sila pilih Nama Pekerja';

  //dump here in case u need this buddy:
  int _radioValue1 = 0;

  void _handleRadioValueChange1(int value) {
    setState(() {
      _radioValue1 = value;

      switch (_radioValue1) {
        case 0:
          Fluttertoast.showToast(
              msg: "LOTO Box",
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.CENTER,
              timeInSecForIosWeb: 1,
              backgroundColor: Colors.orange,
              textColor: Colors.white,
              fontSize: 14.0);
          break;
        case 1:
          Fluttertoast.showToast(
              msg: "HASP Lock",
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.CENTER,
              timeInSecForIosWeb: 1,
              backgroundColor: Colors.orange,
              textColor: Colors.white,
              fontSize: 14.0);
          break;
      }
    });
  }

  Widget _skopKerja(){
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text('SKOP KERJA', style: TextStyle(fontSize: 18, fontWeight: FontWeight.w700)),
          ],
        ),
        Text(
          'Nama dan No Suis (PMU/PPU/PE/PAT/BLACKBOX/FEEDER PILLAR/ LVDB) : ',
          style: _label,
        ),
        Container(
            width: double.infinity,
            padding: EdgeInsets.all(10),
            color: Colors.grey[200],
            child: Text(
              'RSTL-NE-012345678',
              style: _info,
            )),
        SizedBox(
          height: 10,
        ),
        Text(
          'Radas di mana kerja dibuat',
          style: _label,
        ),
        Container(
          width: double.infinity,
          padding: EdgeInsets.all(10),
          color: Colors.grey[200],
          child: Text(
            'CBT-00765999',
            style: _info,
          ),
        ),
        SizedBox(
          height: 10,
        ),
        Text(
          'Jenis kerja ynag dijalankan',
          style: _label,
        ),
        Container(
          width: double.infinity,
          padding: EdgeInsets.all(10),
          color: Colors.grey[200],
          child: Text(
            'Maintenance',
            style: _info,
          ),
        ),
        SizedBox(
          height: 10,
        ),
        Row(
          children: <Widget>[
            Text(
              'Jenis Kunci',
              style: _label,
            ),
            SizedBox(
              width: 8,
            ),
            Radio(
              value: 0,
              groupValue: _radioValue1,
              onChanged: null,
            ),
            Text(
              'LOTO box',
              style: new TextStyle(fontSize: 14.0),
            ),
            Radio(
              value: 1,
              groupValue: _radioValue1,
              onChanged: null,
            ),
            Text(
              'HASP Lock',
              style: new TextStyle(
                fontSize: 14.0,
              ),
            ),
          ],
        ),
        SizedBox(
          height: 10,
        ),
        /*=== GAMBAR ==== */
        Divider(),
        Text('Gambar Lampiran', style: _label,),
        SizedBox( height: 10,),
        Row(
          children: <Widget>[
            Image.asset('assets/images/sampleimage_a.jpg',width:150 ),
            SizedBox(
              width: 10,
            ),
            Image.asset('assets/images/sampleimage_b.jpg',width:150 ),
          ],
        ),
        /*-----------------*/
        SizedBox(height: 20,),
        Text('Tempat Pembumian Tambahan (jika ada)',style: _label,),
        TextField(
          decoration: InputDecoration(
            border: OutlineInputBorder(),
          ),
        ),
        SizedBox(height: 10,),
        Text('Tempat Kunci NSL Tambahan (jika ada)',style: _label,),
        TextField(
          decoration: InputDecoration(
            border: OutlineInputBorder(),
          ),
        ),
        SizedBox(height: 20,),
      ],
    );
  }
  Widget _perakuan(){
    return Column(
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text('PERAKUAN', style: TextStyle(fontSize: 18, fontWeight: FontWeight.w700)),
          ],
        ),
        Container(
          padding: EdgeInsets.all(8),
          decoration: BoxDecoration(border: Border.all(width: 1, color: Colors.blueGrey)),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[

              Text(
                'Saya yang bekerja di bawah penyeliaan Orang Berkecekapan mengaku terima dan paham perkara berikut:-',
              ),
              SizedBox(height: 18,),
              Container(
                margin: EdgeInsets.all(20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text('(i) Skop kerja yang dibenarkan, ', style: TextStyle(color: Colors.blueGrey[600],letterSpacing: 1 ),),
                    Text('(ii) Tempat Pembumian Utama dan Tambahan Litar dibuat,', style: TextStyle(color: Colors.blueGrey[600], letterSpacing: 1 ),),
                    Text('(iii) Memakai Kelengkapan Perlindungan Diri semasa bekerja', style: TextStyle(color: Colors.blueGrey[600], letterSpacing: 1),),
                  ],
                ),
              ),

            ],
          ),
        ),
      ],
    );
  }
  Widget _btnSignIn(){
    return  RaisedButton.icon(
      onPressed: () =>
      {
        setState(() {
          Navigator.of(context).push(
            MaterialPageRoute(builder: (BuildContext context) => BahagianC2()),
          );
        }),
      },
      icon: Icon(
        Icons.check,
        size: 16,
      ),
      label: Row(
        children: <Widget>[
          Text('Sign In ', style: TextStyle(letterSpacing: 1)),
        ],
      ),
      color: Colors.blue,
      splashColor: Colors.blue[700],
      textColor: Colors.white,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Tambah Pekerja & Log Masuk",
          style: TextStyle(
            letterSpacing: 1,
          ),
        ),
        centerTitle: true,
        leading: Builder(
          builder: (BuildContext context) {
            return IconButton(
              icon: const Icon(Icons.arrow_back),
              onPressed: () {
                Navigator.of(context).push(
                  MaterialPageRoute(builder: (BuildContext context) => Bahagian_C1()),
                );
              },
            );
          },
        ),
      ),
      body: Container(
        padding: EdgeInsets.all(20),
        child: ListView(
          children: <Widget>[
            Text(
              'Nama Pekerja : ',
              style: _label,
            ),
            DropdownButtonHideUnderline(
              child: Container(
                padding: EdgeInsets.only(left: 10, right: 10),
                width: double.infinity,
                decoration: BoxDecoration(
                  border: Border.all(color: Colors.blue[700], width: 1.5),
                ),
                child: DropdownButton<String>(
                  isExpanded: true,
                  value: dropdownValue,
                  icon: Icon(Icons.keyboard_arrow_down, color: Colors.blue[700]),
                  iconSize: 24,
                  elevation: 16,
                  onChanged: (String newValue) {
                    setState(() {
                      dropdownValue = newValue;
                    });
                  },
                  items: <String>['Sila pilih Nama Pekerja', 'Abdul Wahab (N/P: 10066564)', 'Abdul Manaf (N/P: 10066565)', 'Abdul Mansoor (N/P: 10066566)']
                      .map<DropdownMenuItem<String>>((String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: Text(value),
                    );
                  }).toList(),
                ),
              ),
            ),
            Divider(),
            _skopKerja(),
            _perakuan(),
            _btnSignIn(),
          ],
        ),
      ),
    );
  }
}
