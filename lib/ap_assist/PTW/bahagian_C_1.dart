import 'dart:convert';
import 'package:dna_app/ap_assist/PTW/bahagian_C_add_worker.dart';
import 'package:dna_app/ap_assist/PTW/bahagian_C_2.dart';
import 'package:dna_app/ap_assist/PTW/form.dart';
import 'package:dna_app/main.dart';
import 'package:flutter/material.dart';

class Bahagian_C1 extends StatefulWidget {
  @override
  _Bahagian_C1State createState() => _Bahagian_C1State();
}

class _Bahagian_C1State extends State<Bahagian_C1> {
  TextStyle _label = TextStyle(
    fontFamily: 'opansans',
    color: Colors.blueGrey.shade700,
    fontSize: 16.0,
    fontWeight: FontWeight.w700,
    letterSpacing: 1,
  );
  TextStyle _info = TextStyle(
    fontFamily: 'opansans',
    fontSize: 15.0,
    fontWeight: FontWeight.normal,
    letterSpacing: 0.75,
  );
  double screenHeight;

  bool _checkbox_item_1 = false;
  bool _checkbox_item_2 = false;
  bool _checkbox_item_3 = false;
  bool _checkbox_item_4 = false;
  bool _checkbox_item_5 = false;

  Widget _buttonNext() {
    return Container(
      padding: EdgeInsets.all(20),
      child: RaisedButton.icon(
        onPressed: () => {
          Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext context) => BahagianC2())),
        },
        icon: Icon(
          Icons.arrow_forward,
          size: 16,
        ),
        label: Row(
          children: <Widget>[
            Text('Next', style: TextStyle(letterSpacing: 1)),
          ],
        ),
        color: Colors.blue,
        splashColor: Colors.blue[700],
        textColor: Colors.white,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
      ),
    );
  }

  Widget _bahagian_C_1() {
    return Container(
      child: Column(
        children: <Widget>[
          SizedBox(
            height: 20,
          ),
          Text(
            'BAHAGIAN C',
            style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold, letterSpacing: 1),
            textAlign: TextAlign.center,
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(18, 25, 18, 0),
            child: Column(
              children: <Widget>[
                Text('(1) Orang Berkecekapan', style: _label),
                Container(
                  padding: EdgeInsets.all(8),
                  child: Text(
                    'Saya menerima Permit Menjalankan Kerja ini mengaku terima dan faham perkara berikut:.',
                    style: TextStyle(fontSize: 16),
                    textAlign: TextAlign.center,
                  ),
                ),
                Container(
                  child: Column(
                    children: <Widget>[
                      CheckboxListTile(
                        title: Text('Skop kerja yang dibenarkan'),
                        controlAffinity: ListTileControlAffinity.platform,
                        value: _checkbox_item_1,
                        onChanged: (bool value) {
                          setState(() {
                            _checkbox_item_1 = value;
                          });
                        },
                      ),
                      CheckboxListTile(
                        title: Text('Tempat Pembumian Utama Litar'),
                        controlAffinity: ListTileControlAffinity.platform,
                        value: _checkbox_item_2,
                        onChanged: (bool value) {
                          setState(() {
                            _checkbox_item_2 = value;
                          });
                        },
                      ),
                      CheckboxListTile(
                        title: Text('Memakai Kelengkapan Perlindungan Diri Lengkap'),
                        controlAffinity: ListTileControlAffinity.platform,
                        value: _checkbox_item_3,
                        onChanged: (bool value) {
                          setState(() {
                            _checkbox_item_3 = value;
                          });
                        },
                      ),
                      CheckboxListTile(
                        title: Text('Tempat Pembumian Tambahan (jika ada)'),
                        controlAffinity: ListTileControlAffinity.platform,
                        value: _checkbox_item_4,
                        onChanged: (bool value) {
                          setState(() {
                            _checkbox_item_4 = value;
                          });
                        },
                      ),
                      Visibility(
                        visible: _checkbox_item_4,
                        child: TextField(
                          decoration: InputDecoration(
                            border: OutlineInputBorder(),
                            labelText: 'Nyatakan',
                            labelStyle: _label,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      CheckboxListTile(
                        title: Text('Tempat Kunci NSL Tambahan (jika ada)'),
                        controlAffinity: ListTileControlAffinity.platform,
                        value: _checkbox_item_5,
                        onChanged: (bool value) {
                          setState(() {
                            _checkbox_item_5 = value;
                          });
                        },
                      ),
                      Visibility(
                        visible: _checkbox_item_5,
                        child: TextField(
                          decoration: InputDecoration(
                            border: OutlineInputBorder(),
                            labelText: 'Nyatakan',
                            labelStyle: _label,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.all(18),
            padding: EdgeInsets.all(18),
            decoration: BoxDecoration(color: Colors.grey[200], border: Border.all(width: 1, color: Colors.blueGrey)),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[Text('Nama Orang Berkecekapan: '), Text(' Howard Wolowitz')],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[Text('No. Pekerja:'), Text('10055567')],
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    screenHeight = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Bahagian C",
          style: TextStyle(
            letterSpacing: 1,
            fontFamily: 'Lato',
          ),
        ),
        centerTitle: true,
        leading: Builder(
          builder: (BuildContext context) {
            return IconButton(
              icon: const Icon(Icons.arrow_back),
              onPressed: () {
                Navigator.of(context).push(
                  MaterialPageRoute(builder: (BuildContext context) => PTW_Form()),
                );
              },
            );
          },
        ),
      ),
      body: ListView(
        children: <Widget>[
          Container(
            height: screenHeight / 5,
            margin: const EdgeInsets.all(8.0),
            padding: const EdgeInsets.all(10.0),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(15.0),
              color: Colors.grey[100],
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Text(
                      'No. Permit : ',
                      style: _label,
                    ),
                    Text('C00018000901', style: _info),
                  ],
                ),
                SizedBox(
                  height: 5,
                ),
                Text(
                  'Keterangan Kerja',
                  style: _label,
                ),
                Text(
                  'PREVENTION MAINTENANCE VCB DAN BUSBAR- TOTAL SHUTDOWN',
                  style: _info,
                ),
                SizedBox(
                  height: 5,
                ),
                Row(
                  children: <Widget>[
                    Text(
                      'Jenis Radas : ',
                      style: _label,
                    ),
                    Text('BUSBAR', style: _info),
                  ],
                ),
              ],
            ),
          ),
          _bahagian_C_1(),
          SizedBox(
            height: 30,
          ),
          _buttonNext(),
        ],
      ),
    );
  }
}
