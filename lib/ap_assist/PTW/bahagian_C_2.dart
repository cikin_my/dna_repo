import 'dart:convert';

import 'package:dna_app/PTW/ptw_summary.dart';
import 'package:dna_app/ap_assist/PTW/form.dart';
import 'package:flutter/material.dart';

import 'bahagian_C_add_worker.dart';

class BahagianC2 extends StatefulWidget {
  @override
  _BahagianC2State createState() => _BahagianC2State();
}

class _BahagianC2State extends State<BahagianC2> {
  TextStyle _label = TextStyle(
    fontFamily: 'opansans',
    color: Colors.blueGrey.shade700,
    fontSize: 16.0,
    fontWeight: FontWeight.w700,
    letterSpacing: 1,
  );
  TextStyle _info = TextStyle(
    fontFamily: 'opansans',
    fontSize: 15.0,
    fontWeight: FontWeight.normal,
    letterSpacing: 0.75,
  );
  double screenHeight;

  bool _checkbox_item_1 = false;
  bool _checkbox_item_2 = false;
  bool _checkbox_item_3 = false;
  bool _checkbox_item_4 = false;
  bool _checkbox_item_5 = false;

  /**
   *  update: October 28
   *  by:cikin.my
   */
  String dropdownValue_1 = 'Sila pilih Nama Pekerja';
  String dropdownValue_2 = 'Sila pilih Nama Pekerja';
  String dropdownValue_3 = 'Sila pilih Nama Pekerja';
  String dropdownValue_4 = 'Sila pilih Nama Pekerja';
  String dropdownValue_5 = 'Sila pilih Nama Pekerja';

  bool workerSelected = false;

//  Widget _buttonAdd()  {
//    return  Row(
//      mainAxisAlignment: MainAxisAlignment.end,
//      children: <Widget>[
//        RaisedButton.icon(
//          onPressed: () =>
//          {
//            setState(() {
//              Navigator.of(context).push(
//                MaterialPageRoute(builder: (BuildContext context) => BahagianC_AddWorker()),
//              );
//            }),
//          },
//          icon: Icon(
//            Icons.add,
//            size: 16,
//          ),
//          label: Row(
//            children: <Widget>[
//              Text('Add ', style: TextStyle(letterSpacing: 1)),
//            ],
//          ),
//          color: Colors.blue,
//          splashColor: Colors.blue[700],
//          textColor: Colors.white,
//          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
//        ),
//      ],
//    );
//  }
  Widget _buildListItem(_data) {
    return InkWell(
      onTap: () {
        Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext context) => BahagianC_AddWorker()));
      },
      highlightColor: Colors.white30,
      splashColor: Colors.red.shade50,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.fromLTRB(8, 8, 8, 2),
                    child: Row(
                      children: <Widget>[
                        Text(
                          _data['name'],
                          style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.w500),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                    padding: EdgeInsets.only(left: 4.0),
                    child: GestureDetector(
                      child: Icon(Icons.keyboard_arrow_right, color: Colors.green[400], size: 25.0),
                      onTap: () {
                        //open alert to select assist
                      },
                    )),
              ],
            )
          ],
        ),
      ),
    );
  }

  Widget _buttonConfirm() {
    return RaisedButton.icon(
      onPressed: () => {
        setState(() {
          Navigator.of(context).push(
            MaterialPageRoute(builder: (BuildContext context) => PtwSummary()),
          );
        }),
      },
      icon: Icon(
        Icons.check,
        size: 16,
      ),
      label: Row(
        children: <Widget>[
          Text('CONFIRM ', style: TextStyle(letterSpacing: 1)),
        ],
      ),
      color: Colors.blue,
      splashColor: Colors.blue[700],
      textColor: Colors.white,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
    );
  }

  Widget formSection() {
    return Container(
      height: screenHeight / 1.5,
      padding: EdgeInsets.all(10),
      child: Stack(
        children: <Widget>[
          FutureBuilder(
              future: DefaultAssetBundle.of(context).loadString('data/worker.json'),
              builder: (context, snapshot) {
                var _data = jsonDecode(snapshot.data.toString());
                if (snapshot.data == null) {
                  return Container(child: Center(child: Text("Loading...")));
                } else {
                  return ListView(
                    children: <Widget>[
                      ListView.builder(
                        physics: ScrollPhysics(),
                        shrinkWrap: true,
                        padding: EdgeInsets.all(8.0),
                        itemCount: _data == null ? 0 : _data.length,
                        itemBuilder: (BuildContext context, int index) {
                          return Container(
                            decoration: BoxDecoration(
                              border: Border(bottom: BorderSide(color: Colors.grey.shade200)),
                            ),
                            child: _buildListItem(_data[index]),
                          );
                        },
                      ),
                      Align(
                        alignment: Alignment.bottomCenter,
                        child: _buttonConfirm(),
                      ),
                    ],
                  );
                }
              }),
        ],
      ),
    );
  }

  Widget AddWorkerName() {
    return Column(
      children: <Widget>[
        SizedBox(height: 8),
        DropdownButtonHideUnderline(
          child: Container(
            padding: EdgeInsets.only(left: 10, right: 10),
            width: double.infinity,
            decoration: BoxDecoration(
              border: Border.all(color: Colors.blue[700], width: 1.5),
            ),
            child: DropdownButton<String>(
              isExpanded: true,
              value: dropdownValue_1,
              icon: Icon(Icons.keyboard_arrow_down, color: Colors.blue[700]),
              iconSize: 24,
              elevation: 16,
              onChanged: (String newValue) {
                setState(() {
                  dropdownValue_1 = newValue;
                  workerSelected = true;
                });
              },
              items: <String>['Sila pilih Nama Pekerja', 'Abdul Wahab (N/P: 10066564)', 'Abdul Manaf (N/P: 10066565)', 'Abdul Mansoor (N/P: 10066566)']
                  .map<DropdownMenuItem<String>>((String value) {
                return DropdownMenuItem<String>(
                  value: value,
                  child: Text(value),
                );
              }).toList(),
            ),
          ),
        ),
        SizedBox(height: 8),
        DropdownButtonHideUnderline(
          child: Container(
            padding: EdgeInsets.only(left: 10, right: 10),
            width: double.infinity,
            decoration: BoxDecoration(
              border: Border.all(color: Colors.blue[700], width: 1.5),
            ),
            child: DropdownButton<String>(
              isExpanded: true,
              value: dropdownValue_2,
              icon: Icon(Icons.keyboard_arrow_down, color: Colors.blue[700]),
              iconSize: 24,
              elevation: 16,
              onChanged: (String newValue) {
                setState(() {
                  dropdownValue_2 = newValue;
                  workerSelected = true;
                });
              },
              items: <String>['Sila pilih Nama Pekerja', 'Abdul Wahab (N/P: 10066564)', 'Abdul Manaf (N/P: 10066565)', 'Abdul Mansoor (N/P: 10066566)']
                  .map<DropdownMenuItem<String>>((String value) {
                return DropdownMenuItem<String>(
                  value: value,
                  child: Text(value),
                );
              }).toList(),
            ),
          ),
        ),
        SizedBox(height: 8),
        DropdownButtonHideUnderline(
          child: Container(
            padding: EdgeInsets.only(left: 10, right: 10),
            width: double.infinity,
            decoration: BoxDecoration(
              border: Border.all(color: Colors.blue[700], width: 1.5),
            ),
            child: DropdownButton<String>(
              isExpanded: true,
              value: dropdownValue_3,
              icon: Icon(Icons.keyboard_arrow_down, color: Colors.blue[700]),
              iconSize: 24,
              elevation: 16,
              onChanged: (String newValue) {
                setState(() {
                  dropdownValue_3 = newValue;
                  workerSelected = true;
                });
              },
              items: <String>['Sila pilih Nama Pekerja', 'Abdul Wahab (N/P: 10066564)', 'Abdul Manaf (N/P: 10066565)', 'Abdul Mansoor (N/P: 10066566)']
                  .map<DropdownMenuItem<String>>((String value) {
                return DropdownMenuItem<String>(
                  value: value,
                  child: Text(value),
                );
              }).toList(),
            ),
          ),
        ),
        SizedBox(height: 8),
        DropdownButtonHideUnderline(
          child: Container(
            padding: EdgeInsets.only(left: 10, right: 10),
            width: double.infinity,
            decoration: BoxDecoration(
              border: Border.all(color: Colors.blue[700], width: 1.5),
            ),
            child: DropdownButton<String>(
              isExpanded: true,
              value: dropdownValue_4,
              icon: Icon(Icons.keyboard_arrow_down, color: Colors.blue[700]),
              iconSize: 24,
              elevation: 16,
              onChanged: (String newValue) {
                setState(() {
                  dropdownValue_4 = newValue;
                  workerSelected = true;
                });
              },
              items: <String>['Sila pilih Nama Pekerja', 'Abdul Wahab (N/P: 10066564)', 'Abdul Manaf (N/P: 10066565)', 'Abdul Mansoor (N/P: 10066566)']
                  .map<DropdownMenuItem<String>>((String value) {
                return DropdownMenuItem<String>(
                  value: value,
                  child: Text(value),
                );
              }).toList(),
            ),
          ),
        ),
        SizedBox(height: 8),
        DropdownButtonHideUnderline(
          child: Container(
            padding: EdgeInsets.only(left: 10, right: 10),
            width: double.infinity,
            decoration: BoxDecoration(
              border: Border.all(color: Colors.blue[700], width: 1.5),
            ),
            child: DropdownButton<String>(
              isExpanded: true,
              value: dropdownValue_5,
              icon: Icon(Icons.keyboard_arrow_down, color: Colors.blue[700]),
              iconSize: 24,
              elevation: 16,
              onChanged: (String newValue) {
                setState(() {
                  dropdownValue_5 = newValue;
                  workerSelected = true;
                });
              },
              items: <String>['Sila pilih Nama Pekerja', 'Abdul Wahab (N/P: 10066564)', 'Abdul Manaf (N/P: 10066565)', 'Abdul Mansoor (N/P: 10066566)']
                  .map<DropdownMenuItem<String>>((String value) {
                return DropdownMenuItem<String>(
                  value: value,
                  child: Text(value),
                );
              }).toList(),
            ),
          ),
        ),
      ],
    );
  }

  Widget _perakuan(){
    return Column(
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text('PERAKUAN', style: TextStyle(fontSize: 18, fontWeight: FontWeight.w700)),
          ],
        ),
        Container(
          padding: EdgeInsets.all(8),
          decoration: BoxDecoration(border: Border.all(width: 1, color: Colors.blueGrey)),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[

              Text(
                'Saya yang bekerja di bawah penyeliaan Orang Berkecekapan mengaku terima dan paham perkara berikut:-',
              ),
              SizedBox(height: 18,),
              Container(
                margin: EdgeInsets.all(20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text('(i) Skop kerja yang dibenarkan, ', style: TextStyle(color: Colors.blueGrey[600],letterSpacing: 1 ),),
                    Text('(ii) Tempat Pembumian Utama dan Tambahan Litar dibuat,', style: TextStyle(color: Colors.blueGrey[600], letterSpacing: 1 ),),
                    Text('(iii) Memakai Kelengkapan Perlindungan Diri semasa bekerja', style: TextStyle(color: Colors.blueGrey[600], letterSpacing: 1),),
                  ],
                ),
              ),

            ],
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    screenHeight = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Bahagian C",
          style: TextStyle(
            letterSpacing: 1,
            fontFamily: 'Lato',
          ),
        ),
        centerTitle: true,
        leading: Builder(
          builder: (BuildContext context) {
            return IconButton(
              icon: const Icon(Icons.arrow_back),
              onPressed: () {
                Navigator.of(context).push(
                  MaterialPageRoute(builder: (BuildContext context) => PTW_Form()),
                );
              },
            );
          },
        ),
      ),
      body: ListView(
        children: <Widget>[
          Container(
            height: screenHeight / 5,
            margin: const EdgeInsets.all(8.0),
            padding: const EdgeInsets.all(10.0),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(15.0),
              color: Colors.grey[100],
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Text(
                      'No. Permit : ',
                      style: _label,
                    ),
                    Text('C00018000901', style: _info),
                  ],
                ),
                Text(
                  'Keterangan Kerja',
                  style: _label,
                ),
                Text(
                  'PREVENTION MAINTENANCE VCB DAN BUSBAR- TOTAL SHUTDOWN',
                  style: _info,
                ),
                Row(
                  children: <Widget>[
                    Text(
                      'Jenis Radas : ',
                      style: _label,
                    ),
                    Text('BUSBAR', style: _info),
                  ],
                ),
              ],
            ),
          ),
          SizedBox(
            height: 20,
          ),
          Text(
            'BAHAGIAN C',
            style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold, letterSpacing: 1),
            textAlign: TextAlign.center,
          ),

          Padding(
            padding: const EdgeInsets.fromLTRB(18, 25, 18, 0),
            child: Column(
              children: <Widget>[
                Text('(2) Orang Bekerja di bawah penyeliaan Orang Berkecekapan', style: _label),
//                _buttonAdd(),
                AddWorkerName(),
                SizedBox(height: 10,),

                Visibility(
                  visible: workerSelected,
                    child: Column(
                      children: <Widget>[
                        Divider(),
                        SizedBox(height: 10,),
                        _perakuan(),
                        _buttonConfirm(),
                      ],
                    )),
              ],
            ),
          ),
//          formSection(),
        ],
      ),
    );
  }
}
