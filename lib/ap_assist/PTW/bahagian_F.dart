import 'package:dna_app/ap_assist/PTW/CompleteWork.dart';
import 'package:flutter/material.dart';

class BahangianF extends StatefulWidget {
  @override
  _BahangianFState createState() => _BahangianFState();
}

class _BahangianFState extends State<BahangianF> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "PENGESAHAN",
          style: TextStyle(
            letterSpacing: 1,
            fontFamily: 'Lato',
          ),
        ),
        centerTitle: true,
        leading: Builder(
          builder: (BuildContext context) {
            return IconButton(
              icon: const Icon(Icons.arrow_back),
              onPressed: () {
                Navigator.of(context).push(
                  MaterialPageRoute(builder: (BuildContext context) => CompleteWork()),
                );
              },
            );
          },
        ),
      ),
      body: Container(
      ),
    );
  }
}
