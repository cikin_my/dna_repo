import 'package:dna_app/ap_assist/PTW/CompleteWork.dart';
import 'package:dna_app/ap_assist/PTW/OkstQp_Ptw_list.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/widgets.dart';
import 'package:fluttertoast/fluttertoast.dart';

class OkstQp_IssuePtw extends StatefulWidget {
  @override
  _OkstQp_IssuePtwState createState() => _OkstQp_IssuePtwState();
}

class _OkstQp_IssuePtwState extends State<OkstQp_IssuePtw> {
  double screenHeight;
  TextStyle _label = TextStyle(
    fontFamily: 'opansans',
    color: Colors.blueGrey.shade700,
    fontSize: 16.0,
    fontWeight: FontWeight.w700,
    letterSpacing: 1,
  );
  TextStyle _info = TextStyle(
    fontFamily: 'opansans',
    fontSize: 15.0,
    fontWeight: FontWeight.normal,
    letterSpacing: 0.75,
  );

  Widget _buildInfoContainer() {
    return Container(
      padding: const EdgeInsets.all(10.0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15.0),
        color: Colors.grey[100],
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Text(
                'No. Permit:  ',
                style: _label,
              ),
              Text(
                'JC00018000901',
                style: _info,
              ),
            ],
          ),
          SizedBox(
            height: 5,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Text(
                'No. JS:  ',
                style: _label,
              ),
              Text(
                'ABC-12-12345',
                style: _info,
              ),
            ],
          ),
          SizedBox(
            height: 5,
          ),
          Text(
            'Keterangan Kerja:  ',
            style: _label,
          ),
          Text(
            'PREVENTION MAINTENANCE VCB DAN BUSBAR-TOTAL SHUTOWN',
            style: _info,
          ),
          SizedBox(
            height: 5,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Text(
                'Jenis Radas:  ',
                style: _label,
              ),
              Text(
                'BUSBAR',
                style: _info,
              ),
            ],
          ),
          SizedBox(
            height: 5,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Text(
                'Status:  ',
                style: _label,
              ),
              Text(
                'Work In Progress',
                style: _info,
              ),
            ],
          ),
          SizedBox(
            height: 5,
          ),
          SizedBox(
            height: 15,
          ),
          Divider(),
        ],
      ),
    );
  }

  Widget _buttonExit() {
    return Container(
      padding: EdgeInsets.all(20),
      child: RaisedButton.icon(
        onPressed: () => {
          setState(() {}),
        },
        icon: Icon(
          Icons.assignment,
          size: 16,
        ),
        label: Row(
          children: <Widget>[
            Text('PENGUJIAN', style: TextStyle(letterSpacing: 1)),
          ],
        ),
        color: Colors.blue,
        splashColor: Colors.blue[700],
        textColor: Colors.white,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
      ),
    );
  }

  Widget _buttonEnter() {
    return Container(
      padding: EdgeInsets.all(20),
      child: RaisedButton.icon(
        onPressed: () => {
          setState(() {}),
        },
        icon: Icon(
          Icons.assignment_turned_in,
          size: 16,
        ),
        label: Row(
          children: <Widget>[
            Text('SELESAI PENGUJIAN', style: TextStyle(letterSpacing: 1)),
          ],
        ),
        color: Colors.blue,
        splashColor: Colors.blue[700],
        textColor: Colors.white,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
      ),
    );
  }

  Widget _buttonSubmit() {
    return RaisedButton.icon(
      onPressed: () => {
        setState(() {
          Navigator.of(context).push(
            MaterialPageRoute(builder: (BuildContext context) => CompleteWork()),
          );
        }),
      },
      icon: Icon(
        Icons.assignment_turned_in,
        size: 16,
      ),
      label: Row(
        children: <Widget>[
          Text('KERJA SELESAI ', style: TextStyle(letterSpacing: 1)),
        ],
      ),
      color: Colors.green,
      splashColor: Colors.green[700],
      textColor: Colors.white,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
    );
  }

  @override
  Widget build(BuildContext context) {
    screenHeight = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Permit Manjalankan Kerja",
          style: TextStyle(
            letterSpacing: 1,
            fontFamily: 'Lato',
          ),
        ),
        centerTitle: true,
        leading: Builder(
          builder: (BuildContext context) {
            return IconButton(
              icon: const Icon(Icons.arrow_back),
              onPressed: () {
                Navigator.of(context).push(
                  MaterialPageRoute(builder: (BuildContext context) => OkstOp_PTW_List()),
                );
              },
            );
          },
        ),
      ),
      body: Container(
        padding: EdgeInsets.all(18),
        height: double.maxFinite,
        child: Stack(
          children: <Widget>[
            ListView(
              children: <Widget>[
                _buildInfoContainer(),
                Text(
                  'Sila pilih aktiviti seterusnya:  ',
                  style: _label,
                ),
                Divider(),
                Text('Notis Keluar Tapak'),
                _buttonExit(),
                Text('Notis Masuk Semula Tapak'),
                _buttonEnter(),
              ],
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: _buttonSubmit(),
            ),
          ],
        ),
      ),
    );
  }
}
