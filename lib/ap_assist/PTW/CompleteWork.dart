import 'dart:convert';

import 'package:dna_app/ap_assist/PTW/OkstQp_issue_ptw.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class CompleteWork extends StatefulWidget {
  @override
  _CompleteWorkState createState() => _CompleteWorkState();
}

class _CompleteWorkState extends State<CompleteWork> {
//for dummy demo only. you can load bool value from api here
  List<bool> isHighlighted = [true, false, false, true, false];


  double screenHeight;

  TextStyle _label = TextStyle(
    fontFamily: 'opansans',
    color: Colors.blueGrey.shade700,
    fontSize: 16.0,
    fontWeight: FontWeight.w700,
    letterSpacing: 1,
  );
  TextStyle _info = TextStyle(
    fontFamily: 'opansans',
    fontSize: 15.0,
    fontWeight: FontWeight.normal,
    letterSpacing: 0.75,
  );

  Widget _infoContainer() {
    return Container(
//      height: screenHeight / 3.5,
      margin: const EdgeInsets.all(8.0),
      padding: const EdgeInsets.all(10.0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15.0),
        color: Colors.grey[100],
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Text(
                'No. Permit:  ',
                style: _label,
              ),
              Text(
                'JC00018000901',
                style: _info,
              ),
            ],
          ),
          SizedBox(
            height: 5,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Text(
                'No. JS:  ',
                style: _label,
              ),
              Text(
                'ABC-12-12345',
                style: _info,
              ),
            ],
          ),
          SizedBox(
            height: 5,
          ),
          Text(
            'Keterangan Kerja:  ',
            style: _label,
          ),
          Text(
            'PREVENTION MAINTENANCE VCB DAN BUSBAR-TOTAL SHUTOWN',
            style: _info,
          ),
          SizedBox(
            height: 5,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Text(
                'Jenis Radas:  ',
                style: _label,
              ),
              Text(
                'BUSBAR',
                style: _info,
              ),
            ],
          ),
          SizedBox(
            height: 5,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Text(
                'Status:  ',
                style: _label,
              ),
              Text(
                'Work In Progress',
                style: _info,
              ),
            ],
          ),
          SizedBox(
            height: 5,
          ),
          SizedBox(
            height: 15,
          ),
          Divider(),
        ],
      ),
    );
  }

  Widget _buttonSubmit() {
    return RaisedButton.icon(
      onPressed: isHighlighted.contains(false) ? null : () => {showAlertDialog_submit(context)},
      icon: Icon(
        Icons.send,
        size: 16,
      ),
      label: Row(
        children: <Widget>[
          Text('Submit ', style: TextStyle(letterSpacing: 1)),
        ],
      ),
      color: Colors.blue,
      splashColor: Colors.blue[700],
      textColor: Colors.white,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
    );
  }

  showAlertDialog(BuildContext context, index) {
    // set up the buttons
    Widget cancelButton = FlatButton(
      child: Text(
        "Cancel",
        style: TextStyle(letterSpacing: 1),
      ),
      onPressed: () {
        Navigator.pop(context);
      },
      color: Colors.grey,
      textColor: Colors.white,
      splashColor: Colors.grey[700],
    );
    Widget continueButton = FlatButton(
      child: Text(
        "Confirm",
        style: TextStyle(letterSpacing: 1),
      ),
      onPressed: () {
        for (int i = 0; i < isHighlighted.length; i++) {
          setState(() {
            if (index == i) {
              isHighlighted[index] = true;
            }
          });
        }
        setState(() {
          isHighlighted[index] = true;
        });
        Navigator.pop(context);
      },
      color: Colors.green,
      textColor: Colors.white,
      splashColor: Colors.red[700],
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      titlePadding: EdgeInsets.all(0),
      contentPadding: EdgeInsets.all(0),
      contentTextStyle: TextStyle(color: Colors.black, fontSize: 12, fontFamily: 'opensans'),
      title: Container(
        padding: EdgeInsets.all(10),
        decoration: BoxDecoration(color: Colors.green),
        child: Text(
          "PENGESAHAN",
          style: TextStyle(color: Colors.white, letterSpacing: 1, fontSize: 16),
          textAlign: TextAlign.center,
        ),
      ),
      content: Container(
        height: screenHeight / 2,
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              SizedBox(
                height: 18,
              ),
              Icon(
                FontAwesomeIcons.exclamationCircle,
                color: Colors.grey[700],
                size: 25,
              ),
              SizedBox(
                height: 18,
              ),
              Text(
                'BAHAGIAN F',
                style: TextStyle(fontSize: 16, color: Colors.black, letterSpacing: 1, fontWeight: FontWeight.w600),
              ),
              SizedBox(
                height: 18,
              ),
              Text(
                'Dengan ini saya mengaku bahawa kerja-kerja telah selesai dijalankan mengikut skop dalam Permit Menjalankan Kerja',
                style: TextStyle(
                  fontSize: 14,
                ),
                textAlign: TextAlign.center,
              ),
              SizedBox(height: 20),
              Container(
                margin: EdgeInsets.all(8),
                padding: EdgeInsets.all(8),
                child: Column(
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Expanded(
                            child: Text(
                          'Nama :',
                          style: _label,
                          textAlign: TextAlign.right,
                        )),
                        Expanded(
                            child: Text(
                          'James Dean',
                          style: _info,
                          textAlign: TextAlign.left,
                        )),
                      ],
                    ),
                    SizedBox(height: 4),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Expanded(
                            child: Text(
                          'No Pekerja :',
                          style: _label,
                          textAlign: TextAlign.right,
                        )),
                        Expanded(
                            child: Text(
                          '1000666788',
                          style: _info,
                          textAlign: TextAlign.left,
                        )),
                      ],
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
      actions: [
        cancelButton,
        continueButton,
      ],
    );
    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  showAlertDialog_submit(BuildContext context) {
    // set up the buttons
    Widget cancelButton = FlatButton(
      child: Text(
        "Cancel",
        style: TextStyle(letterSpacing: 1),
      ),
      onPressed: () {
        Navigator.pop(context);
      },
      color: Colors.grey,
      textColor: Colors.white,
      splashColor: Colors.grey[700],
    );
    Widget continueButton = FlatButton(
      child: Text(
        "Confirm",
        style: TextStyle(letterSpacing: 1),
      ),
      onPressed: () {
        Navigator.pop(context);
      },
      color: Colors.red,
      textColor: Colors.white,
      splashColor: Colors.red[700],
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      titlePadding: EdgeInsets.all(0),
      contentPadding: EdgeInsets.all(0),
      contentTextStyle: TextStyle(color: Colors.black, fontSize: 12, fontFamily: 'opensans'),
      title: Container(
        padding: EdgeInsets.all(10),
        decoration: BoxDecoration(color: Colors.red),
        child: Text(
          "PENGESAHAN",
          style: TextStyle(color: Colors.white, letterSpacing: 1, fontSize: 16),
          textAlign: TextAlign.center,
        ),
      ),
      content: Container(
        height: screenHeight / 2,
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              SizedBox(
                height: 18,
              ),
              Icon(
                FontAwesomeIcons.exclamationCircle,
                color: Colors.red[700],
                size: 25,
              ),
              SizedBox(
                height: 18,
              ),
              Text(
                'BAHAGIAN F',
                style: TextStyle(fontSize: 16, color: Colors.black, letterSpacing: 1, fontWeight: FontWeight.w600),
              ),
              SizedBox(
                height: 18,
              ),
              Text(
                'Dengan ini saya mengaku bahawa kerja-kerja telah selesai '
                'dijalankan mengikut skop dan memohon Permit Menjalankan '
                'Kerja (PTW) ini DIBATALKAN',
                style: TextStyle(
                  fontSize: 14,
                ),
                textAlign: TextAlign.center,
              ),
              SizedBox(height: 20),
              Container(
                margin: EdgeInsets.all(8),
                padding: EdgeInsets.all(8),
                child: Column(
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Expanded(
                            child: Text(
                          'Nama CP:',
                          style: _label,
                          textAlign: TextAlign.right,
                        )),
                        Expanded(
                            child: Text(
                          'James Dean',
                          style: _info,
                          textAlign: TextAlign.left,
                        )),
                      ],
                    ),
                    SizedBox(height: 4),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Expanded(
                            child: Text(
                          'No Pekerja :',
                          style: _label,
                          textAlign: TextAlign.right,
                        )),
                        Expanded(
                            child: Text(
                          '1000666788',
                          style: _info,
                          textAlign: TextAlign.left,
                        )),
                      ],
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
      actions: [
        cancelButton,
        continueButton,
      ],
    );
    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    screenHeight = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.green[700],
        title: Text(
          "Perakuan Kerja Siap",
          style: TextStyle(
            letterSpacing: 1,
            fontFamily: 'Lato',
          ),
        ),
        centerTitle: true,
        leading: Builder(
          builder: (BuildContext context) {
            return IconButton(
              icon: const Icon(Icons.arrow_back),
              onPressed: () {
                Navigator.of(context).push(
                  MaterialPageRoute(builder: (BuildContext context) => OkstQp_IssuePtw()),
                );
              },
            );
          },
        ),
      ),
      body: Container(
        height: double.maxFinite,
        child: Stack(
          children: <Widget>[
            FutureBuilder(
                future: DefaultAssetBundle.of(context).loadString('data/worker.json'),
                builder: (context, snapshot) {
                  var _data = jsonDecode(snapshot.data.toString());
                  if (snapshot.data == null) {
                    return Container(child: Center(child: Text("Loading...")));
                  } else {
                    return ListView(
                      children: <Widget>[
                        _infoContainer(),
                        Padding(
                          padding: const EdgeInsets.fromLTRB(18, 25, 18, 0),
                          child: Column(
                            children: <Widget>[
                              Text(
                                'Senarai Orang Bekerja dibawah penyeliaan Orang Berkecekapan',
                                style: TextStyle(letterSpacing: 2, fontSize: 17, color: Colors.blueGrey),
                              ),
                              Divider(),
                            ],
                          ),
                        ),
                        //====== listview for worker's name  start here ====
                        ListView.builder(
                          shrinkWrap: true,
                          itemCount: _data == null ? 0 : _data.length, //_listViewData.length,
                          itemBuilder: (context, index) => Container(
                            child: ListTile(
                              title: Row(
                                children: <Widget>[
                                  Icon(
                                    Icons.check_circle,
                                    color: isHighlighted[index] ? Colors.green : Colors.grey[300],
                                  ),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Text(
                                    _data[index]['name'],
                                    style: TextStyle(fontWeight: FontWeight.w600),
                                  ),
                                ],
                              ),
                              trailing: Icon(
                                Icons.keyboard_arrow_right,
                                color: Colors.green,
                              ),

                              onTap: () => showAlertDialog(context, index), // _onSelected(index),
                            ),
                          ),
                        ),
                      ],
                    );
                  }
                }),
            Align(alignment: Alignment.bottomCenter, child: _buttonSubmit()),
          ],
        ),
      ),
    );
  }
}
