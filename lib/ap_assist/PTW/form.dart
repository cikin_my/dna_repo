import 'package:dna_app/PTW/bahagian_C.dart';
import 'package:dna_app/PTW/ptw_list.dart';
import 'package:dna_app/ap_assist/PTW/OkstQp_Ptw_list.dart';
import 'package:dna_app/ap_assist/PTW/bahagian_C_1.dart';
import 'package:dna_app/ap_assist/PTW/bahagian_C_add_worker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/widgets.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:grouped_buttons/grouped_buttons.dart';

class PTW_Form extends StatefulWidget {
  @override
  _PTW_FormState createState() => _PTW_FormState();
}

class _PTW_FormState extends State<PTW_Form> {
  int _radioValue1 = 0;

  void _handleRadioValueChange1(int value) {
    setState(() {
      _radioValue1 = value;

      switch (_radioValue1) {
        case 0:
          Fluttertoast.showToast(
              msg: "LOTO Box",
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.CENTER,
              timeInSecForIosWeb: 1,
              backgroundColor: Colors.orange,
              textColor: Colors.white,
              fontSize: 14.0);
          break;
        case 1:
          Fluttertoast.showToast(
              msg: "HASP Lock",
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.CENTER,
              timeInSecForIosWeb: 1,
              backgroundColor: Colors.orange,
              textColor: Colors.white,
              fontSize: 14.0);
          break;
      }
    });
  }

  bool _langkah_prosidure_1 = true;
  bool _langkah_prosidure_2 = true;
  bool _langkah_prosidure_3 = true;
  bool _langkah_prosidure_4 = true;
  String dropdownValue = 'LABEL NOMBOR SUIS & NAMA F';

  Item selectedUser;
  List<Item> users = <Item>[
    const Item(
        'Ahmad',
        Icon(
          Icons.person,
          color: const Color(0xFF167F67),
        )),
    const Item(
        'Hassan',
        Icon(
          Icons.person,
          color: const Color(0xFF167F67),
        )),
    const Item(
        'Merry',
        Icon(
          Icons.person,
          color: const Color(0xFF167F67),
        )),
    const Item(
        'Ahmad Ali',
        Icon(
          Icons.person,
          color: const Color(0xFF167F67),
        )),
  ];

  double screenHeight;
  TextStyle _label = TextStyle(
    fontFamily: 'opansans',
    color: Colors.blueGrey.shade700,
    fontSize: 16.0,
    fontWeight: FontWeight.w700,
    letterSpacing: 1,
  );
  TextStyle _info = TextStyle(
    fontFamily: 'opansans',
    fontSize: 15.0,
    fontWeight: FontWeight.normal,
    letterSpacing: 0.75,
  );

  Widget _buildInfoContainer() {
    return Container(
      height: screenHeight / 5,
      margin: const EdgeInsets.all(8.0),
      padding: const EdgeInsets.all(10.0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15.0),
        color: Colors.grey[100],
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            children: <Widget>[
              Text(
                ('Permit Manjalankan Kerja'),
                style: TextStyle(fontWeight: FontWeight.bold, letterSpacing: 1, color: Colors.black, fontSize: 16, fontFamily: 'BellotaText'),
                textAlign: TextAlign.center,
              ),
            ],
            mainAxisAlignment: MainAxisAlignment.center,
          ),
          Divider(),
          SizedBox(
            height: 15,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Text(
                'No. JS:  ',
                style: _label,
              ),
              Text(
                'ABC-12-12345',
                style: _info,
              ),
            ],
          ),
          SizedBox(
            height: 5,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Text(
                'No. Permit:  ',
                style: _label,
              ),
              Text(
                'JC00018000901',
                style: _info,
              ),
            ],
          ),
          SizedBox(
            height: 5,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Text(
                'Stesen:  ',
                style: _label,
              ),
              Text(
                'Kuantan',
                style: _info,
              ),
            ],
          ),
          SizedBox(
            height: 5,
          ),
        ],
      ),
    );
  }

  Widget _formStart() {
    return Padding(
      padding: const EdgeInsets.all(18.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            'No. Authorisation/Sanction :',
            style: _label,
          ),
          SizedBox(
            height: 8,
          ),
          Row(
            children: <Widget>[
              Text(
                '(RCC) :',
                style: _label,
              ),
              Text(
                'XYZABC1230000',
                style: _info,
              ),
            ],
          ),
          SizedBox(height: 8),
          Row(
            children: <Widget>[
              Text(
                '(AP) :',
                style: _label,
              ),
              Text(
                'XYZABC1230000',
                style: _info,
              ),
            ],
          ),
          SizedBox(height: 18),
          Row(
            children: <Widget>[
              Text(
                'Nama OKST/OP :',
                style: _label,
              ),
              Text(
                'Eddard Stark',
                style: _info,
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget _bahagian_A() {
    return Container(
      margin: EdgeInsets.all(8),
      padding: EdgeInsets.all(8),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SizedBox(
            height: 18,
          ),
          Text('Punca Bekalan dimatikan dan Tempat Pembumian Utama Litar di mana radas dibumi serta dilitar pintaskan'),
          SizedBox(
            height: 18,
          ),
          Text(
            'Nama dan No Suis (PMU/ PPU/ SSU/ PE/ PAT/ BLACKBOX/ FEEDER PILLAR/ LVDB)',
            style: _label,
          ),
          SizedBox(height: 10),
          Container(
            padding: EdgeInsets.all(18),
            width: double.infinity,
            color: Colors.grey[200],
            child: Text(
              'RSTL-NE-012345678',
              style: _info,
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Container(
            padding: EdgeInsets.all(18),
            width: double.infinity,
            color: Colors.grey[200],
            child: Text(
              'CBT-0076533',
              style: _info,
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Container(
            padding: EdgeInsets.all(18),
            width: double.infinity,
            color: Colors.grey[200],
            child: Text(
              'Maintenance',
              style: _info,
            ),
          ),
          SizedBox(
            height: 18,
          ),
          Row(
            children: <Widget>[
              Text(
                'Jenis Kunci',
                style: _label,
              ),
              SizedBox(
                width: 8,
              ),
              Radio(
                value: 0,
                groupValue: _radioValue1,
                onChanged: null,
              ),
              Text(
                'LOTO box',
                style: new TextStyle(fontSize: 14.0),
              ),
              Radio(
                value: 1,
                groupValue: _radioValue1,
                onChanged: null,
              ),
              Text(
                'HASP Lock',
                style: new TextStyle(
                  fontSize: 14.0,
                ),
              ),
            ],
          ),
          SizedBox(
            height: 18,
          ),
          Container(
              padding: EdgeInsets.all(8),
              decoration: BoxDecoration(border: Border.all(width: 1, color: Colors.blueGrey)),
              child: Text(
                'Saya mengaku bahawa langkah-langkah prosidur keselamatan telah dipatuhi'
                'untuk memastikan kerja yang dijalankan adalah selamat.',
                textAlign: TextAlign.center,
              )),
          SizedBox(
            height: 18,
          ),
          Container(
            child: Column(
              children: <Widget>[
                CheckboxListTile(
                    title: Text('TAKLIMAT  KESELAMATAN & SKOP KERJA '),
                    subtitle: Text('sebelum kerja dimulakan kepada semua Orang Bekerja'),
                    controlAffinity: ListTileControlAffinity.platform,
                    value: true,
                    onChanged: null),
                CheckboxListTile(
                    title: Text('KELENGKAPAN PERLINDUNGAN DIRI'),
                    subtitle: Text('Orang Bekerja Lengkap'),
                    controlAffinity: ListTileControlAffinity.platform,
                    value: true,
                    onChanged: null),
                CheckboxListTile(
                    title: Text('KENALPASTI HAZARD (HI).'), subtitle: Text('Sila Pilih'), controlAffinity: ListTileControlAffinity.platform, value: true, onChanged: null),
                Container(
                  padding: EdgeInsets.all(18),
                  width: double.infinity,
                  color: Colors.grey[200],
                  child: Text(
                    'LABEL NOMBOR SUIS & NAMA F'
                    '',
                    style: _info,
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                //langkah prosidure 4
                CheckboxListTile(
                    title: Text('LANGKAH KAWALAN BAGI HAZARD DINYATAKAN. '),
                    subtitle: Text('(KAWALAN RISIKO - DC)'),
                    controlAffinity: ListTileControlAffinity.platform,
                    value: true,
                    onChanged: null),
                Container(
                  padding: EdgeInsets.all(18),
                  width: double.infinity,
                  color: Colors.grey[200],
                  child: Text(
                    '',
                    style: _info,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _buttonNext() {
    return Container(
      padding: EdgeInsets.all(20),
      child: RaisedButton.icon(
        onPressed: () => {
          Navigator.of(context).push(
            MaterialPageRoute(builder: (BuildContext context) => Bahagian_C1()),
          )
        },
        icon: Icon(
          Icons.arrow_forward,
          size: 16,
        ),
        label: Row(
          children: <Widget>[
            Text('Next', style: TextStyle(letterSpacing: 1)),
          ],
        ),
        color: Colors.blue,
        splashColor: Colors.blue[700],
        textColor: Colors.white,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    screenHeight = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "DNSC",
          style: TextStyle(
            letterSpacing: 1,
            fontFamily: 'Lato',
          ),
        ),
        centerTitle: true,
        leading: Builder(
          builder: (BuildContext context) {
            return IconButton(
              icon: const Icon(Icons.arrow_back),
              onPressed: () {
                Navigator.of(context).push(
                  MaterialPageRoute(builder: (BuildContext context) => OkstOp_PTW_List()),
                );
              },
            );
          },
        ),
      ),
      body: ListView(
        children: <Widget>[
          _buildInfoContainer(),
          _formStart(),
          Divider(),
          Text(
            'BAHAGIAN A',
            style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold, letterSpacing: 1),
            textAlign: TextAlign.center,
          ),
          _bahagian_A(),
          SizedBox(height: 18),
          _buttonNext(),

        ],
      ),
    );
  }
}

class Item {
  const Item(this.name, this.icon);

  final String name;
  final Icon icon;
}
