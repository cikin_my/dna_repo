import 'package:dna_app/PTW/bahagian_C.dart';
import 'package:dna_app/PTW/pte_form.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class BahagianD extends StatefulWidget {
  @override
  _BahagianDState createState() => _BahagianDState();
}

class _BahagianDState extends State<BahagianD> {
  TextStyle _label = TextStyle(
    fontFamily: 'opansans',
    color: Colors.blueGrey.shade700,
    fontSize: 16.0,
    fontWeight: FontWeight.w700,
    letterSpacing: 1,
  );
  TextStyle _info = TextStyle(
    fontFamily: 'opansans',
    fontSize: 15.0,
    fontWeight: FontWeight.normal,
    letterSpacing: 0.75,
  );

  bool _checkbox_1 = false;
  bool _checkbox_2 = false;
  bool _checkbox_3 = false;
  bool _checkbox_4 = false;

  bool _checkbox_5 = false;
  bool _checkbox_6 = false;
  bool _checkbox_7 = false;
  bool _checkbox_8 = false;
  bool _checkbox_9 = false;

  Widget _bahagian_D1() {
    return Column(
      children: <Widget>[
        Container(
          color: Colors.deepPurple[50],
          child: ListTile(
            title: Text(
              'BAHAGIAN D ',
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold, letterSpacing: 1),
              textAlign: TextAlign.center,
            ),
            trailing: Icon(
              Icons.edit,
              color: Colors.deepPurple,
            ),
          ),
        ),
        Container(
          child: Column(
            children: <Widget>[
              Container(
                padding: EdgeInsets.all(10),
                child: Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15.0),
                  ),
                  color: Colors.grey[100],
                  child: Padding(
                    padding: const EdgeInsets.all(18.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          'No. Permit ',
                          style: _label,
                        ),
                        Text(
                          'JC00018000901',
                          style: _info,
                        ),
                        SizedBox(height: 8),
                        Text(
                          'Keterangan Kerja ',
                          style: _label,
                        ),
                        Row(
                          children: <Widget>[
                            Expanded(
                              child: Text(
                                'PREVENTIVE MAINTENANCE VCB DAN BUSBAR - TOTAL SHUTDOWN',
                                style: _info,
                              ),
                            ),
                          ],
                        ),
                        SizedBox(height: 8),
                      ],
                    ),
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.all(18),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      'BAHAGIAN D - D1',
                      style: _label,
                    ),
                    SizedBox(height: 10),
                    Text(
                      'NOTIS KELUAR TAPAK  - TUJUAN PENGUJIAN',
                      style: _info,
                    ),
                    SizedBox(height: 8),
                    Text(
                      'Dengan ini saya mengaku bahawa kerja-kerja diberhentikan sementara dan:',
                      style: TextStyle(fontSize: 16),
                      textAlign: TextAlign.center,
                    ),
                  ],
                ),
              ),
              Divider(),
              CheckboxListTile(
                title: Text('Semua orang bawah jagaan telah berundur dari skop kerjanya'),
                controlAffinity: ListTileControlAffinity.platform,
                value: _checkbox_1,
                onChanged: (bool value) {
                  setState(() {
                    _checkbox_1 = value;
                  });
                },
              ),
              Divider(),
              CheckboxListTile(
                title: Text('Tiada apa-apa alatan tertinggal'),
                controlAffinity: ListTileControlAffinity.platform,
                value: _checkbox_2,
                onChanged: (bool value) {
                  setState(() {
                    _checkbox_2 = value;
                  });
                },
              ),
              Divider(),
              CheckboxListTile(
                title: Text('Pembumian Tambahan telah ditanggalkan '),
                controlAffinity: ListTileControlAffinity.platform,
                value: _checkbox_3,
                onChanged: (bool value) {
                  setState(() {
                    _checkbox_3 = value;
                  });
                },
              ),
              Divider(),
              CheckboxListTile(
                title: Text('Kunci NSL Tambahan ditanggalkan'),
                controlAffinity: ListTileControlAffinity.platform,
                value: _checkbox_4,
                onChanged: (bool value) {
                  setState(() {
                    _checkbox_4 = value;
                  });
                },
              ),
              Divider(),
              SizedBox(height: 20,),
            ],
          ),
        ),
      ],
    );
  }
  Widget _bahagian_D2() {
    return Column(
      children: <Widget>[
        Container(
          child: Column(
            children: <Widget>[
              Container(
                padding: EdgeInsets.all(18),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      'BAHAGIAN D - D2',
                      style: _label,
                    ),
                    SizedBox(height: 10),
                    Text(
                      'NOTIS KELUAR TAPAK  - TUJUAN PENGUJIAN',
                      style: _info,
                    ),
                    SizedBox(height: 8),
                    Text(
                      'Dengan ini saya mengaku bahawa kerja-kerja diberhentikan sementara dan:',
                      style: TextStyle(fontSize: 16),
                      textAlign: TextAlign.center,
                    ),
                  ],
                ),
              ),
              Divider(),
              CheckboxListTile(
                title: Text('Semua orang bawah jagaan telah berundur dari skop kerjanya'),
                controlAffinity: ListTileControlAffinity.platform,
                value: _checkbox_5,
                onChanged: (bool value) {
                  setState(() {
                    _checkbox_5 = value;
                  });
                },
              ),
              Divider(),
              CheckboxListTile(
                title: Text('Tiada apa-apa alatan tertinggal'),
                controlAffinity: ListTileControlAffinity.platform,
                value: _checkbox_6,
                onChanged: (bool value) {
                  setState(() {
                    _checkbox_6 = value;
                  });
                },
              ),
              Divider(),
              CheckboxListTile(
                title: Text('Pembumian Tambahan telah ditanggalkan '),
                controlAffinity: ListTileControlAffinity.platform,
                value: _checkbox_7,
                onChanged: (bool value) {
                  setState(() {
                    _checkbox_7 = value;
                  });
                },
              ),
              Divider(),
              CheckboxListTile(
                title: Text('Kunci NSL Tambahan ditanggalkan'),
                controlAffinity: ListTileControlAffinity.platform,
                value: _checkbox_8,
                onChanged: (bool value) {
                  setState(() {
                    _checkbox_8 = value;
                  });
                },
              ),
              Divider(),
              CheckboxListTile(
                title: Text('Pembumian Utama sahaja dibuka (Earth Off)'),
                controlAffinity: ListTileControlAffinity.platform,
                value: _checkbox_9,
                onChanged: (bool value) {
                  setState(() {
                    _checkbox_9 = value;
                  });
                },
              ),
              Divider(),
              SizedBox(height: 20,),
            ],
          ),
        ),
      ],
    );
  }

  Widget _buildButton_d1() {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        RaisedButton.icon(
          onPressed: () => {
            Navigator.of(context).push(
              MaterialPageRoute(builder: (BuildContext context) => PTW_form()),
            )
          },
          icon: Icon(
            Icons.check,
            size: 16,
          ),
          label: Row(
            children: <Widget>[
              Text('Confirm', style: TextStyle(letterSpacing: 1)),
            ],
          ),
          color: Colors.deepPurple,
          splashColor: Colors.deepPurple[700],
          textColor: Colors.white,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
        ),

      ],
    );
  }
  Widget _buildButton_d2() {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        RaisedButton.icon(
          onPressed: () => {
            Navigator.of(context).push(
              MaterialPageRoute(builder: (BuildContext context) => PTW_form()),
            )
          },
          icon: Icon(
            Icons.check,
            size: 16,
          ),
          label: Row(
            children: <Widget>[
              Text('Confirm', style: TextStyle(letterSpacing: 1)),
            ],
          ),
          color: Colors.deepPurple,
          splashColor: Colors.deepPurple[700],
          textColor: Colors.white,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
        ),

      ],
    );
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Permit Menjalankan Kerja",
          style: TextStyle(
            letterSpacing: 1,
            fontFamily: 'Lato',
          ),
        ),
        centerTitle: true,
        leading: Builder(
          builder: (BuildContext context) {
            return IconButton(
              icon: const Icon(Icons.arrow_back),
              onPressed: () {
                Navigator.of(context).push(
                  MaterialPageRoute(builder: (BuildContext context) => PTW_form()),
                );
              },
            );
          },
        ),
      ),
      body: ListView(
        children: <Widget>[
          _bahagian_D1(),
          _buildButton_d1(),
          SizedBox(height:28),
          _bahagian_D2(),
          _buildButton_d2(),
        ],
      ),
    );
  }
}
