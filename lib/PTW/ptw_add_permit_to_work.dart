import 'dart:convert';

import 'package:dna_app/PTW/pte_form.dart';
import 'package:dna_app/PTW/ptw_list.dart';
import 'package:dna_app/ap_assist/PTW/OkstQp_Ptw_list.dart';
import 'package:dna_app/ap_assist/PTW/form.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';

class AddPermitToWork extends StatefulWidget {
  @override
  _AddPermitToWorkState createState() => _AddPermitToWorkState();
}

class _AddPermitToWorkState extends State<AddPermitToWork> {
  TextStyle _label = TextStyle(
    fontFamily: 'opansans',
    color: Colors.blueGrey.shade700,
    fontSize: 16.0,
    fontWeight: FontWeight.w700,
    letterSpacing: 1,
  );
  TextStyle _info = TextStyle(
    fontFamily: 'opansans',
    fontSize: 15.0,
    fontWeight: FontWeight.normal,
    letterSpacing: 0.75,
  );
  double screenHeight;

  Widget _infoOperation() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(8, 8, 8, 0),
      child: Card(
        elevation: 1,
        shadowColor: Colors.blueGrey[100],
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  children: <Widget>[
                    Text(
                      'No Js :',
                      style: TextStyle(color: Colors.blueGrey, fontWeight: FontWeight.w500, fontSize: 14),
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Text(
                      'ABC-12-12345',
                      style: TextStyle(color: Colors.black87, fontWeight: FontWeight.w300, fontSize: 14),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  children: <Widget>[
                    Text(
                      'Substation :',
                      style: TextStyle(color: Colors.blueGrey, fontWeight: FontWeight.w500, fontSize: 14),
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Text(
                      '1-Mar-2020 18:00PM',
                      style: TextStyle(color: Colors.black87, fontWeight: FontWeight.w300, fontSize: 14),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  children: <Widget>[
                    Text(
                      'Station :',
                      style: TextStyle(color: Colors.blueGrey, fontWeight: FontWeight.w500, fontSize: 14),
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Text(
                      'Kuantan',
                      style: TextStyle(color: Colors.black87, fontWeight: FontWeight.w300, fontSize: 14),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buttonAdd() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: <Widget>[
        RaisedButton.icon(
          onPressed: () => {
            setState(() {
              Navigator.of(context).push(
                MaterialPageRoute(builder: (BuildContext context) => PTW_form()),
              );
            }),
          },
          icon: Icon(
            Icons.add,
            size: 16,
          ),
          label: Row(
            children: <Widget>[
              Text('Add ', style: TextStyle(letterSpacing: 1)),
            ],
          ),
          color: Colors.blue,
          splashColor: Colors.blue[700],
          textColor: Colors.white,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
        ),
      ],
    );
  }

  Widget _buildListItem(_data) {
    return InkWell(
      onTap: () {
        Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext context) => PTW_Form()));
      },
      highlightColor: Colors.white30,
      splashColor: Colors.red.shade50,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.fromLTRB(8, 8, 8, 2),
                    child: Row(
                      children: <Widget>[
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              'No Permit : ' + _data['no_permit'],
                              style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.w500),
                            ),
                            Text(
                              _data['Ap_assist'],
                              style: TextStyle(
                                fontSize: 14,
                                color: Colors.grey[600],
                              ),
                            )
                          ],
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                    padding: EdgeInsets.only(left: 4.0),
                    child: GestureDetector(
                      child: Icon(Icons.keyboard_arrow_right, color: Colors.green[400], size: 25.0),
                      onTap: () {
                        //open alert to select assist
                      },
                    )),
              ],
            )
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    screenHeight = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Permit Menjalankan Kerja",
          style: TextStyle(
            letterSpacing: 1,
            fontFamily: 'Lato',
          ),
        ),
        centerTitle: true,
        leading: Builder(
          builder: (BuildContext context) {
            return IconButton(
              icon: const Icon(Icons.arrow_back),
              onPressed: () {
                Navigator.of(context).push(
                  MaterialPageRoute(builder: (BuildContext context) => PTW_JobSheet()),
                );
              },
            );
          },
        ),
      ),
      body: ListView(
        children: <Widget>[
          _infoOperation(),
          Container(
            height: screenHeight / 2,
            padding: EdgeInsets.all(10),
            child: Stack(
              children: <Widget>[
                FutureBuilder(
                    future: DefaultAssetBundle.of(context).loadString('data/SwitchingData.json'),
                    builder: (context, snapshot) {
                      var _data = jsonDecode(snapshot.data.toString());
                      if (snapshot.data == null) {
                        return Container(child: Center(child: Text("Loading...")));
                      } else {
                        return ListView(
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.fromLTRB(18, 25, 18, 0),
                              child: Row(
                                children: <Widget>[
                                  Expanded(child: Text('PERMIT TO WORK', style: _label)),
                                  _buttonAdd(),
                                  Divider(),
                                ],
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              ),
                            ),
                            ListView.builder(
                              physics: ScrollPhysics(),
                              shrinkWrap: true,
                              padding: EdgeInsets.all(8.0),
                              itemCount: _data == null ? 0 : _data.length,
                              itemBuilder: (BuildContext context, int index) {
                                return Container(
                                  decoration: BoxDecoration(
                                    border: Border(bottom: BorderSide(color: Colors.grey.shade200)),
                                  ),
                                  child: _buildListItem(_data[index]),
                                );
                              },
                            ),
                          ],
                        );
                      }
                    }),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
