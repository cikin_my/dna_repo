import 'package:dna_app/PTW/ptw_add_permit_to_work.dart';
import 'package:dna_app/PTW/view_borang.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class UploadForm extends StatefulWidget {
  @override
  _UploadFormState createState() => _UploadFormState();
}

class _UploadFormState extends State<UploadForm> {
  @override
  Widget build(BuildContext context) {

    
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Upload PTW",
          style: TextStyle(
            letterSpacing: 1,
            fontFamily: 'Lato',
          ),
        ),
        centerTitle: true,
        leading: Builder(
          builder: (BuildContext context) {
            return IconButton(

              icon: const Icon(Icons.arrow_back),
              onPressed: () {
                Navigator.of(context).push(
                  MaterialPageRoute(builder: (BuildContext context) => AddPermitToWork()),
                );
              },
            );
          },
        ),
      ),
      body:Center(
        child: Container(

          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              InkWell(
                child: Container(                  
                  padding: new EdgeInsets.all(25.0),
                  decoration: BoxDecoration(
                    border: Border.all(color: Colors.grey[300],width:1)
                  ),
                  child: Image(
                    image: AssetImage('assets/images/camera.png'),
                    fit: BoxFit.cover,
                    height: 100,
                  ),

                ),
                onTap: (){
                  setState(() {
                    Navigator.of(context).push(
                      MaterialPageRoute(builder: (BuildContext context) => ViewBorang()),
                    );
                  });
                },
                splashColor: Colors.grey[200],
              ),
              Text('take picture form 1'),

              SizedBox(height: 20,),

              InkWell(
                child: Container(
                  padding: new EdgeInsets.all(25.0),
                  decoration: BoxDecoration(
                      border: Border.all(color: Colors.grey[300],width:1)
                  ),
                  child: Image(
                    image: AssetImage('assets/images/camera.png'),
                    fit: BoxFit.cover,
                    height: 100,
                  ),
                ),
                onTap: (){
                  Navigator.of(context).push(
                    MaterialPageRoute(builder: (BuildContext context) => ViewBorang()),
                  );
                },
                splashColor: Colors.grey[200],
              ),
              Text('take picture form 2'),
            ],
          ),
          alignment: FractionalOffset(0.5, 0.5),
        ),
      ),
    );
  }
}
