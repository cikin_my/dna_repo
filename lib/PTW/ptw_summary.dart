import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_custom_clippers/flutter_custom_clippers.dart';
import 'package:grouped_buttons/grouped_buttons.dart';

class PtwSummary extends StatefulWidget {
  @override
  _PtwSummaryState createState() => _PtwSummaryState();
}

class _PtwSummaryState extends State<PtwSummary> {
  double screenHeight;

  TextStyle _label = TextStyle(
    fontFamily: 'opansans',
    color: Colors.blueGrey.shade700,
    fontSize: 16.0,
    fontWeight: FontWeight.w700,
    letterSpacing: 1,
  );
  TextStyle _info = TextStyle(
    fontFamily: 'opansans',
    fontSize: 15.0,
    fontWeight: FontWeight.normal,
    letterSpacing: 0.75,
  );

  /*  *         widgets
  * ========================================*/
  Widget _infoContainer() {
    return Container(
      height: 170,
      margin: const EdgeInsets.all(8.0),
      padding: const EdgeInsets.all(10.0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15.0),
        color: Colors.grey[200],
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            children: <Widget>[
              Text(
                (' Summary '),
                style: TextStyle(fontWeight: FontWeight.bold, letterSpacing: 2, color: Colors.black54, fontSize: 14, fontFamily: 'Lato'),
                textAlign: TextAlign.center,
              ),
            ],
            mainAxisAlignment: MainAxisAlignment.center,
          ),
          Divider(),
          SizedBox(
            height: 15,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Text(
                'No. JS:  ',
                style: _label,
              ),
              Text(
                'ABC-12-12345',
                style: _info,
              ),
            ],
          ),
          SizedBox(
            height: 5,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Text(
                'No. Permit:  ',
                style: _label,
              ),
              Text(
                'JC00018000901',
                style: _info,
              ),
            ],
          ),
          SizedBox(
            height: 5,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Text(
                'Stesen:  ',
                style: _label,
              ),
              Text(
                'Kuantan',
                style: _info,
              ),
            ],
          ),
          SizedBox(
            height: 5,
          ),
        ],
      ),
    );
  }

  Widget _generalForm() {
    return Padding(
      padding: const EdgeInsets.all(18.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            'No. Authorisation/Sanction :',
            style: _label,
          ),
          SizedBox(
            height: 8,
          ),
          Row(
            children: <Widget>[
              Text(
                '(RCC) :',
                style: _label,
              ),
              Text(
                'XYZABC1230000',
                style: _info,
              ),
            ],
          ),
          SizedBox(height: 8),
          Row(
            children: <Widget>[
              Text(
                '(AP) :',
                style: _label,
              ),
              Text(
                'XYZABC1230000',
                style: _info,
              ),
            ],
          ),
          SizedBox(height: 18),
          Row(
            children: <Widget>[
              Text(
                'Nama OKST/OP :',
                style: _label,
              ),
              Text(
                'Eddard Stark',
                style: _info,
              ),
            ],
          ),
          SizedBox(
            height: 18,
          ),
        ],
      ),
    );
  }

  Widget _bahagian_A() {
    return Container(
      margin: EdgeInsets.all(8),
      padding: EdgeInsets.all(8),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            width: double.infinity,
            color: Colors.blue[50],
            padding: EdgeInsets.all(18),
            child: Text(
              'BAHAGIAN A',
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold, letterSpacing: 1),
              textAlign: TextAlign.center,
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 18.0, bottom: 10),
            child: Text('Punca Bekalan dimatikan dan Tempat Pembumian Utama Litar di mana radas dibumi serta dilitar pintaskan'),
          ),
          Text(
            'Nama dan No Suis (PMU/ PPU/ SSU/ PE/ PAT/ BLACKBOX/ FEEDER PILLAR/ LVDB)',
            style: _label,
          ),
          /*nama_nosuis*/
          Container(
            padding: EdgeInsets.all(18),
            margin: EdgeInsets.only(bottom: 18, top: 10),
            width: double.infinity,
            color: Colors.grey[100],
            child: Text(
              'RSTL-NE-012345678',
              style: _info,
            ),
          ),
          Text(
            'Radas di mana kerja dibuat :',
            style: _label,
          ),
          /*radas*/
          Container(
            padding: EdgeInsets.all(18),
            margin: EdgeInsets.only(bottom: 18, top: 10),
            width: double.infinity,
            color: Colors.grey[100],
            child: Text(
              'CBT-0076533',
              style: _info,
            ),
          ),
          Text(
            'Jenis kerja yang dijalankan :',
            style: _label,
          ),
          /*jeniskerja*/
          Container(
            padding: EdgeInsets.all(18),
            margin: EdgeInsets.only(bottom: 18, top: 10),
            width: double.infinity,
            color: Colors.grey[100],
            child: Text(
              'Maintenance',
              style: _info,
            ),
          ),
          /*jeniskunci */
          Text(
            'Jenis Kunci :',
            style: _label,
          ),
          Container(
            padding: EdgeInsets.all(18),
            margin: EdgeInsets.only(bottom: 18, top: 10),
            width: double.infinity,
            color: Colors.grey[100],
            child: Text(
              'LOTO Box',
              style: _info,
            ),
          ),
          Divider(),
          Container(
              padding: EdgeInsets.all(8),
              margin: EdgeInsets.only(top: 20, bottom: 18),
              decoration: BoxDecoration(border: Border.all(width: 1, color: Colors.blueGrey)),
              child: Text(
                'Saya mengaku bahawa langkah-langkah prosidur keselamatan telah dipatuhi'
                'untuk memastikan kerja yang dijalankan adalah selamat.',
                textAlign: TextAlign.center,
              )),
          Container(
            child: Column(
              children: <Widget>[
                /*isTaklimatKerja*/
                CheckboxListTile(
                    title: Text('TAKLIMAT  KESELAMATAN & SKOP KERJA '),
                    subtitle: Text('sebelum kerja dimulakan kepada semua Orang Bekerja'),
                    controlAffinity: ListTileControlAffinity.platform,
                    value: true,
                    onChanged: null),
                /*isKelengkapanDiri*/
                CheckboxListTile(
                    title: Text('KELENGKAPAN PERLINDUNGAN DIRI'),
                    subtitle: Text('Orang Bekerja Lengkap'),
                    controlAffinity: ListTileControlAffinity.platform,
                    value: true,
                    onChanged: null),
                /*isHazard*/
                CheckboxListTile(
                    title: Text('KENALPASTI HAZARD (HI).'), subtitle: Text('Sila Pilih'), controlAffinity: ListTileControlAffinity.platform, value: true, onChanged: null),
                /*kenalpastihazard*/
                Container(
                  padding: EdgeInsets.all(18),
                  margin: EdgeInsets.only(bottom: 18),
                  width: double.infinity,
                  color: Colors.grey[100],
                  child: Text(
                    'LABEL NOMBOR SUIS & NAMA F',
                    style: _info,
                  ),
                ),
                /*isLangkahKawalan*/
                CheckboxListTile(
                    title: Text('LANGKAH KAWALAN BAGI HAZARD DINYATAKAN. '),
                    subtitle: Text('(KAWALAN RISIKO - DC)'),
                    controlAffinity: ListTileControlAffinity.platform,
                    value: true,
                    onChanged: null),
                /*langkahkawalan*/
                Container(
                  padding: EdgeInsets.all(18),
                  width: double.infinity,
                  color: Colors.grey[100],
                  child: Text(
                    'pasang papan tanda',
                    style: _info,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _bahagian_C_1() {
    return Container(
      padding: EdgeInsets.all(18),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SizedBox(
            height: 20,
          ),
          Container(
            width: double.infinity,
            color: Colors.blue[50],
            padding: EdgeInsets.all(18),
            child: Text(
              'BAHAGIAN C',
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold, letterSpacing: 1),
              textAlign: TextAlign.center,
            ),
          ),
          SizedBox(
            height: 20,
          ),
          Text('(1) Orang Berkecekapan', style: _label),
          Container(
            padding: EdgeInsets.all(8),
            child: Text(
              'Saya menerima Permit Menjalankan Kerja ini mengaku terima dan faham perkara berikut:.',
              style: TextStyle(fontSize: 16),
            ),
          ),
          Container(
            child: Column(
              children: <Widget>[
                CheckboxListTile(title: Text('Skop kerja yang dibenarkan'), value: true, onChanged: null),
                CheckboxListTile(title: Text('Tempat Pembumian Utama Litar'), value: true, onChanged: null),
                CheckboxListTile(title: Text('Memakai Kelengkapan Perlindungan Diri Lengkap'), value: true, onChanged: null),
                CheckboxListTile(title: Text('Tempat Pembumian Tambahan (jika ada)'), value: true, onChanged: null),
                Container(
                  padding: EdgeInsets.all(18),
                  margin: EdgeInsets.only(bottom: 10),
                  width: double.infinity,
                  color: Colors.grey[100],
                  child: Text(
                    'Test pembumian tambahan'
                    '',
                    style: _info,
                  ),
                ),
                CheckboxListTile(title: Text('Tempat Kunci NSL Tambahan (jika ada)'), value: true, onChanged: null),
                Container(
                  padding: EdgeInsets.all(18),
                  width: double.infinity,
                  color: Colors.grey[100],
                  child: Text(
                    'Test kunci NSL Tambahan',
                    style: _info,
                  ),
                ),
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.all(18),
            margin: EdgeInsets.only(top: 20),
            decoration: BoxDecoration(color: Colors.grey[100], border: Border.all(width: 1, color: Colors.blueGrey)),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[Text('Nama Orang Berkecekapan: '), Text(' Howard Wolowitz')],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[Text('No. Pekerja:'), Text('10055567')],
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget _bahagian_C_2(data) {
    return Container(
      padding: const EdgeInsets.fromLTRB(18, 25, 18, 30),
      child: Column(
        children: <Widget>[
          Text('(2) Orang Bekerja di bawah penyeliaan Orang Berkecekapan', style: _label),
          ListView.builder(
            shrinkWrap: true,
            itemExtent: 30,
            itemCount: data == null ? 0 : data.length, //_listViewData.length,
            itemBuilder: (context, index) => Container(
              child: ListTile(
                title: Row(
                  children: <Widget>[
                    Icon(Icons.check_circle, size: 14, color: Colors.green),
                    SizedBox(
                      width: 10,
                    ),
                    Text(
                      data[index]['name'],
                      style: TextStyle(fontWeight: FontWeight.w600),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Text(
                      '( N/P: ' + data[index]['np'] + ')',
                      style: TextStyle(fontWeight: FontWeight.w400),
                    ),
                  ],
                ),
                // _onSelected(index),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _btnSelesai() {
    return RaisedButton.icon(
      onPressed: () => {
        setState(() {
          Navigator.of(context).push(
            MaterialPageRoute(builder: (BuildContext context) => PtwSummary()),
          );
        }),
      },
      icon: Icon(
        Icons.assignment_turned_in,
        size: 16,
      ),
      label: Row(
        children: <Widget>[
          Text('KERJA SELESAI', style: TextStyle(letterSpacing: 1)),
        ],
      ),
      color: Colors.green,
      splashColor: Colors.green[700],
      textColor: Colors.white,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
    );
  }

  @override
  Widget build(BuildContext context) {
    screenHeight = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Permit Menjalankan Kerja",
          style: TextStyle(
            letterSpacing: 1,
            fontFamily: 'Lato',
          ),
          textAlign: TextAlign.center,
        ),
        centerTitle: true,
        leading: Builder(
          builder: (BuildContext context) {
            return IconButton(
              icon: const Icon(Icons.arrow_back),
              onPressed: () {
//                Navigator.of(context).push(
//                  MaterialPageRoute(builder: (BuildContext context) => AddPermitToWork()),
//                );
              },
            );
          },
        ),
      ),
      body: Container(
        height: double.maxFinite,
        child: Stack(
          children: <Widget>[
            FutureBuilder(
                future: DefaultAssetBundle.of(context).loadString('data/worker.json'),
                builder: (context, snapshot) {
                  var _data = jsonDecode(snapshot.data.toString());
                  if (snapshot.data == null) {
                    return Container(child: Center(child: Text("Loading...")));
                  } else {
                    return ListView(
                      children: <Widget>[
                        _infoContainer(),
                        _generalForm(),
                        _bahagian_A(),
                        _bahagian_C_1(),
                        _bahagian_C_2(_data),
                        Align(alignment: Alignment.bottomCenter, child: _btnSelesai()),
                      ],
                    );
                  }
                }),
          ],
        ),
      ),
    );
  }
}
