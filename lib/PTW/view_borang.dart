import 'package:dna_app/PTW/upload_manual_form.dart';
import 'package:flutter/material.dart';
import 'package:photo_view/photo_view.dart';




class ViewBorang extends StatefulWidget {
  @override
  _ViewBorangState createState() => _ViewBorangState();
}

class _ViewBorangState extends State<ViewBorang> {
  @override
  Widget build(BuildContext context) {

    double _scale = 1.0;
    double _previousScale = 1.0;

    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Borang Permit",
          style: TextStyle(
            letterSpacing: 1,
            fontFamily: 'Lato',
          ),
        ),
        centerTitle: true,
        leading: Builder(
          builder: (BuildContext context) {
            return IconButton(

              icon: const Icon(Icons.arrow_back),
              onPressed: () {
                Navigator.of(context).push(
                  MaterialPageRoute(builder: (BuildContext context) => UploadForm()),
                );
              },
            );
          },
        ),
      ),
      body:Center(
        child: Container(
          child: ListView(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text('Form part 1',textAlign: TextAlign.center,style: TextStyle(fontSize: 16,color: Colors.blueGrey),),
              ),
              GestureDetector(
                child: Image(
                  image: AssetImage('assets/images/ptw1.png'),
                  fit: BoxFit.contain,
                  height: 350,
                ),
                onTap: (){
                  Navigator.push(context, MaterialPageRoute(builder: (_) {
                    return DetailScreen();
                  }));
                },
              ),


              SizedBox(height: 20,),

              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text('Form part 2',textAlign: TextAlign.center,style: TextStyle(fontSize: 16,color: Colors.blueGrey),),
              ),
              GestureDetector(
                child: Image(
                  image: AssetImage('assets/images/ptw2.png'),
                  fit: BoxFit.contain,
                  height: 350,
                ),
                onTap: (){
                  Navigator.push(context, MaterialPageRoute(builder: (_) {
                    return DetailScreen2();
                  }));
                },
              ),

            ],
          ),

        ),
      ),
    );
  }
}

class DetailScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GestureDetector(
        child: Center(
          child: Hero(
            tag: 'imageHero',
            child: Container(
                child: PhotoView(
                  imageProvider: AssetImage("assets/images/ptw1.png"),
                )
            ),
          ),
        ),
        onTap: () {
          Navigator.pop(context);
        },
      ),
    );
  }
}
class DetailScreen2 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GestureDetector(
        child: Center(
          child: Hero(
            tag: 'imageHero',
            child: Container(
                child: PhotoView(
                  imageProvider: AssetImage("assets/images/ptw2.png"),
                )
            ),
          ),
        ),
        onTap: () {
          Navigator.pop(context);
        },
      ),
    );
  }
}
