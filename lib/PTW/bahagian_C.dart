import 'package:dna_app/PTW/bahagian_D.dart';
import 'package:dna_app/PTW/bahagian_E.dart';
import 'package:flutter/material.dart';

class BahagianC extends StatefulWidget {
  @override
  _BahagianCState createState() => _BahagianCState();
}

class _BahagianCState extends State<BahagianC> {


  /*========================================
  *         style
  * ========================================*/
  TextStyle _label = TextStyle(
    fontFamily: 'opansans',
    color: Colors.blueGrey.shade700,
    fontSize: 16.0,
    fontWeight: FontWeight.w700,
    letterSpacing: 1,
  );
  TextStyle _info = TextStyle(
    fontFamily: 'opansans',
    fontSize: 15.0,
    fontWeight: FontWeight.normal,
    letterSpacing: 0.75,
  );


  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Divider(),
        SizedBox(
          height: 20,
        ),
        Container(
          color: Colors.blue[50],
          child: ListTile(
            title: Text(
              'BAHAGIAN C',
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold, letterSpacing: 1),
              textAlign: TextAlign.center,
            ),
            trailing: Icon(
              Icons.edit,
              color: Colors.blue,
            ),
          ),
        ),
        Container(
          child: Column(
            children: <Widget>[
              Container(
                padding: EdgeInsets.all(10),
                child: Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15.0),
                  ),
                  color: Colors.grey[100],
                  child: Padding(
                    padding: const EdgeInsets.all(18.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          'No. Permit ',
                          style: _label,
                        ),
                        Text(
                          'JC00018000901',
                          style: _info,
                        ),
                        SizedBox(height: 8),
                        Text(
                          'Keterangan Kerja ',
                          style: _label,
                        ),
                        Row(
                          children: <Widget>[
                            Expanded(
                              child: Text(
                                'PREVENTIVE MAINTENANCE VCB DAN BUSBAR - TOTAL SHUTDOWN',
                                style: _info,
                              ),
                            ),
                          ],
                        ),
                        SizedBox(height: 8),
                        Text(
                          'Jenis radas: ',
                          style: _label,
                        ),
                        Text(
                          'BUSBAR',
                          style: _info,
                        ),
                        SizedBox(height: 8),
                        Text(
                          'Status: ',
                          style: _label,
                        ),
                        Text(
                          'Work In Progress',
                          style: _info,
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.all(10),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      'Sila Pilih Aktiviti Seterusnya:',
                      style: _label,
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Container(
                      width: double.infinity,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Text('Notis keluar Tapak'),
                          RaisedButton.icon(
                            onPressed: () => {
                              setState(() {

                              }),
                              Navigator.of(context).push(
                                MaterialPageRoute(builder: (BuildContext context) => BahagianD()),
                              )
                            },
                            icon: Icon(
                              Icons.assignment,
                              size: 23,
                            ),
                            label: Row(
                              children: <Widget>[
                                Text('PENGUJIAN', style: TextStyle(letterSpacing: 1)),
                              ],
                            ),
                            color: Colors.blue,
                            splashColor: Colors.blue[700],
                            textColor: Colors.white,
                            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
                          ),
                          SizedBox(height: 15),
                          Text('Notis Masuk Semula Tapak'),
                          RaisedButton.icon(
                            onPressed: () => {
                              //only enable when _ispengujian is enable
                              Navigator.of(context).push(
                                MaterialPageRoute(builder: (BuildContext context) => BahagianE()),
                              )
                            },
                            icon: Icon(
                              Icons.assignment_turned_in,
                              size: 23,
                            ),
                            label: Row(
                              children: <Widget>[
                                Text('SELESAI PENGUJIAN', style: TextStyle(letterSpacing: 1)),
                              ],
                            ),
                            color: Colors.blue,
                            splashColor: Colors.blue[700],
                            textColor: Colors.white,
                            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(height: 30),
              Align(
                alignment: Alignment.bottomCenter,
                child: RaisedButton.icon(

                  onPressed: null,
                  icon: Icon(
                    Icons.done_all,
                    size: 23,
                  ),
                  label: Row(
                    children: <Widget>[
                      Text('KERJA SELESAI', style: TextStyle(letterSpacing: 1)),
                    ],
                  ),
                  color: Colors.green,
                  splashColor: Colors.blue[700],
                  textColor: Colors.white,
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
                ),
              )
            ],
          ),
        ),
      ],
    );
  }
}
