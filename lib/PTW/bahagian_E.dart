import 'package:dna_app/PTW/bahagian_C.dart';
import 'package:dna_app/PTW/pte_form.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class BahagianE extends StatefulWidget {
  @override
  _BahagianEState createState() => _BahagianEState();
}

class _BahagianEState extends State<BahagianE> {
  TextStyle _label = TextStyle(
    fontFamily: 'opansans',
    color: Colors.blueGrey.shade700,
    fontSize: 16.0,
    fontWeight: FontWeight.w700,
    letterSpacing: 1,
  );
  TextStyle _info = TextStyle(
    fontFamily: 'opansans',
    fontSize: 15.0,
    fontWeight: FontWeight.normal,
    letterSpacing: 0.75,
  );

  bool _checkbox_1 = false;
  bool _checkbox_2 = false;


  Widget _bahagian_E1() {
    return Column(
      children: <Widget>[
        Container(
          color: Colors.green[50],
          child: ListTile(
            title: Text(
              'BAHAGIAN E ',
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold, letterSpacing: 1),
              textAlign: TextAlign.center,
            ),
            trailing: Icon(
              Icons.edit,
              color: Colors.deepPurple,
            ),
          ),
        ),
        Container(
          child: Column(
            children: <Widget>[
              Container(
                padding: EdgeInsets.all(10),
                child: Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15.0),
                  ),
                  color: Colors.grey[100],
                  child: Padding(
                    padding: const EdgeInsets.all(18.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          'No. Permit ',
                          style: _label,
                        ),
                        Text(
                          'JC00018000901',
                          style: _info,
                        ),
                        SizedBox(height: 8),
                        Text(
                          'Keterangan Kerja ',
                          style: _label,
                        ),
                        Row(
                          children: <Widget>[
                            Expanded(
                              child: Text(
                                'PREVENTIVE MAINTENANCE VCB DAN BUSBAR - TOTAL SHUTDOWN',
                                style: _info,
                              ),
                            ),
                          ],
                        ),
                        SizedBox(height: 8),
                      ],
                    ),
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.all(18),
                child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      'BAHAGIAN E - E1',
                      style: _label,
                    ),
                    SizedBox(height: 10),
                    Text(
                      'NOTIS MASUK TAPAK SEMULA - SELESAI PENGUJIAN',
                      style: _info, textAlign: TextAlign.center,
                    ),
                    SizedBox(height: 8),
                    Text(
                      'Dengan ini saya mengaku bahawa kerja-kerja pengujian telah siap dijalankan:',
                      style: TextStyle(fontSize: 16),
                      textAlign: TextAlign.center,
                    ),
                    Divider(),
                    Text(
                      'Perkara di ruangan Bahagian A iaitu (a), (b) dan (c) telah DIBUMIKAN dan DIKUNCI kembali kepada kedudukan seperti yang dinyatakan serta kerja boleh '
                      'diteruskan',
                      style: _info,
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(height: 28),
                    Container(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text(
                                'Nama AP: ',
                                style: _label,
                              ),
                              Text('ADDERD STARK', style: _info),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text(
                                'Staff ID: ',
                                style: _label,
                              ),
                              Text('100100010', style: _info),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text(
                                'Time: ',
                                style: _label,
                              ),
                              Text('12-09-2020-1840', style: _info),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 20,
              ),
            ],
          ),
        ),
      ],
    );
  }

  Widget _bahagian_E2() {
    return Column(
      children: <Widget>[
        Container(
          child: Column(
            children: <Widget>[
              Container(
                padding: EdgeInsets.all(18),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      'BAHAGIAN E - E2',
                      style: _label,
                    ),
                    SizedBox(height: 10),
                    Text(
                      'NOTIS KELUAR TAPAK  - TUJUAN PENGUJIAN',
                      style: _info,
                    ),
                    SizedBox(height: 8),
                    Text(
                      'Dengan ini saya mengaku bahawa kerja boleh dijalankan:',
                      style: TextStyle(fontSize: 16),
                      textAlign: TextAlign.center,
                    ),
                  ],
                ),
              ),
              Divider(),
              CheckboxListTile(
                title: Text('Pembumian Tambahan dipasang semula'),
                controlAffinity: ListTileControlAffinity.platform,
                value: _checkbox_1,
                onChanged: (bool value) {
                  setState(() {
                    _checkbox_1 = value;
                  });
                },
              ),
              Divider(),
              CheckboxListTile(
                title: Text('Kunci NSL Tambahan telah dipasang semula'),
                controlAffinity: ListTileControlAffinity.platform,
                value: _checkbox_2,
                onChanged: (bool value) {
                  setState(() {
                    _checkbox_2 = value;
                  });
                },
              ),
              SizedBox(height: 28),
              Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          'Nama CP: ',
                          style: _label,
                        ),
                        Text('Howard Wolowitz', style: _info),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          'Staff ID: ',
                          style: _label,
                        ),
                        Text('100100010', style: _info),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          'Time: ',
                          style: _label,
                        ),
                        Text('12-09-2020-1840', style: _info),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  Widget _buildButton_E1() {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        RaisedButton.icon(
          onPressed: () => {
            Navigator.of(context).push(
              MaterialPageRoute(builder: (BuildContext context) => PTW_form()),
            )
          },
          icon: Icon(
            Icons.check,
            size: 16,
          ),
          label: Row(
            children: <Widget>[
              Text('Confirm', style: TextStyle(letterSpacing: 1)),
            ],
          ),
          color: Colors.green,
          splashColor: Colors.green[700],
          textColor: Colors.white,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
        ),
      ],
    );
  }

  Widget _buildButton_E2() {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        RaisedButton.icon(
          onPressed: () => {
            Navigator.of(context).push(
              MaterialPageRoute(builder: (BuildContext context) => PTW_form()),
            )
          },
          icon: Icon(
            Icons.check,
            size: 16,
          ),
          label: Row(
            children: <Widget>[
              Text('Confirm', style: TextStyle(letterSpacing: 1)),
            ],
          ),
          color: Colors.green,
          splashColor: Colors.green[700],
          textColor: Colors.white,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Permit Menjalankan Kerja",
          style: TextStyle(
            letterSpacing: 1,
            fontFamily: 'Lato',
          ),
        ),
        centerTitle: true,
        leading: Builder(
          builder: (BuildContext context) {
            return IconButton(
              icon: const Icon(Icons.arrow_back),
              onPressed: () {
                Navigator.of(context).push(
                  MaterialPageRoute(builder: (BuildContext context) => PTW_form()),
                );
              },
            );
          },
        ),
      ),
      body: ListView(
        children: <Widget>[
          _bahagian_E1(),
          _buildButton_E1(),
          SizedBox(height: 28),
          _bahagian_E2(),
          _buildButton_E2(),
        ],
      ),
    );
  }
}
