import 'package:dna_app/PTW/bahagian_A.dart';
import 'package:dna_app/PTW/bahagian_B.dart';
import 'package:dna_app/PTW/bahagian_C.dart';
import 'package:dna_app/PTW/ptw_add_permit_to_work.dart';
import 'package:dna_app/PTW/ptw_cancelled.dart';
import 'package:dna_app/PTW/ptw_list.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/widgets.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:grouped_buttons/grouped_buttons.dart';

class PTW_form extends StatefulWidget {
  @override
  _PTW_formState createState() => _PTW_formState();
}

class _PTW_formState extends State<PTW_form> {

  /*========================================
  *         variables
  * ========================================*/
  double screenHeight;
  Item selectedUser;
  List<Item> Persons = <Item>[
    const Item(
        'Ahmad',
        Icon(
          Icons.person,
          color: const Color(0xFF167F67),
        )),
    const Item(
        'Hassan',
        Icon(
          Icons.person,
          color: const Color(0xFF167F67),
        )),
    const Item(
        'Merry',
        Icon(
          Icons.person,
          color: const Color(0xFF167F67),
        )),
    const Item(
        'Ahmad Ali',
        Icon(
          Icons.person,
          color: const Color(0xFF167F67),
        )),
  ];

  /*========================================
  *           style
  * ========================================*/
  TextStyle _label = TextStyle(
    fontFamily: 'opansans',
    color: Colors.blueGrey.shade700,
    fontSize: 16.0,
    fontWeight: FontWeight.w700,
    letterSpacing: 1,
  );
  TextStyle _info = TextStyle(
    fontFamily: 'opansans',
    fontSize: 15.0,
    fontWeight: FontWeight.normal,
    letterSpacing: 0.75,
  );

 /*==========================================
  *           Widgets
  * ========================================*/
  Widget _infoContainer() {
    return Container(
      padding: EdgeInsets.all(10),
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        color: Colors.grey[100],
        child: Padding(
          padding: const EdgeInsets.all(18.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              SizedBox(
                height: 15,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Text(
                    'No. JS:  ',
                    style: _label,
                  ),
                  Text(
                    'ABC-12-12345',
                    style: _info,
                  ),
                ],
              ),
              SizedBox(
                height: 5,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Text(
                    'No. Permit:  ',
                    style: _label,
                  ),
                  Text(
                    'JC00018000901',
                    style: _info,
                  ),
                ],
              ),
              SizedBox(
                height: 5,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Text(
                    'Stesen:  ',
                    style: _label,
                  ),
                  Text(
                    'Kuantan',
                    style: _info,
                  ),
                ],
              ),
              SizedBox(
                height: 5,
              ),
            ],
          ),
        ),
      ),
    );
  }
  Widget _generalForm() {
    return Padding(
      padding: const EdgeInsets.all(18.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            'No. Authorisation/Sanction :',
            style: _label,
          ),
          SizedBox(
            height: 8,
          ),
          TextField(
            decoration: InputDecoration(
              border: OutlineInputBorder(),
              labelText: '(RCC)',
              labelStyle: _label,
            ),
          ),
          SizedBox(height: 8),
          TextField(
            decoration: InputDecoration(border: OutlineInputBorder(), labelStyle: _label, labelText: '(AP)'),
          ),
          SizedBox(height: 18),
          Text(
            'Nama OKST/OP :',
            style: _label,
          ),
          Padding(
            padding: const EdgeInsets.only(left: 8.0, right: 8),
            child: DropdownButton<Item>(
              isExpanded: true,
              hint: Text(
                "Assign",
                style: _info,
              ),
              value: selectedUser,
              onChanged: (Item Value) {
                setState(() {
                  selectedUser = Value;
                });
              },
              items: Persons.map((Item user) {
                return DropdownMenuItem<Item>(
                  value: user,
                  child: Row(
                    children: <Widget>[
                      user.icon,
                      SizedBox(
                        width: 10,
                      ),
                      Text(
                        user.name,
                        style: TextStyle(color: Colors.black),
                      ),
                    ],
                  ),
                );
              }).toList(),
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    screenHeight = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Permit Menjalankan Kerja",
          style: TextStyle(
            letterSpacing: 1,
            fontFamily: 'Lato',
          ),
        ),
        centerTitle: true,
        leading: Builder(
          builder: (BuildContext context) {
            return IconButton(
              icon: const Icon(Icons.arrow_back),
              onPressed: () {
                Navigator.of(context).push(
                  MaterialPageRoute(builder: (BuildContext context) => AddPermitToWork()),
                );
              },
            );
          },
        ),
      ),
      body: ListView(
        children: <Widget>[
          _infoContainer(),
          _generalForm(),
          BahagianA(),
          BahagianB(),
          BahagianC(),

        ],
      ),
    );
  }
}


/*=========================================
 *          classes
 * ========================================*/
class Item {
  const Item(this.name, this.icon);
  final String name;
  final Icon icon;
}
