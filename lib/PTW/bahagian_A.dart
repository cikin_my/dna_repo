import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:grouped_buttons/grouped_buttons.dart';

class BahagianA extends StatefulWidget {
  @override
  _BahagianAState createState() => _BahagianAState();
}

class _BahagianAState extends State<BahagianA> {
  /*========================================
  *         variables
  * ========================================*/
  double screenHeight;
  bool _langkah_prosidure_1 = false;
  bool _langkah_prosidure_2 = false;
  bool _langkah_prosidure_3 = false;
  bool _langkah_prosidure_4 = false;
  String dropdownValue = 'LABEL NOMBOR SUIS & NAMA F';

  /*========================================
  *         radio Handler
  * ========================================*/
  int _radioValue1 = -1;

  void _handleRadioValueChange1(int value) {
    setState(() {
      _radioValue1 = value;

      switch (_radioValue1) {
        case 0:
          Fluttertoast.showToast(
              msg: "11KV",
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.CENTER,
              timeInSecForIosWeb: 1,
              backgroundColor: Colors.orange,
              textColor: Colors.white,
              fontSize: 14.0);
          break;
        case 1:
          Fluttertoast.showToast(
              msg: "33KV",
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.CENTER,
              timeInSecForIosWeb: 1,
              backgroundColor: Colors.orange,
              textColor: Colors.white,
              fontSize: 14.0);
          break;
      }
    });
  }

  /*========================================
  *         style
  * ========================================*/
  TextStyle _label = TextStyle(
    fontFamily: 'opansans',
    color: Colors.blueGrey.shade700,
    fontSize: 16.0,
    fontWeight: FontWeight.w700,
    letterSpacing: 1,
  );
  TextStyle _info = TextStyle(
    fontFamily: 'opansans',
    fontSize: 15.0,
    fontWeight: FontWeight.normal,
    letterSpacing: 0.75,
  );

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Divider(),
        Container(
          color: Colors.deepOrange[50],
          child: ListTile(
            title: Text(
              'BAHAGIAN A',
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold, letterSpacing: 1),
              textAlign: TextAlign.center,
            ),
            trailing: Icon(
              Icons.edit,
              color: Colors.deepOrange,
            ),
          ),
        ),
        Container(
          margin: EdgeInsets.all(8),
          padding: EdgeInsets.all(8),
          child: Column(
            children: <Widget>[
              Container(
                margin: EdgeInsets.all(8),
                padding: EdgeInsets.all(8),
                decoration: BoxDecoration(border: Border.all(width: 1, color: Colors.blueGrey)),
                child: Text(
                  'Saya mengaku bahawa semua yang bertanda di bawah telah dibuat dengan TEPAT '
                  'dan RADAS adalah selamat untuk dijalankan kerja ke atasnya.',
                  textAlign: TextAlign.justify,
                ),
              ),
              CheckboxGroup(
                labels: <String>[
                  "DIMATIKAN",
                  "DIASINGKAN",
                  "DIBUKTIKAN MATI",
                  "DIBUMIKAN",
                  "DIKUNCIKAN",
                  "DILETAKAN NOTIS BAHAYA & AWAS",
                  "DIHADANGKAN",
                ],
                onChange: (bool isChecked, String label, int index) => print("isChecked: $isChecked   label: $label  index: $index"),
                onSelected: (List<String> checked) => print("checked: ${checked.toString()}"),
              ),
              SizedBox(
                height: 18,
              ),
              Text('Punca Bekalan dimatikan dan Tempat Pembumian Utama Litar di mana radas dibumi serta dilitar pintaskan'),
              SizedBox(
                height: 18,
              ),
              Text(
                'Nama dan No Suis (PMU/ PPU/ SSU/ PE/ PAT/ BLACKBOX/ FEEDER PILLAR/ LVDB)',
                style: _label,
              ),
              TextField(
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              TextField(
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Radas di mana kerja dibuat',
                  labelStyle: _label,
                ),
              ),
              SizedBox(
                height: 10,
              ),
              TextField(
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Jenis kerja yang dijalankan',
                  labelStyle: _label,
                ),
              ),
              SizedBox(
                height: 8,
              ),
              Row(
                children: <Widget>[
                  Text(
                    'Jenis Kunci',
                    style: _label,
                  ),
                  SizedBox(
                    width: 8,
                  ),
                  Radio(
                    value: 0,
                    groupValue: _radioValue1,
                    onChanged: _handleRadioValueChange1,
                  ),
                  Text(
                    'LOTO box',
                    style: new TextStyle(fontSize: 14.0),
                  ),
                  Radio(
                    value: 1,
                    groupValue: _radioValue1,
                    onChanged: _handleRadioValueChange1,
                  ),
                  Text(
                    'HASP Lock',
                    style: new TextStyle(
                      fontSize: 14.0,
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 8,
              ),
              Container(
                  margin: EdgeInsets.all(8),
                  padding: EdgeInsets.all(8),
                  decoration: BoxDecoration(border: Border.all(width: 1, color: Colors.blueGrey)),
                  child: Text('Saya mengaku bahawa langkah-langkah '
                      'prosidur keselamatan telah dipatuhi untuk memastikan kerja '
                      'yang dijalankan adalah selamat.')),
              SizedBox(
                height: 18,
              ),
              Container(
                child: Column(
                  children: <Widget>[
                    CheckboxListTile(
                      title: Text('TAKLIMAT  KESELAMATAN & SKOP KERJA '),
                      subtitle: Text('sebelum kerja dimulakan kepada semua Orang Bekerja'),
                      controlAffinity: ListTileControlAffinity.platform,
                      value: _langkah_prosidure_1,
                      onChanged: (bool value) {
                        setState(() {
                          _langkah_prosidure_1 = value;
                        });
                      },
                    ),
                    CheckboxListTile(
                      title: Text('KELENGKAPAN PERLINDUNGAN DIRI'),
                      subtitle: Text('Orang Bekerja Lengkap'),
                      controlAffinity: ListTileControlAffinity.platform,
                      value: _langkah_prosidure_2,
                      onChanged: (bool value) {
                        setState(() {
                          _langkah_prosidure_2 = value;
                        });
                      },
                    ),
                    CheckboxListTile(
                      title: Text('KENALPASTI HAZARD (HI).'),
                      subtitle: Text('Sila Pilih'),
                      controlAffinity: ListTileControlAffinity.platform,
                      value: _langkah_prosidure_3,
                      onChanged: (bool value) {
                        setState(() {
                          _langkah_prosidure_3 = value;
                        });
                      },
                    ),
                    Visibility(
                      visible: _langkah_prosidure_3,
                      child: Container(
                          padding: EdgeInsets.symmetric(
                            horizontal: 10.0,
                          ),
                          decoration: BoxDecoration(
                            border: Border.all(
                              color: Colors.grey[400], //
                              width: 1,
                            ),
                            borderRadius: BorderRadius.all(Radius.circular(5.0) //
                                ),
                          ),
                          child: DropdownButtonHideUnderline(
                            child: DropdownButton<String>(
                              isExpanded: true,
                              value: dropdownValue,
                              icon: Icon(Icons.keyboard_arrow_down),
                              iconSize: 24,
                              elevation: 16,
                              onChanged: (String newValue) {
                                setState(() {
                                  dropdownValue = newValue;
                                });
                              },
                              items: <String>['LABEL NOMBOR One', 'LABEL NOMBOR SUIS & NAMA F', 'LABEL NOMBOR three', 'LABEL NOMBOR Four']
                                  .map<DropdownMenuItem<String>>((String value) {
                                return DropdownMenuItem<String>(
                                  value: value,
                                  child: Text(value),
                                );
                              }).toList(),
                            ),
                          )
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    //langkah prosidure 4
                    CheckboxListTile(
                      title: Text('LANGKAH KAWALAN BAGI HAZARD DINYATAKAN. '),
                      subtitle: Text('(KAWALAN RISIKO - DC)'),
                      controlAffinity: ListTileControlAffinity.platform,
                      value: _langkah_prosidure_4,
                      onChanged: (bool value) {
                        setState(() {
                          _langkah_prosidure_4 = value;
                        });
                      },
                    ),
                    Visibility(
                      visible: _langkah_prosidure_4,
                      child: TextField(
                        decoration: InputDecoration(
                          border: OutlineInputBorder(),
                          labelText: 'Nyatakan',
                          labelStyle: _label,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
