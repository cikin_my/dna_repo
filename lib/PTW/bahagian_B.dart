import 'package:flutter/material.dart';

class BahagianB extends StatefulWidget {
  @override
  _BahagianBState createState() => _BahagianBState();
}

class _BahagianBState extends State<BahagianB> {
  /*========================================
  *         variables
  * ========================================*/
  double screenHeight;

  /*========================================
  *         style
  * ========================================*/
  TextStyle _label = TextStyle(
    fontFamily: 'opansans',
    color: Colors.blueGrey.shade700,
    fontSize: 16.0,
    fontWeight: FontWeight.w700,
    letterSpacing: 1,
  );
  TextStyle _info = TextStyle(
    fontFamily: 'opansans',
    fontSize: 15.0,
    fontWeight: FontWeight.normal,
    letterSpacing: 0.75,
  );

  Widget _buttonConfirm() {
    return RaisedButton.icon(
      onPressed: () => {
        showAlertDialog(context),
      },
      icon: Icon(
        Icons.check,
        size: 23,
      ),
      label: Row(
        children: <Widget>[
          Text('CONFIRM', style: TextStyle(letterSpacing: 1)),
        ],
      ),
      color: Colors.blue,
      splashColor: Colors.blue[700],
      textColor: Colors.white,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
    );
  }

  showAlertDialog(BuildContext context) {
    // set up the buttons
    Widget cancelButton = FlatButton(
      child: Text(
        "Cancel",
        style: TextStyle(letterSpacing: 1),
      ),
      onPressed: () {
        Navigator.pop(context);
      },
      color: Colors.grey,
      textColor: Colors.white,
      splashColor: Colors.grey[700],
    );
    Widget continueButton = FlatButton(
      child: Text(
        "Confirm",
        style: TextStyle(letterSpacing: 1),
      ),
      onPressed: () {
        setState(() {});
        Navigator.pop(context);
      },
      color: Colors.green,
      textColor: Colors.white,
      splashColor: Colors.red[700],
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      titlePadding: EdgeInsets.all(0),
      contentPadding: EdgeInsets.all(0),
      contentTextStyle: TextStyle(color: Colors.black, fontSize: 12, fontFamily: 'opensans'),
      title: Container(
        padding: EdgeInsets.all(10),
        decoration: BoxDecoration(color: Colors.green),
        child: Text(
          "Permit Menjalankan Kerja",
          style: TextStyle(color: Colors.white, letterSpacing: 1, fontSize: 16),
          textAlign: TextAlign.center,
        ),
      ),
      content: Container(
        height: screenHeight / 2,
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              SizedBox(
                height: 18,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text('AP NAME: '),
                  Text('Ariyya Stark'),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text('Staff ID: '),
                  Text('100100001'),
                ],
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  'Pengesahan untuk mengeluarkan Permit Menjalankan Kerja untuk:',
                  textAlign: TextAlign.center,
                ),
              ),
              SizedBox(height: 20),
              Container(
                margin: EdgeInsets.all(8),
                padding: EdgeInsets.all(8),
                decoration: BoxDecoration(border: Border.all(width: 1, color: Colors.blueGrey)),
                child: Column(
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Expanded(
                            child: Text(
                          'No JS :',
                          style: _label,
                          textAlign: TextAlign.right,
                        )),
                        Expanded(
                            child: Text(
                          'ABC-12-12345',
                          style: _info,
                          textAlign: TextAlign.left,
                        )),
                      ],
                    ),
                    SizedBox(height: 4),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Expanded(
                            child: Text(
                          'No Permit :',
                          style: _label,
                          textAlign: TextAlign.right,
                        )),
                        Expanded(
                            child: Text(
                          'JC00018000901',
                          style: _info,
                          textAlign: TextAlign.left,
                        )),
                      ],
                    ),
                    SizedBox(height: 4),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Expanded(
                            child: Text(
                          'Stesen :',
                          style: _label,
                          textAlign: TextAlign.right,
                        )),
                        Expanded(
                            child: Text(
                          'kuantan',
                          style: _info,
                          textAlign: TextAlign.left,
                        )),
                      ],
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
      actions: [
        cancelButton,
        continueButton,
      ],
    );
    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    screenHeight = MediaQuery.of(context).size.height;
    return Column(
      children: <Widget>[
        Divider(),
        SizedBox(
          height: 20,
        ),
        Container(
          color: Colors.lightGreen[50],
          child: ListTile(
            title: Text(
              'BAHAGIAN B',
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold, letterSpacing: 1),
              textAlign: TextAlign.center,
            ),
            trailing: Icon(
              Icons.edit,
              color: Colors.lightGreen,
            ),
          ),
        ),
        Container(
          child: Column(
            children: <Widget>[
              Container(
                  width: double.infinity,
                  padding: EdgeInsets.all(15),
                  margin: EdgeInsets.all(10),
                  decoration: BoxDecoration(
                    color: Colors.blue[800],
                  ),
                  child: Text(
                    'PENGESAHAN',
                    style: TextStyle(color: Colors.white, letterSpacing: 1, fontSize: 16),
                    textAlign: TextAlign.center,
                  )),
              Container(
                padding: EdgeInsets.all(18),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      'Bahagian B',
                      style: TextStyle(letterSpacing: 1, fontWeight: FontWeight.w600),
                    ),
                    SizedBox(
                      height: 8,
                    ),
                    Text(
                      'Peringatan : PEMBUMIAN UTAMA LITAR tidak boleh diubah atau dialih '
                      'KECUALI  oleh ORANG BERKEBENARAN seperti tandatangan di bawah:',
                      style: TextStyle(fontWeight: FontWeight.w500, fontSize: 18),
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(height: 20),
                    Container(
                      margin: EdgeInsets.all(8),
                      padding: EdgeInsets.all(8),
                      decoration: BoxDecoration(border: Border.all(width: 1, color: Colors.blueGrey)),
                      child: Text(
                        'Saya mengaku bahawa langkah-langkah prosidur keselamatan telah '
                        'dipatuhi untuk memastikan kerja yang dijalankan adalah selamat.',
                        style: TextStyle(fontSize: 16),
                        textAlign: TextAlign.center,
                      ),
                    ),
                    SizedBox(height: 20),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Expanded(
                            child: Text(
                          'QP Name :',
                          style: _label,
                          textAlign: TextAlign.right,
                        )),
                        Expanded(
                            child: Text(
                          'EDDARD STARK',
                          style: _info,
                          textAlign: TextAlign.left,
                        )),
                      ],
                    ),
                    SizedBox(height: 4),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Expanded(
                            child: Text(
                          'Staff ID :',
                          style: _label,
                          textAlign: TextAlign.right,
                        )),
                        Expanded(
                            child: Text(
                          '100100010s',
                          style: _info,
                          textAlign: TextAlign.left,
                        )),
                      ],
                    ),
                    SizedBox(height: 4),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Expanded(
                            child: Text(
                          'Timestamp :',
                          style: _label,
                          textAlign: TextAlign.right,
                        )),
                        Expanded(
                            child: Text(
                          '20-09-2020-18-06',
                          style: _info,
                          textAlign: TextAlign.left,
                        )),
                      ],
                    )
                  ],
                ),
              )
            ],
          ),
        ),
        _buttonConfirm(),
      ],
    );
  }
}
