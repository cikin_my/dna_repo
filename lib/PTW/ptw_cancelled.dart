import 'package:dna_app/PTW/ptw_list.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class PTW_Cancelled extends StatefulWidget {
  @override
  _PTW_CancelledState createState() => _PTW_CancelledState();
}

class _PTW_CancelledState extends State<PTW_Cancelled> {

  double screenHeight;
  bool cancelledDoc = false;
  bool btnConfirm = true;

  TextStyle _label = TextStyle(
    fontFamily: 'opansans',
    color: Colors.blueGrey.shade700,
    fontSize: 16.0,
    fontWeight: FontWeight.w700,
    letterSpacing: 1,
  );
  TextStyle _info = TextStyle(
    fontFamily: 'opansans',
    fontSize: 15.0,
    fontWeight: FontWeight.normal,
    letterSpacing: 0.75,
  );

  Widget _summary() {
    return Container(
      padding: EdgeInsets.all(10),
      width: double.infinity,
      height: screenHeight / 2,
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15.0),
        ),
        color: Colors.grey[100],
        child: Padding(
          padding: const EdgeInsets.all(18.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              SizedBox(
                height: 15,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Text(
                    'No. JS:  ',
                    style: _label,
                  ),
                  Text(
                    'ABC-12-12345',
                    style: _info,
                  ),
                ],
              ),
              SizedBox(
                height: 5,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Text(
                    'No. Permit:  ',
                    style: _label,
                  ),
                  Text(
                    'JC00018000901',
                    style: _info,
                  ),
                ],
              ),
              SizedBox(
                height: 5,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Text(
                    'Stesen:  ',
                    style: _label,
                  ),
                  Text(
                    'Kuantan',
                    style: _info,
                  ),
                ],
              ),
              SizedBox(
                height: 5,
              ),
              Text(
                'Keterangan Kerja ',
                style: _label,
              ),
              Row(
                children: <Widget>[
                  Expanded(
                    child: Text(
                      'PREVENTIVE MAINTENANCE VCB DAN BUSBAR - TOTAL SHUTDOWN',
                      style: _info,
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 25,
              ),
              Text(
                'No. Authorisation/Sanction :',
                style: _label,
              ),
              SizedBox(
                height: 8,
              ),
              Row(
                children: <Widget>[
                  Text(
                    '(RCC) :',
                    style: _label,
                  ),
                  Text(
                    'XYZABC1230000',
                    style: _info,
                  ),
                ],
              ),
              SizedBox(height: 8),
              Row(
                children: <Widget>[
                  Text(
                    '(AP) :',
                    style: _label,
                  ),
                  Text(
                    'XYZABC1230000',
                    style: _info,
                  ),
                ],
              ),
              SizedBox(height: 18),
              Row(
                children: <Widget>[
                  Text(
                    'Nama OKST/OP :',
                    style: _label,
                  ),
                  Text(
                    'Eddard Stark',
                    style: _info,
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
  Widget _simpleStack() => Container(
      child: Image.asset('assets/images/cancel.png')
  );


  @override
  Widget build(BuildContext context) {
    screenHeight = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Permit Menjalankan Kerja",
          style: TextStyle(
            letterSpacing: 1,
            fontFamily: 'Lato',
          ),
        ),
        centerTitle: true,
        leading: Builder(
          builder: (BuildContext context) {
            return IconButton(
              icon: const Icon(Icons.arrow_back),
              onPressed: () {
                Navigator.of(context).push(
                  MaterialPageRoute(builder: (BuildContext context) => PTW_JobSheet()),
                );
              },
            );
          },
        ),
      ),
      body: Stack(
        children: <Widget>[
          Column(
            children: <Widget>[
              Expanded(child: _summary()),
              Visibility(
                visible: btnConfirm,
                child: Container(
                  child: Column(
                    children: <Widget>[
                      Text('Klik butang di bawah untufk MEMBATALKAN Permit Menjalankan Kerja', style: _label, textAlign: TextAlign.center,),
                      Align(
                        alignment: Alignment.bottomCenter,
                        child: RaisedButton(
                          onPressed: () {
                            setState(() {
                              cancelledDoc = true;
                              btnConfirm=false;
                            });
                          },
                          child: const Text('Confirm', style: TextStyle(fontSize: 17, letterSpacing: 2)),
                          color: Colors.red,
                          splashColor: Colors.red[700],
                          textColor: Colors.white,
                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),

                        ),
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(height: 20,),

            ],
          ),
          Visibility(
            visible: cancelledDoc,
              child: Positioned(top:screenHeight/30, bottom: screenHeight/3,left: 50,right: 50, child: _simpleStack())),
        ],
      ),
    );
  }
}
