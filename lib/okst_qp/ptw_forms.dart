import 'package:dna_app/PTW/ptw_list.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/widgets.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:grouped_buttons/grouped_buttons.dart';

class Okst_Qp_forms extends StatefulWidget {
  @override
  _Okst_Qp_formsState createState() => _Okst_Qp_formsState();
}

class _Okst_Qp_formsState extends State<Okst_Qp_forms> {
  int _radioValue1 = 0;

  void _handleRadioValueChange1(int value) {
    setState(() {
      _radioValue1 = value;

      switch (_radioValue1) {
        case 0:
          Fluttertoast.showToast(
              msg: "LOTO Box",
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.CENTER,
              timeInSecForIosWeb: 1,
              backgroundColor: Colors.orange,
              textColor: Colors.white,
              fontSize: 14.0);
          break;
        case 1:
          Fluttertoast.showToast(
              msg: "HASP Lock",
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.CENTER,
              timeInSecForIosWeb: 1,
              backgroundColor: Colors.orange,
              textColor: Colors.white,
              fontSize: 14.0);
          break;
      }
    });
  }

  bool _langkah_prosidure_1 = true;
  bool _langkah_prosidure_2 = true;
  bool _langkah_prosidure_3 = true;
  bool _langkah_prosidure_4 = true;
  String dropdownValue = 'LABEL NOMBOR SUIS & NAMA F';

  Item selectedUser;
  List<Item> users = <Item>[
    const Item(
        'Ahmad',
        Icon(
          Icons.person,
          color: const Color(0xFF167F67),
        )),
    const Item(
        'Hassan',
        Icon(
          Icons.person,
          color: const Color(0xFF167F67),
        )),
    const Item(
        'Merry',
        Icon(
          Icons.person,
          color: const Color(0xFF167F67),
        )),
    const Item(
        'Ahmad Ali',
        Icon(
          Icons.person,
          color: const Color(0xFF167F67),
        )),
  ];

  double screenHeight;
  TextStyle _label = TextStyle(
    fontFamily: 'opansans',
    color: Colors.blueGrey.shade700,
    fontSize: 16.0,
    fontWeight: FontWeight.w700,
    letterSpacing: 1,
  );
  TextStyle _info = TextStyle(
    fontFamily: 'opansans',
    fontSize: 15.0,
    fontWeight: FontWeight.normal,
    letterSpacing: 0.75,
  );

  Widget _buildInfoContainer() {
    return Container(
      height: screenHeight / 5,
      margin: const EdgeInsets.all(8.0),
      padding: const EdgeInsets.all(10.0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15.0),
        color: Colors.grey[100],
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            children: <Widget>[
              Text(
                ('Permit Manjalankan Kerja'),
                style: TextStyle(fontWeight: FontWeight.bold, letterSpacing: 1, color: Colors.black, fontSize: 16, fontFamily: 'BellotaText'),
                textAlign: TextAlign.center,
              ),
            ],
            mainAxisAlignment: MainAxisAlignment.center,
          ),
          Divider(),
          SizedBox(
            height: 15,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Text(
                'No. JS:  ',
                style: _label,
              ),
              Text(
                'ABC-12-12345',
                style: _info,
              ),
            ],
          ),
          SizedBox(
            height: 5,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Text(
                'No. Permit:  ',
                style: _label,
              ),
              Text(
                'JC00018000901',
                style: _info,
              ),
            ],
          ),
          SizedBox(
            height: 5,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Text(
                'Stesen:  ',
                style: _label,
              ),
              Text(
                'Kuantan',
                style: _info,
              ),
            ],
          ),
          SizedBox(
            height: 5,
          ),
        ],
      ),
    );
  }

  Widget _formStart() {
    return Padding(
      padding: const EdgeInsets.all(18.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            'No. Authorisation/Sanction :',
            style: _label,
          ),
          SizedBox(
            height: 8,
          ),
          Row(
            children: <Widget>[
              Text(
                '(RCC) :',
                style: _label,
              ),
              Text(
                'XYZABC1230000',
                style: _info,
              ),
            ],
          ),
          SizedBox(height: 8),
          Row(
            children: <Widget>[
              Text(
                '(AP) :',
                style: _label,
              ),
              Text(
                'XYZABC1230000',
                style: _info,
              ),
            ],
          ),
          SizedBox(height: 18),
          Row(
            children: <Widget>[
              Text(
                'Nama OKST/OP :',
                style: _label,
              ),
              Text(
                'Eddard Stark',
                style: _info,
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget _bahagian_A() {
    return Container(
      margin: EdgeInsets.all(8),
      padding: EdgeInsets.all(8),
      child: Column(
        children: <Widget>[
          SizedBox(
            height: 18,
          ),
          Text('Punca Bekalan dimatikan dan Tempat Pembumian Utama Litar di mana radas dibumi serta dilitar pintaskan'),
          SizedBox(
            height: 18,
          ),
          Text(
            'Nama dan No Suis (PMU/ PPU/ SSU/ PE/ PAT/ BLACKBOX/ FEEDER PILLAR/ LVDB)',
            style: _label,
          ),
          TextField(
            decoration: InputDecoration(
              border: OutlineInputBorder(),
              labelText: 'RSTL-NE-012345678',
            ),
          ),
          SizedBox(
            height: 10,
          ),
          TextField(
            decoration: InputDecoration(
              border: OutlineInputBorder(),
              labelText: 'CBT-0076533',
            ),
          ),
          SizedBox(
            height: 10,
          ),
          TextField(
            decoration: InputDecoration(
              border: OutlineInputBorder(),
              labelText: 'Maintenance',
            ),
          ),
          SizedBox(
            height: 8,
          ),
          Padding(
            padding: const EdgeInsets.only(left: 8.0, right: 8),
            child: Row(
              children: <Widget>[
                Text(
                  'Jenis kunci',
                  style: TextStyle(color: Colors.blueGrey, fontWeight: FontWeight.w500, fontSize: 14),
                ),
                SizedBox(
                  width: 8,
                ),
                Radio(
                  value: 0,
                  groupValue: _radioValue1,
                  onChanged: _handleRadioValueChange1,
                ),
                Text(
                  'LOTO Box',
                  style: new TextStyle(fontSize: 14.0),
                ),
                Radio(
                  value: 1,
                  groupValue: _radioValue1,
                  onChanged: _handleRadioValueChange1,
                ),
                Text(
                  'HASP Lock',
                  style: new TextStyle(
                    fontSize: 14.0,
                  ),
                ),
              ],
            ),
          ),
          SizedBox(
            height: 8,
          ),
          Text('Saya mengaku bahawa langkah-langkah prosidur keselamatan telah dipatuhi untuk memastikan kerja yang dijalankan adalah selamat.'),
          SizedBox(
            height: 18,
          ),
          Container(
            child: Column(
              children: <Widget>[
                CheckboxListTile(
                  title: Text('TAKLIMAT  KESELAMATAN & SKOP KERJA '),
                  subtitle: Text('sebelum kerja dimulakan kepada semua Orang Bekerja'),
                  controlAffinity: ListTileControlAffinity.platform,
                  value: _langkah_prosidure_1,
                  onChanged: (bool value) {
                    setState(() {
                      _langkah_prosidure_1 = value;
                    });
                  },
                ),
                CheckboxListTile(
                  title: Text('KELENGKAPAN PERLINDUNGAN DIRI'),
                  subtitle: Text('Orang Bekerja Lengkap'),
                  controlAffinity: ListTileControlAffinity.platform,
                  value: _langkah_prosidure_2,
                  onChanged: (bool value) {
                    setState(() {
                      _langkah_prosidure_2 = value;
                    });
                  },
                ),
                CheckboxListTile(
                  title: Text('KENALPASTI HAZARD (HI).'),
                  subtitle: Text('Sila Pilih'),
                  controlAffinity: ListTileControlAffinity.platform,
                  value: _langkah_prosidure_3,
                  onChanged: (bool value) {
                    setState(() {
                      _langkah_prosidure_3 = value;
                    });
                  },
                ),
                TextField(
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'RSTL-NE-012345678',
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                //langkah prosidure 4
                CheckboxListTile(
                  title: Text('LANGKAH KAWALAN BAGI HAZARD DINYATAKAN. '),
                  subtitle: Text('(KAWALAN RISIKO - DC)'),
                  controlAffinity: ListTileControlAffinity.platform,
                  value: _langkah_prosidure_4,
                  onChanged: (bool value) {
                    setState(() {
                      _langkah_prosidure_4 = value;
                    });
                  },
                ),
                TextField(
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Nyatakan',
                    labelStyle: _label,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _bahagian_C() {
    return Container(
      child: Column(
        children: <Widget>[
          Container(
            padding: EdgeInsets.all(18),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                SizedBox(
                  height: 8,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Expanded(
                        child: Text(
                      'Peringatan : PEMBUMIAN UTAMA LITAR tidak boleh diubah atau dialih KECUALI  oleh ORANG BERKEBENARAN seperti tandatangan di bawah:',
                      style: TextStyle(fontWeight: FontWeight.w500, fontSize: 18),
                      textAlign: TextAlign.center,
                    )),
                  ],
                ),
                SizedBox(height: 20),
                Container(
                  margin: EdgeInsets.all(8),
                  padding: EdgeInsets.all(8),
                  decoration: BoxDecoration(border: Border.all(width: 1, color: Colors.blueGrey)),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Expanded(
                          child: Text(
                        'Saya mengaku bahawa langkah-langkah prosidur keselamatan telah dipatuhi untuk memastikan kerja yang dijalankan adalah selamat.',
                        style: TextStyle(fontSize: 16),
                        textAlign: TextAlign.center,
                      )),
                    ],
                  ),
                ),
                SizedBox(height: 30),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Expanded(
                        child: Text(
                      'QP Name :',
                      style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold, color: Colors.blueGrey[600]),
                      textAlign: TextAlign.center,
                    )),
                    Expanded(
                        child: Text(
                      'EDDARD STARK',
                      style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold, color: Colors.blueGrey[600]),
                      textAlign: TextAlign.center,
                    )),
                  ],
                ),
                SizedBox(height: 4),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Expanded(
                        child: Text(
                      'Staff ID :',
                      style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold, color: Colors.blueGrey[600]),
                      textAlign: TextAlign.center,
                    )),
                    Expanded(
                        child: Text(
                      '100100010s',
                      style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold, color: Colors.blueGrey[600]),
                      textAlign: TextAlign.center,
                    )),
                  ],
                ),
                SizedBox(height: 4),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Expanded(
                        child: Text(
                      'Timestamp :',
                      style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold, color: Colors.blueGrey[600]),
                      textAlign: TextAlign.center,
                    )),
                    Expanded(
                        child: Text(
                      '20-09-2020-18-06',
                      style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold, color: Colors.blueGrey[600]),
                      textAlign: TextAlign.center,
                    )),
                  ],
                )
              ],
            ),
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    screenHeight = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "DNSC",
          style: TextStyle(
            letterSpacing: 1,
            fontFamily: 'Lato',
          ),
        ),
        centerTitle: true,
        leading: Builder(
          builder: (BuildContext context) {
            return IconButton(
              icon: const Icon(Icons.arrow_back),
              onPressed: () {
                Navigator.of(context).push(
                  MaterialPageRoute(builder: (BuildContext context) => PTW_JobSheet()),
                );
              },
            );
          },
        ),
      ),
      body: ListView(
        children: <Widget>[
          _buildInfoContainer(),
          _formStart(),
          Divider(),
          Text(
            'BAHAGIAN A',
            style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold, letterSpacing: 1),
            textAlign: TextAlign.center,
          ),
          _bahagian_A(),
          _bahagian_C(),
        ],
      ),
    );
  }
}

class Item {
  const Item(this.name, this.icon);

  final String name;
  final Icon icon;
}
