import 'dart:convert';
import 'package:dna_app/PTW/pte_form.dart';
import 'package:dna_app/homepage.dart';
import 'package:dna_app/myAppBar.dart';
import 'package:dna_app/okst_qp/ptw_forms.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class OperationList extends StatefulWidget {
  @override
  _OperationListState createState() => _OperationListState();
}

class _OperationListState extends State<OperationList> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Operation",
          style: TextStyle(
            letterSpacing: 1,
            fontFamily: 'Lato',
          ),
        ),
        centerTitle: true,
        leading: Builder(
          builder: (BuildContext context) {
            return IconButton(
              icon: const Icon(Icons.arrow_back),
              onPressed: () {
                Navigator.of(context).push(
                  MaterialPageRoute(builder: (BuildContext context) => OperationList()),
                );
              },
            );
          },
        ),
      ),
      body: Container(
        padding: EdgeInsets.all(8),
        child: FutureBuilder(
            future:
            DefaultAssetBundle.of(context).loadString('data/data.json'),
            builder: (context, snapshot) {
              var _data = jsonDecode(snapshot.data.toString());

              return ListView.builder(
                physics: ClampingScrollPhysics(),
                itemCount: _data == null ? 0 : _data.length,
                // Build the ListView
                itemBuilder: (BuildContext context, int index) {
                  return Card(
                    elevation:1,
                    child: InkWell(
                      onTap: (){
                        Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext context) => Okst_Qp_forms()));
                      },
                      splashColor: Colors.red.shade50,
                      focusColor: Colors.white,

                      child: Padding(
                          padding: EdgeInsets.all(10.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Row(                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  Image.asset(
                                    'assets/images/document.png',
                                    height: 40,
                                    width: 40,
                                  ),
                                  SizedBox(width: 15,),
                                  Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Text(
                                        _data[index]['nojs'],
                                        style: TextStyle(
                                            fontSize: 16.0,
                                            fontWeight: FontWeight.bold,
                                            letterSpacing: 1,fontFamily: 'opensans'

                                        ),
                                      ),
                                      Text(
                                        _data[index]['location'],
                                        style: TextStyle(
                                          fontSize: 14.0,
                                          color: Colors.black54,
                                        ),
                                      ),
                                      Row(
                                        children: <Widget>[
                                          Text(
                                            _data[index]['date'],
                                            style: TextStyle(
                                              fontSize: 14.0,
                                              color: Colors.black54,
                                            ),
                                          ),
                                          SizedBox(width: 10,),
                                          Text(
                                            _data[index]['time'],
                                            style: TextStyle(
                                              fontSize: 14.0,
                                              color: Colors.black54,
                                            ),
                                          ),
                                        ],
                                      ),
                                      SizedBox(height:4.0),
                                      Text(
                                        _data[index]['status'],
                                        style: TextStyle(
                                          fontSize: 14.0,
                                          color: Colors.black54,
                                        ),
                                      ),
                                    ],
                                  ),



                                ],
                              ),
                              Icon(Icons.keyboard_arrow_right, color: Colors.red.shade300, size: 28,)
                            ],
                          )),
                    ),
                  );
                },
              );
            }),
      ),

    );
  }
}
