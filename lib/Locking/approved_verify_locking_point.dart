import 'package:dna_app/Locking/listview_verify_locking_point.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

//1. ASSIGN AP

class ApprovedLockingPoint extends StatefulWidget {
  @override
  _ApprovedLockingPointState createState() => _ApprovedLockingPointState();
}

class _ApprovedLockingPointState extends State<ApprovedLockingPoint> {
  double screenHeight;
  String value;
  bool _apAssigned = false;

  int _radioValue1 = -1;
  int correctScore = 0;

  /*s All Widget
============================================= */

  Widget _infoOperation() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(8, 8, 8, 0),
      child: Card(
        elevation: 1,
        shadowColor: Colors.blueGrey[100],
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  children: <Widget>[
                    Text(
                      'AP Assist :',
                      style: TextStyle(color: Colors.blueGrey, fontWeight: FontWeight.w500, fontSize: 14),
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Text(
                      'Arya Stark',
                      style: TextStyle(color: Colors.black87, fontWeight: FontWeight.w300, fontSize: 14),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  children: <Widget>[
                    Text(
                      'No Js :',
                      style: TextStyle(color: Colors.blueGrey, fontWeight: FontWeight.w500, fontSize: 14),
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Text(
                      'ABC-12-12345',
                      style: TextStyle(color: Colors.black87, fontWeight: FontWeight.w300, fontSize: 14),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  children: <Widget>[
                    Text(
                      'Date & Time :',
                      style: TextStyle(color: Colors.blueGrey, fontWeight: FontWeight.w500, fontSize: 14),
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Text(
                      '1-Mar-2020 18:00PM',
                      style: TextStyle(color: Colors.black87, fontWeight: FontWeight.w300, fontSize: 14),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 8.0, left: 8, right: 8),
                child: Text(
                  'Keterangan Kerja :',
                  style: TextStyle(color: Colors.blueGrey, fontWeight: FontWeight.w500, fontSize: 14),
                ),
              ),
              SizedBox(
                width: 5,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 8, right: 8, bottom: 8),
                child: Text(
                  'Preventive Maintenance PE ABC',
                  style: TextStyle(color: Colors.black87, fontWeight: FontWeight.w300, fontSize: 14),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildForm() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Card(
        elevation: 1,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  children: <Widget>[
                    Text(
                      'Punca Bekalan:   ',
                      style: TextStyle(color: Colors.blueGrey, fontWeight: FontWeight.w500, fontSize: 14),
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Text(
                      'PMU AA ',
                      style: TextStyle(color: Colors.black87, fontWeight: FontWeight.w300, fontSize: 14),
                    ),
                  ],
                ),
              ),

              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  children: <Widget>[
                    Text(
                      'Operasi :',
                      style: TextStyle(color: Colors.blueGrey, fontWeight: FontWeight.w500, fontSize: 14),
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Text(
                      'a) VCB/OCB: Open/Rack',
                      style: TextStyle(color: Colors.black87, fontWeight: FontWeight.w300, fontSize: 14),
                    ),
                  ],
                ),
              ),

              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  children: <Widget>[
                    Text(
                      'Feeder/ CB# :',
                      style: TextStyle(color: Colors.blueGrey, fontWeight: FontWeight.w500, fontSize: 14),
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Text(
                      'XYZABC1230000',
                      style: TextStyle(color: Colors.black87, fontWeight: FontWeight.w300, fontSize: 14),
                    ),
                  ],
                ),
              ),

              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  children: <Widget>[
                    Text(
                      'No. Link',
                      style: TextStyle(color: Colors.blueGrey, fontWeight: FontWeight.w500, fontSize: 14),
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Text(
                      'CDMX10600890',
                      style: TextStyle(color: Colors.black87, fontWeight: FontWeight.w300, fontSize: 14),
                    ),
                  ],
                ),
              ),

              Padding(
                padding: const EdgeInsets.only(left: 8.0, right: 8),
                child: Row(
                  children: <Widget>[
                    Text(
                      'Voltage',
                      style: TextStyle(color: Colors.blueGrey, fontWeight: FontWeight.w500, fontSize: 14),
                    ),
                    SizedBox(
                      width: 8,
                    ),
                    Text(
                      '11KV',
                      style: new TextStyle(fontSize: 14.0),
                    ),
                  ],
                ),
              ),

              Padding(
                padding: const EdgeInsets.only(left: 8.0, right: 8),
                child: Row(
                  children: <Widget>[
                    Text(
                      'Location',
                      style: TextStyle(color: Colors.blueGrey, fontWeight: FontWeight.w500, fontSize: 14),
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Text(
                      '3.6337865, 101.5611392',
                      style: TextStyle(color: Colors.black87, fontWeight: FontWeight.w300, fontSize: 14),
                    ),
                    Icon(
                      Icons.pin_drop,
                      color: Colors.green,
                    ),
                  ],
                ),
              ),

              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  children: <Widget>[
                    Text(
                      'Attach Image:',
                      style: TextStyle(color: Colors.blueGrey, fontWeight: FontWeight.w500, fontSize: 14),
                      textAlign: TextAlign.left,
                    ),
                  ],
                ),
              ),
              //image

              Row(
                children: <Widget>[
                  Expanded(
                      child: Image.asset(
                    'assets/images/imageplaceholder.jpg',
                    width: 130,
                    height: 130,
                  )),
                  Expanded(
                      child: Image.asset(
                    'assets/images/imageplaceholder.jpg',
                    width: 130,
                    height: 130,
                  )),
                ],
              ),
              SizedBox(
                height: 18,
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    screenHeight = MediaQuery.of(context).size.height;
    return Scaffold(
        appBar: AppBar(
          title: Text(
            "Verify Locking Point",
            style: TextStyle(
              letterSpacing: 1,
              fontFamily: 'Lato',
            ),
          ),
          centerTitle: true,
          leading: Builder(
            builder: (BuildContext context) {
              return IconButton(
                icon: const Icon(Icons.arrow_back),
                onPressed: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(builder: (BuildContext context) => ListviewVerifyLockingPoint()),
                  );
                },
              );
            },
          ),
        ),
        body: Container(
          height: double.maxFinite,
          child: Stack(
            children: <Widget>[
              ListView(
                children: <Widget>[
                  _infoOperation(),
                  _buildForm(),
                ],
              ),
            ],
          ),
        ));
  }
}
