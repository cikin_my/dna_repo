
import 'package:dna_app/Locking/approved_verify_locking_point.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
//1. ASSIGN AP

class formView_VerifyLockingPoint extends StatefulWidget {
  @override
  _formView_VerifyLockingPointState createState() => _formView_VerifyLockingPointState();
}

class _formView_VerifyLockingPointState extends State<formView_VerifyLockingPoint> {
  double screenHeight;
  String value;
  bool _apAssigned = false;

  int _radioValue1 = -1;
  int correctScore = 0;

  TextEditingController _textFieldController = TextEditingController();

  void _handleRadioValueChange1(int value) {
    setState(() {
      _radioValue1 = value;

      switch (_radioValue1) {
        case 0:
          Fluttertoast.showToast(
              msg: "11KV",
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.CENTER,
              timeInSecForIosWeb: 1,
              backgroundColor: Colors.orange,
              textColor: Colors.white,
              fontSize: 14.0);
          break;
        case 1:
          Fluttertoast.showToast(
              msg: "33KV",
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.CENTER,
              timeInSecForIosWeb: 1,
              backgroundColor: Colors.orange,
              textColor: Colors.white,
              fontSize: 14.0);
          break;
      }
    });
  }

  /*s All Widget
============================================= */

  Widget _infoOperation() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(8, 8, 8, 0),
      child: Card(
        elevation: 1,
        shadowColor: Colors.blueGrey[100],
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  children: <Widget>[
                    Text(
                      'AP Assist :',
                      style: TextStyle(color: Colors.blueGrey, fontWeight: FontWeight.w500, fontSize: 14),
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Text(
                      'Arya Stark',
                      style: TextStyle(color: Colors.black87, fontWeight: FontWeight.w300, fontSize: 14),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  children: <Widget>[
                    Text(
                      'No Js :',
                      style: TextStyle(color: Colors.blueGrey, fontWeight: FontWeight.w500, fontSize: 14),
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Text(
                      'ABC-12-12345',
                      style: TextStyle(color: Colors.black87, fontWeight: FontWeight.w300, fontSize: 14),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  children: <Widget>[
                    Text(
                      'Date & Time :',
                      style: TextStyle(color: Colors.blueGrey, fontWeight: FontWeight.w500, fontSize: 14),
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Text(
                      '1-Mar-2020 18:00PM',
                      style: TextStyle(color: Colors.black87, fontWeight: FontWeight.w300, fontSize: 14),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 8.0, left: 8, right: 8),
                child: Text(
                  'Keterangan Kerja :',
                  style: TextStyle(color: Colors.blueGrey, fontWeight: FontWeight.w500, fontSize: 14),
                ),
              ),
              SizedBox(
                width: 5,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 8, right: 8, bottom: 8),
                child: Text(
                  'Preventive Maintenance PE ABC',
                  style: TextStyle(color: Colors.black87, fontWeight: FontWeight.w300, fontSize: 14),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildForm() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Card(
        elevation: 1,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  children: <Widget>[
                    Text(
                      'Punca Bekalan:   ',
                      style: TextStyle(color: Colors.blueGrey, fontWeight: FontWeight.w500, fontSize: 14),
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Text(
                      'PMU AA ',
                      style: TextStyle(color: Colors.black87, fontWeight: FontWeight.w300, fontSize: 14),
                    ),
                  ],
                ),
              ),

              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  children: <Widget>[
                    Text(
                      'Operasi :',
                      style: TextStyle(color: Colors.blueGrey, fontWeight: FontWeight.w500, fontSize: 14),
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Text(
                      'a) VCB/OCB: Open/Rack',
                      style: TextStyle(color: Colors.black87, fontWeight: FontWeight.w300, fontSize: 14),
                    ),
                  ],
                ),
              ),

              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  children: <Widget>[
                    Text(
                      'Feeder/ CB# :',
                      style: TextStyle(color: Colors.blueGrey, fontWeight: FontWeight.w500, fontSize: 14),
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Text(
                      'XYZABC1230000',
                      style: TextStyle(color: Colors.black87, fontWeight: FontWeight.w300, fontSize: 14),
                    ),
                  ],
                ),
              ),

              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  children: <Widget>[
                    Text(
                      'No. Link',
                      style: TextStyle(color: Colors.blueGrey, fontWeight: FontWeight.w500, fontSize: 14),
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Text(
                      'CDMX10600890',
                      style: TextStyle(color: Colors.black87, fontWeight: FontWeight.w300, fontSize: 14),
                    ),
                  ],
                ),
              ),

              Padding(
                padding: const EdgeInsets.only(left: 8.0, right: 8),
                child: Row(
                  children: <Widget>[
                    Text(
                      'Voltage',
                      style: TextStyle(color: Colors.blueGrey, fontWeight: FontWeight.w500, fontSize: 14),
                    ),
                    SizedBox(
                      width: 8,
                    ),
                    Radio(
                      value: 0,
                      groupValue: _radioValue1,
                      onChanged: _handleRadioValueChange1,
                    ),
                    Text(
                      '11KV',
                      style: new TextStyle(fontSize: 14.0),
                    ),
                    Radio(
                      value: 1,
                      groupValue: _radioValue1,
                      onChanged: _handleRadioValueChange1,
                    ),
                    Text(
                      '33KV',
                      style: new TextStyle(
                        fontSize: 14.0,
                      ),
                    ),
                  ],
                ),
              ),

              Padding(
                padding: const EdgeInsets.only(left: 8.0, right: 8),
                child: Row(
                  children: <Widget>[
                    Text(
                      'Location',
                      style: TextStyle(color: Colors.blueGrey, fontWeight: FontWeight.w500, fontSize: 14),
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Text(
                      '3.6337865, 101.5611392',
                      style: TextStyle(color: Colors.black87, fontWeight: FontWeight.w300, fontSize: 14),
                    ),
                    Icon(
                      Icons.pin_drop,
                      color: Colors.green,
                    ),
                  ],
                ),
              ),

              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  children: <Widget>[
                    Text(
                      'Attach Image:',
                      style: TextStyle(color: Colors.blueGrey, fontWeight: FontWeight.w500, fontSize: 14),
                      textAlign: TextAlign.left,
                    ),
                  ],
                ),
              ),
              //image

              Row(
                children: <Widget>[
                  Expanded(
                      child: Image.asset(
                    'assets/images/imageplaceholder.jpg',
                    width: 130,
                    height: 130,
                  )),
                  Expanded(
                      child: Image.asset(
                    'assets/images/imageplaceholder.jpg',
                    width: 130,
                    height: 130,
                  )),
                ],
              ),

              SizedBox(
                height: 18,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 18.0, right: 18),
                child: Divider(),
              ),
              _buildButton(),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildButton() {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        RaisedButton.icon(
          onPressed: () => {
            setState(() {
              _apAssigned = true;
            }),
            _showAlertApprove(context, 'Approve')
          },
          icon: Icon(
            Icons.check,
            size: 16,
          ),
          label: Row(
            children: <Widget>[
              Text('Approve', style: TextStyle(letterSpacing: 1)),
            ],
          ),
          color: Colors.blue,
          splashColor: Colors.blue[700],
          textColor: Colors.white,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
        ),
        RaisedButton.icon(
          onPressed: () => {
            setState(() {
              _apAssigned = true;
            }),
            _showAlertReject(context),
          },
          icon: Icon(
            Icons.close,
            size: 16,
          ),
          label: Row(
            children: <Widget>[
              Text('Reject', style: TextStyle(letterSpacing: 1)),
            ],
          ),
          color: Colors.red[700],
          splashColor: Colors.red[900],
          textColor: Colors.white,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
        ),
      ],
    );
  }

  Future _showAlertApprove(BuildContext context, String message) async {
    return showDialog(
        context: context,
        child: new AlertDialog(
          title: new Text(message),
          content: Text('By taking this action you are agree to approve this Locking Point Verification.'),
          actions: <Widget>[
            FlatButton(
              onPressed: () => Navigator.pop(context),
              child: new Text('Cancel'),
              splashColor: Colors.grey[200],
              textColor: Colors.grey,
            ),
            FlatButton(
              onPressed: () => Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext context) => ApprovedLockingPoint())),
              child: Text('Approve'),
              color: Colors.blue,
              splashColor: Colors.blue[700],
            ),
          ],
        ));
  }

  _showAlertReject(BuildContext context) async {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text('Reject'),
            content: TextField(
              controller: _textFieldController,
              decoration: new InputDecoration(
                hintText: 'Please state reason for rejection',
                hintStyle: TextStyle(fontSize: 14),
                fillColor: Colors.white,
                border: new OutlineInputBorder(
                  borderRadius: new BorderRadius.circular(8.0),
                  borderSide: new BorderSide(width: 1),
                ),
                //fillColor: Colors.green
              ),
              keyboardType: TextInputType.text,
              style: new TextStyle(
                fontFamily: "BellotaText",
              ),
            ),
            actions: <Widget>[
              FlatButton(
                onPressed: () => Navigator.pop(context),
                child: new Text('Cancel'),
                splashColor: Colors.grey[200],
                textColor: Colors.grey,
              ),
              FlatButton(
                onPressed: () => Navigator.pop(context),
                child: new Text('Reject'),
                color: Colors.red[700],
                splashColor: Colors.red[900],
              ),
            ],
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    screenHeight = MediaQuery.of(context).size.height;
    return Scaffold(
        appBar: AppBar(
          title: Text(
            "Verify Locking Point",
            style: TextStyle(
              letterSpacing: 1,
              fontFamily: 'Lato',
            ),
          ),
          centerTitle: true,
          leading: Builder(
            builder: (BuildContext context) {
              return IconButton(
                icon: const Icon(Icons.arrow_back),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              );
            },
          ),
        ),
        body: Container(
          height: double.maxFinite,
          child: Stack(
            children: <Widget>[
              ListView(
                children: <Widget>[
                  _infoOperation(),
                  _buildForm(),
                ],
              ),
            ],
          ),
        ));
  }
}
