import 'dart:convert';

import 'package:dna_app/Locking/form_view_verify_locking_point.dart';
import 'package:dna_app/Locking/listview_locking_point.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';


class ListviewVerifyLockingPoint extends StatefulWidget {
  @override
  _ListviewVerifyLockingPointState createState() => _ListviewVerifyLockingPointState();
}

class _ListviewVerifyLockingPointState extends State<ListviewVerifyLockingPoint> {
  /*use for Demo Execute function
============================================= */
  bool _showTextCExecuted = false;
  bool _isVisible = true;
  bool _apAssigned = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    setState(() {
//      images.add("Add Image");
    });
  }


  //  our widget ::
  //1. list of switching sequence
  Widget _buildListItem(_data) {
    return InkWell(
      onTap: () {
        //*=====================================================================================================================*/
        /*                           change  view :     Assign AP / Issue Open                                                  */
        //*=====================================================================================================================*/
        Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext context) => formView_VerifyLockingPoint()));
      },
      highlightColor: Colors.white30,
      splashColor: Colors.red.shade50,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.fromLTRB(8, 8, 8, 2),
                  child: Row(
                    children: <Widget>[
                      Text(
                        "Feeder" + ":",
                        style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.w400),
                      ),
                      SizedBox(
                        width: 5,
                      ),
                      Text(
                        _data['feeder'],
                        style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.w400),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(8, 8, 8, 2),
                  child: Row(
                    children: <Widget>[
                      Text(
                        "link No" + ":",
                        style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.w400),
                      ),
                      SizedBox(
                        width: 5,
                      ),
                      Text(
                        _data['link_no'],
                        style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.w400),
                      ),
                    ],
                  ),
                ),

              ],
            ),
            Icon(Icons.keyboard_arrow_right, color: Colors.red,),
          ],
        ),
      ),
    );
  }

  //2. Details about that switching
  Widget _buildInfoContainer() {
    return Container(
      height: 100,
      margin: const EdgeInsets.all(8.0),
      padding: const EdgeInsets.all(10.0),
      decoration: BoxDecoration(
        boxShadow: [
          BoxShadow(blurRadius: 5.0, color: Colors.red[200], offset: Offset(0, 5)),
        ],
        borderRadius: BorderRadius.circular(15.0),
        gradient: LinearGradient(
          colors: [
            Color.fromRGBO(255, 137, 100, 1),
            Color.fromRGBO(255, 93, 110, 1),
          ],
        ),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Row(
            children: <Widget>[
              //*=====================================================================================================================*/
              /*                            change the title                                                                          */
              //*=====================================================================================================================*/

              Text( ('Verify Locking Point'),
                style: TextStyle(fontWeight: FontWeight.bold, letterSpacing: 2, color: Colors.white, fontSize: 16, fontFamily: 'BellotaText'),
                textAlign: TextAlign.center,
              ),

              //3. if step 3(open issue) show this
              /*====================================*/
              /* Text(
                  'Issue Open',
                  style: TextStyle(fontWeight: FontWeight.bold, letterSpacing: 2, color: Colors.white, fontSize: 16, fontFamily: 'BellotaText'),
                  textAlign: TextAlign.center,
                ),
                */

              //*=====================================================================================================================*/
            ],
            mainAxisAlignment: MainAxisAlignment.center,
          ),
          Divider(),
          SizedBox(
            height: 15,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                'No. JS:  ',
                style: TextStyle(fontSize: 15, color: Colors.white, letterSpacing: .75),
              ),
              Text(
                'ABC-12-12345',
                style: TextStyle(fontSize: 15, color: Colors.white, letterSpacing: .75),
              ),
            ],
          ),
          SizedBox(
            height: 5,
          ),

        ],
      ),
    );
  }


  @override
  Widget build(BuildContext context) {
    //    text styling ::
    TextStyle _style1 = TextStyle(
      fontFamily: 'opansans',
      color: Colors.blueGrey.shade700,
      fontSize: 15.0,
      fontWeight: FontWeight.w700,
    );
    TextStyle _style2 = TextStyle(
      fontFamily: 'opansans',
      color: Colors.blueGrey.shade700,
      fontSize: 13.0,
      fontWeight: FontWeight.w700,
    );
    TextStyle _style3 = TextStyle(
      fontFamily: 'opansans',
      fontSize: 13.0,
    );

    return Scaffold(
      appBar: AppBar(
        title: Text(
          "DNA",
          style: TextStyle(
            letterSpacing: 1,
            fontFamily: 'Lato',
          ),
        ),
        centerTitle: true,
        leading: Builder(
          builder: (BuildContext context) {
            return IconButton(
              icon: const Icon(Icons.arrow_back),
              onPressed: () {
                Navigator.of(context).push(
                  MaterialPageRoute(builder: (BuildContext context) => LockingPoint()),
                );
              },
            );
          },
        ),
      ),
      body: Container(
        height: double.maxFinite,
        child: Stack(
          children: <Widget>[
            FutureBuilder(
                future: DefaultAssetBundle.of(context).loadString('data/LockingPointData.json'),
                builder: (context, snapshot) {
                  var _data = jsonDecode(snapshot.data.toString());
                  if (snapshot.data == null) {
                    return Container(child: Center(child: Text("Loading...")));
                  } else {
                    return ListView(
                      children: <Widget>[
                        _buildInfoContainer(),
                        ListView.builder(
                          physics: ScrollPhysics(),
                          shrinkWrap: true,
                          padding: EdgeInsets.all(8.0),
                          itemCount: _data == null ? 0 : _data.length,
                          itemBuilder: (BuildContext context, int index) {
                            return Container(
                              decoration: BoxDecoration(
                                border: Border(bottom: BorderSide(color: Colors.grey.shade200)),
                              ),
                              child: _buildListItem(_data[index]),
                            );
                          },
                        ),

                      ],
                    );
                  }
                }),
          ],
        ),
      ),
    );
  }
}