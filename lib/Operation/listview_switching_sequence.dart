import 'dart:convert';

import 'package:dna_app/Operation/form_view_2_Open_Issue.dart';
import 'package:dna_app/Operation/listview_worklist.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'form_view_1_AssignAP.dart';

class ListViewSwitchingSequence extends StatefulWidget {
  @override
  _SwitchingSequanceState createState() => _SwitchingSequanceState();
}

class _SwitchingSequanceState extends State<ListViewSwitchingSequence> {
  /*use for Demo Execute function
============================================= */
  bool _showTextCExecuted = false;
  bool _isVisible = true;
  bool _apAssigned = false;

  //  our widget ::
  //1. list of switching sequence
  Widget _buildListItem(_data) {
    return InkWell(
      onTap: () {
        //*=====================================================================================================================*/
        /*                           change  view :     Assign AP / Issue Open                                                  */
        //*=====================================================================================================================*/
         Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext context) => _showTextCExecuted != true? formView_1_AssignAP() : formView_2_OpenIssue()));
      },
      highlightColor: Colors.white30,
      splashColor: Colors.red.shade50,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.fromLTRB(8, 8, 8, 2),
                    child: Row(
                      children: <Widget>[
                        Text(
                          _data['type'] + ":",
                          style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.w500),
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        Text(
                          _data['location'],
                          style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.w500),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                      padding: const EdgeInsets.fromLTRB(8, 0, 8, 8),
                      child: Container(
                          child: _data["Ap_assist"] != "unassigned"
                              ? Row(
                                  children: <Widget>[
                                    Text(
                                      _data["Ap_assist"],
                                      style: TextStyle(
                                        color: Colors.green,
                                        letterSpacing: 1,
                                      ),
                                    ),
                                  ],
                                )
                              : Text(
                                  'Unassigned',
                                  style: TextStyle(color: Colors.grey[600]),
                                ))),
                ],
              ),
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                    padding: EdgeInsets.only(left: 4.0),
                    child: GestureDetector(
                      child: _data["Ap_assist"] == "unassigned"
                          ? Icon(Icons.person_add, color: Colors.deepOrange, size: 25.0)
                          : Icon(Icons.person, color: Colors.green[400], size: 25.0),
                      onTap: () {
                        //open alert to select assist
                      },
                    )),
              ],
            )
          ],
        ),
      ),
    );
  }

  //2. Details about that switching
  Widget _buildInfoContainer() {
    return Container(
      height: 200,
      margin: const EdgeInsets.all(8.0),
      padding: const EdgeInsets.all(10.0),
      decoration: BoxDecoration(
        boxShadow: [
          BoxShadow(blurRadius: 5.0, color: Colors.red[200], offset: Offset(0, 5)),
        ],
        borderRadius: BorderRadius.circular(15.0),
        gradient: LinearGradient(
          colors: [
            Color.fromRGBO(255, 137, 100, 1),
            Color.fromRGBO(255, 93, 110, 1),
          ],
        ),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            children: <Widget>[
              //*=====================================================================================================================*/
              /*                            change the title                                                                          */
              //*=====================================================================================================================*/

              Text( (
                  _apAssigned ==true ? 'Issue Open' :
                  _showTextCExecuted ==true ? 'Instruction to Execute Work':'List of Switching Sequence'

              ),
                style: TextStyle(fontWeight: FontWeight.bold, letterSpacing: 2, color: Colors.white, fontSize: 16, fontFamily: 'BellotaText'),
                textAlign: TextAlign.center,
              ),

              //3. if step 3(open issue) show this
              /*====================================*/
              /* Text(
                  'Issue Open',
                  style: TextStyle(fontWeight: FontWeight.bold, letterSpacing: 2, color: Colors.white, fontSize: 16, fontFamily: 'BellotaText'),
                  textAlign: TextAlign.center,
                ),
                */

              //*=====================================================================================================================*/
            ],
            mainAxisAlignment: MainAxisAlignment.center,
          ),
          Divider(),
          SizedBox(
            height: 15,
          ),
          Row(
            children: <Widget>[
              Text(
                'No. JS:  ',
                style: TextStyle(fontSize: 15, color: Colors.white, letterSpacing: .75),
              ),
              Text(
                'ABC-12-12345',
                style: TextStyle(fontSize: 15, color: Colors.white, letterSpacing: .75),
              ),
            ],
          ),
          SizedBox(
            height: 5,
          ),
          Row(
            children: <Widget>[
              Text(
                'Date & Time:',
                style: TextStyle(fontSize: 15, color: Colors.white, letterSpacing: .75),
              ),
              Text(
                '2-Mar-2020 18:00PM',
                style: TextStyle(fontSize: 15, color: Colors.white, letterSpacing: .75),
              ),
            ],
          ),
          SizedBox(
            height: 5,
          ),
          Text(
            'Keterangan Kerja:',
            style: TextStyle(fontSize: 16, color: Colors.white),
          ),
          Text(
            'Preventive Maintenance PE ABC, Preventive Maintenance PE ABCPreventive Maintenance PE ABC',
            style: TextStyle(fontSize: 15, color: Colors.white, letterSpacing: .75),
          ),
          SizedBox(
            height: 5,
          ),
        ],
      ),
    );
  }

  //3. button Execution to be available when all Ap is assigned to all sequence
  Widget _buildButton() {
    return Container(
      height: 84,
      padding: EdgeInsets.all(20),
      child: RaisedButton.icon(
        onPressed: () => {
          setState(() {
//            open alert dialog
            _showAlertApprove(context, 'Confirm');
//            _showTextCExecuted = true;
//            _isVisible = false;
          }),
        },
        icon: Icon(
          FontAwesomeIcons.cogs,
          size: 18,
        ),
        label: Row(
          children: <Widget>[
            SizedBox(
              width: 15,
            ),
            Text('Execute', style: TextStyle(letterSpacing: 2, fontFamily: 'nexa', fontSize: 16)),
          ],
        ),
        color: Colors.blue,
        splashColor: Colors.blue[700],
        textColor: Colors.white,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
      ),
    );
  }

  //4. message to be display after click on execution button
  Widget _buildExecutedMsg() {
    return Container(
      color: Colors.green[500],
      padding: EdgeInsets.all(20),
      child: Center(
        child: Text(
          'Execute Work Instructed on 1-Mar-2020 18:30PM',
          style: TextStyle(letterSpacing: 1, color: Colors.white, fontSize: 17, fontStyle: FontStyle.italic),
          textAlign: TextAlign.center,
        ),
      ),
    );
  }

  Widget _stepIndicator() {
    return Container(
      child: Padding(
        padding: const EdgeInsets.only(left: 18.0, right: 18),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Icon(
              Icons.check_circle,
              color: Colors.blueAccent,
              size: 30,
            ),
            Text(
              'Assign \n AP',
              style: TextStyle(fontSize: 14, letterSpacing: 1),
              textAlign: TextAlign.center,
            ),
            Expanded(
              child: new Container(
                  margin: const EdgeInsets.only(left: 10.0, right: 15.0),
                  child: Divider(
                    color: Colors.black,
                    height: 50,
                  )),
            ),
            Icon(
              Icons.check_circle,
              color: (_showTextCExecuted != true ? Colors.grey : Colors.blueAccent),
              size: 30,
            ),
            Text(
              'Instruction \n To Execute',
              style: TextStyle(fontSize: 14, letterSpacing: 1),
              textAlign: TextAlign.center,
            ),
            Expanded(
              child: new Container(
                  margin: const EdgeInsets.only(left: 15.0, right: 10.0),
                  child: Divider(
                    color: Colors.black,
                    height: 50,
                  )),
            ),
            Icon(
              Icons.check_circle,
              color: Colors.grey,
              size: 30,
            ),
            Text(
              'Issue \n Open',
              style: TextStyle(fontSize: 14, letterSpacing: 1),
              textAlign: TextAlign.center,
            ),
          ],
        ),
      ),
    );
  }

  Future _showAlertApprove(BuildContext context, String message) async {
    return showDialog(
        context: context,
        child: new AlertDialog(
          title: new Text(message),
          content: Text('By taking this action you are agree to approve this Locking Point Verification.'),
          actions: <Widget>[
            FlatButton(
              onPressed: () => Navigator.pop(context),
              child: new Text('Cancel'),
              splashColor: Colors.grey[200],
              textColor: Colors.grey,
            ),
            FlatButton(
              onPressed: () => {
                setState(() {
                  _showTextCExecuted = true;
                  _isVisible = false;
                }),
              },
              child: Text('Approve'),
              color: Colors.blue,
              splashColor: Colors.blue[700],
            ),
          ],
        ));
  }

  @override
  Widget build(BuildContext context) {
    //    text styling ::
    TextStyle _style1 = TextStyle(
      fontFamily: 'opansans',
      color: Colors.blueGrey.shade700,
      fontSize: 15.0,
      fontWeight: FontWeight.w700,
    );
    TextStyle _style2 = TextStyle(
      fontFamily: 'opansans',
      color: Colors.blueGrey.shade700,
      fontSize: 13.0,
      fontWeight: FontWeight.w700,
    );
    TextStyle _style3 = TextStyle(
      fontFamily: 'opansans',
      fontSize: 13.0,
    );

    return Scaffold(
      appBar: AppBar(
        title: Text(
          "DNA",
          style: TextStyle(
            letterSpacing: 1,
            fontFamily: 'Lato',
          ),
        ),
        centerTitle: true,
        leading: Builder(
          builder: (BuildContext context) {
            return IconButton(
              icon: const Icon(Icons.arrow_back),
              onPressed: () {
                Navigator.of(context).push(
                  MaterialPageRoute(builder: (BuildContext context) => WorkList()),
                );
              },
            );
          },
        ),
      ),
      body: Container(
        height: double.maxFinite,
        child: Stack(
          children: <Widget>[
            FutureBuilder(
                future: DefaultAssetBundle.of(context).loadString('data/SwitchingData.json'),
                builder: (context, snapshot) {
                  var _data = jsonDecode(snapshot.data.toString());
                  if (snapshot.data == null) {
                    return Container(child: Center(child: Text("Loading...")));
                  } else {
                    return ListView(
                      children: <Widget>[
                        _buildInfoContainer(),
                        ListView.builder(
                          physics: ScrollPhysics(),
                          shrinkWrap: true,
                          padding: EdgeInsets.all(8.0),
                          itemCount: _data == null ? 0 : _data.length,
                          itemBuilder: (BuildContext context, int index) {
                            return Container(
                              decoration: BoxDecoration(
                                border: Border(bottom: BorderSide(color: Colors.grey.shade200)),
                              ),
                              child: _buildListItem(_data[index]),
                            );
                          },
                        ),
                        Visibility(
                          visible: _isVisible,
                          child: _buildButton(),
                        ),
                        Visibility(
                          visible: _showTextCExecuted,
                          child: _buildExecutedMsg(),
                        ),
                        _stepIndicator(),
                        SizedBox(
                          height: 20,
                        ),
                      ],
                    );
                  }
                }),
          ],
        ),
      ),
    );
  }
}
