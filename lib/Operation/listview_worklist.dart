import 'dart:convert';



import 'package:dna_app/Operation/listview_switching_sequence.dart';
import 'package:dna_app/myAppBar.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class WorkList extends StatefulWidget {
  @override
  _WorkListState createState() => _WorkListState();
}

class _WorkListState extends State<WorkList> {
  Widget _buildContent(_data) {
    return InkWell(
      onTap: () {
//        Navigator.of(context).push(MaterialPageRoute(
//            builder: (BuildContext context) =>
//                WorkerAssignmentDetails()));
      },
      highlightColor: Colors.white30,
      splashColor: Colors.orange.shade50,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.fromLTRB(8, 8, 8, 2),
                    child: Text(
                      _data['location'],
                      style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.w500),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(8, 0, 8, 8),
                    child: Text(
                      "Status: " + _data['status'],
                      style: TextStyle(
                        fontSize: 14,
                        color: Colors.grey.shade700,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                    padding: EdgeInsets.only(left: 4.0),
                    child: GestureDetector(
                      child: Icon(Icons.keyboard_arrow_right, color: Colors.blue, size: 30.0),
                      onTap: () {
                        //go to PO  Work Plan page
                      },
                    )),
              ],
            )
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ReusableWidgets.getAppBar('Work List'),
      body: Container(
        padding: EdgeInsets.all(8),
        child: FutureBuilder(
            future:
            DefaultAssetBundle.of(context).loadString('data/data.json'),
            builder: (context, snapshot) {
              var _data = jsonDecode(snapshot.data.toString());

              return ListView.builder(
                physics: ClampingScrollPhysics(),
                itemCount: _data == null ? 0 : _data.length,
                // Build the ListView
                itemBuilder: (BuildContext context, int index) {
                  return Card(
                    elevation:1,
                    child: InkWell(
                      onTap: (){
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (BuildContext context) => ListViewSwitchingSequence()));
                      },
                      splashColor: Colors.red.shade50,
                      focusColor: Colors.white,

                      child: Padding(
                          padding: EdgeInsets.all(10.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Row(                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  Image.asset(
                                    'assets/images/document.png',
                                    height: 40,
                                    width: 40,
                                  ),
                                  SizedBox(width: 15,),
                                  Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Text(
                                        _data[index]['nojs'],
                                        style: TextStyle(
                                            fontSize: 16.0,
                                            fontWeight: FontWeight.bold,
                                            letterSpacing: 1,fontFamily: 'opensans'

                                        ),
                                      ),
                                      Text(
                                        _data[index]['location'],
                                        style: TextStyle(
                                          fontSize: 14.0,
                                          color: Colors.black54,
                                        ),
                                      ),
                                      Row(
                                        children: <Widget>[
                                          Text(
                                            _data[index]['date'],
                                            style: TextStyle(
                                              fontSize: 14.0,
                                              color: Colors.black54,
                                            ),
                                          ),
                                          SizedBox(width: 10,),
                                          Text(
                                            _data[index]['time'],
                                            style: TextStyle(
                                              fontSize: 14.0,
                                              color: Colors.black54,
                                            ),
                                          ),
                                        ],
                                      ),
                                      SizedBox(height:4.0),
                                      Text(
                                        _data[index]['status'],
                                        style: TextStyle(
                                          fontSize: 14.0,
                                          color: Colors.black54,
                                        ),
                                      ),
                                    ],
                                  ),



                                ],
                              ),
                              Icon(Icons.keyboard_arrow_right, color: Colors.red.shade300, size: 28,)



                            ],
                          )),
                    ),
                  );
                },
              );
            }),
      ),

    );
  }
}
