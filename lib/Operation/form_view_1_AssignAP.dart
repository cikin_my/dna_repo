
import 'package:dna_app/Operation/listview_switching_sequence.dart';
import 'package:dna_app/Operation/listview_worklist.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

//1. ASSIGN AP

class formView_1_AssignAP extends StatefulWidget {
  @override
  _formView_1_AssignAPState createState() => _formView_1_AssignAPState();
}

class _formView_1_AssignAPState extends State<formView_1_AssignAP> {
  double screenHeight;
  String value;
  bool _apAssigned = false;
  /*s All Widget
============================================= */

  Widget _infoOperation(){
    return   Padding(
      padding: const EdgeInsets.all(4.0),
      child: Card(
        elevation: 1   ,
        shadowColor: Colors.blueGrey[100],
        margin: const EdgeInsets.all(8.0),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                children: <Widget>[
                  Text(
                    'No Js :',
                    style: TextStyle(color: Colors.blueGrey, fontWeight: FontWeight.w500, fontSize: 14),
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  Text(
                    'ABC-12-12345',
                    style: TextStyle(color: Colors.black87, fontWeight: FontWeight.w300, fontSize: 16),
                  ),
                ],
              ),
              Row(
                children: <Widget>[
                  Text(
                    'Date & Time :',
                    style: TextStyle(color: Colors.blueGrey, fontWeight: FontWeight.w500, fontSize: 14),
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  Text(
                    '1-Mar-2020 18:00PM',
                    style: TextStyle(color: Colors.black87, fontWeight: FontWeight.w300, fontSize: 16),
                  ),
                ],
              ),
              Text(
                'Keterangan Kerja :',
                style: TextStyle(color: Colors.blueGrey, fontWeight: FontWeight.w500, fontSize: 14),
              ),
              SizedBox(
                width: 5,
              ),
              Text(
                'reventive Maintenance PE ABC',
                style: TextStyle(color: Colors.black87, fontWeight: FontWeight.w300, fontSize: 16),
              ),


              Divider(),
              Row(
                children: <Widget>[
                  Text(
                    'Punca Bekalan:   ',
                    style: TextStyle(color: Colors.blueGrey, fontWeight: FontWeight.w500, fontSize: 14),
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  Text(
                    'PMU AA ',
                    style: TextStyle(color: Colors.black87, fontWeight: FontWeight.w300, fontSize: 16),
                  ),
                ],
              ),
              Row(
                children: <Widget>[
                  Text(
                    'Lokasi :',
                    style: TextStyle(color: Colors.blueGrey, fontWeight: FontWeight.w500, fontSize: 14),
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  Text(
                    'TS - SMK Taman Megah Ria',
                    style: TextStyle(color: Colors.black87, fontWeight: FontWeight.w300, fontSize: 16),
                  ),
                ],
              ),
              Row(
                children: <Widget>[
                  Text(
                    'Switch :',
                    style: TextStyle(color: Colors.blueGrey, fontWeight: FontWeight.w500, fontSize: 14),
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  Text(
                    'JPG06065',
                    style: TextStyle(color: Colors.black87, fontWeight: FontWeight.w300, fontSize: 16),
                  ),
                ],
              ),
              Row(
                children: <Widget>[
                  Text(
                    'Command:  ON :',
                    style: TextStyle(color: Colors.blueGrey, fontWeight: FontWeight.w500, fontSize: 14),
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  Text(
                    'ON',
                    style: TextStyle(color: Colors.black87, fontWeight: FontWeight.w300, fontSize: 16),
                  ),
                ],
              ),


            ],
          ),
        ),
      ),
    );
  }
  Widget _buildButton() {
    return RaisedButton.icon(
      onPressed: () => {
        setState(() {
          _apAssigned = true;
        }),
      Navigator.of(context).push(MaterialPageRoute(
      builder: (BuildContext context) => ListViewSwitchingSequence())),
      },
      icon: Icon(Icons.person_add, size: 16,),
      label: Row(
        children: <Widget>[
          Text('Confirm', style: TextStyle(letterSpacing: 1)),
        ],
      ),
      color: Colors.blue,
      splashColor: Colors.blue[700],
      textColor: Colors.white,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
    );
  }
  Widget _buildForm (){
    return   Padding(
      padding: const EdgeInsets.all(8.0),
      child: Card(
        elevation: 1,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: <Widget>[
              Row(
                children: <Widget>[
                  Expanded(
                      child: Text(
                        "Ap Assist: ",
                        style: TextStyle(fontWeight: FontWeight.bold, letterSpacing: 1, color: Colors.blueGrey),
                      )),
                ],
              ),
              SizedBox(
                height: 2,
              ),
              DropdownButton(
                  isExpanded: true,
                  value: value,
                  hint: Text("Choose Ap Assist"),
                  items: [
                    DropdownMenuItem(
                      child: Text("Ali Aboo"),
                      value: "Item 1",
                    ),
                    DropdownMenuItem(
                      child: Text("Ahmad Mohamad"),
                      value: "Item 2",
                    )
                  ],
                  onChanged: (newValue) {
                    print(newValue);
                    setState(() {
                      value = newValue;
                    });
                  }),
              SizedBox(
                height: 2,
              ),
              _buildButton(),



            ],
          ),
        ),
      ),
    );
  }

  Widget _stepIndicator(){
    return Container(
      child: Padding(
        padding: const EdgeInsets.only (left:18.0, right: 18),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Icon(Icons.check_circle, color: Colors.blueAccent,size: 30,),
            Text('Assign \n AP', style: TextStyle(fontSize: 14, letterSpacing: 1),textAlign: TextAlign.center,),
            Expanded(
              child: new Container(
                  margin: const EdgeInsets.only(left: 10.0, right: 15.0),
                  child: Divider(
                    color: Colors.black,
                    height: 50,
                  )),
            ),

            Icon(Icons.check_circle, color: Colors.grey,size: 30,),
            Text('Instruction \n To Execute', style: TextStyle(fontSize: 14, letterSpacing: 1),textAlign: TextAlign.center,),

            Expanded(
              child: new Container(
                  margin: const EdgeInsets.only(left: 15.0, right: 10.0),
                  child: Divider(
                    color: Colors.black,
                    height: 50,
                  )),
            ),
            Icon(Icons.check_circle, color: Colors.grey,size: 30,),
            Text('Issue \n Open', style: TextStyle(fontSize: 14, letterSpacing: 1),textAlign: TextAlign.center,),
          ],
        ),
      ),
    );
  }


  @override
  Widget build(BuildContext context) {
    screenHeight = MediaQuery.of(context).size.height;
    return Scaffold(
        appBar: AppBar(
          title: Text(
            "Assign to AP Assist",
            style: TextStyle(
              letterSpacing: 1,
              fontFamily: 'Lato',
            ),
          ),
          centerTitle: true,
          leading: Builder(
            builder: (BuildContext context) {
              return IconButton(
                icon: const Icon(Icons.arrow_back),
                onPressed: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(builder: (BuildContext context) => WorkList()),
                  );
                },
              );
            },
          ),
        ),
        body: Container(
          height: double.maxFinite,
          child: Stack(
            children: <Widget>[
              ListView(
                children: <Widget>[
                  _infoOperation(),
                  _buildForm(),
                ],

              ),
              Positioned(
                child: Align(
                  alignment: FractionalOffset.bottomCenter,
                  child: _stepIndicator(),
                ),
              ),
            ],
          ),
        ));
  }
}
